#!/usr/bin/env bash

set -ex

coqc=$(command -v coqc)
export COQBIN=${coqc%/*}

make -k pdf slides
