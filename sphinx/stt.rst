Adding Proof Irrelevance
----------------------------------

When adding a universe |SProp| for types with irrelevant proofs, we
face a choice: should it be impredicative (like the :ref:`univ-prop`)?

If it is impredicative, we have a unique small universe |SProp|:

.. inference:: SProp-type

   ⊢ Γ
   0 < u
   -------------------
   Γ ⊢ \SProp : \Type_u

.. inference:: SProp-impred

   Γ ⊢ A
   Γ, x : A ⊢ B : \SProp
   ----------------------
   Γ ⊢ Π (x:A), B : \SProp

The formation rules for lambda abstractions and applications are
unchanged from :ref:`Lambda <infer-lambda>` and :ref:`App
<infer-app>` even when they involve irrelevant types.

Then we add the proof irrelevance rule:

.. inference:: SProp-irrelevant

   Γ ⊢ A : \SProp
   Γ ⊢ t : A
   Γ ⊢ u : A
   --------------
   Γ ⊢ t ≡ u : A

Impredicative |SProp| is used by Coq and by Lean (with Lean calling it
|Prop|).

If we don't want impredicativity (which is how it works in Agda), we
cannot have for instance :math:`⊢ Π (A:\SProp), A : \SProp`. Instead
we have a :math:`\SProp_i` for every universe level :math:`i` just as
we have :math:`\Type_i`. Then we have

.. inference:: SProp-type-pred

   ⊢ Γ
   u < v
   -----------
   Γ ⊢ \SProp_u : \Type_v

.. inference:: SProp-pred

   Γ ⊢ A : \Type_u
   Γ, x : A ⊢ B : \SProp_v
   ----------------------
   Γ ⊢ Π (x:A), B : \SProp_{max(u,v)}

.. inference:: SProp-irrelevance-pred

   Γ ⊢ A : \SProp_u
   Γ ⊢ a : A
   Γ ⊢ b : A
   --------------
   Γ ⊢ a ≡ b : A

Since this thesis focuses on Coq, we focus on the case where |SProp|
is impredicative then present the predicative case as an alternative.

Cumulativity and |SProp|
+++++++++++++++++++++++++

In our implementation of Coq, we forbid cumulativity between |SProp|
and the relevant universes: :math:`⊬ \SProp ≤ \Type_u`.

Cumulativity of |SProp| is consistent, and it seems decidable: when
comparing two terms, we would ask whether the principal type of their
principal type reduces to |SProp| (although we have no full proof that
this works).

The reason we forbid it is that it is not efficiently decided: we have
to infer types and do potentially costly reductions at every subterm
of a conversion problem.

.. note::

   We would also need to do retyping to work with :ref:`Records with
   Eta Expansion <primitive-record>`: in the following context

   .. coqdoc::

      Record Wrap (A:Type) : Type := wrap { unwrap : A }.
      Axioms (A:SProp) (x y : Wrap A).

   the type ``Wrap A`` is in whatever relevant universe was inferred
   for ``Wrap``, so ``x`` and ``y`` are not directly convertible by
   proof irrelevance. However we have ``x ≡ wrap (unwrap x) ≡ wrap
   (unwrap y) ≡ y`` by eta expansion, then proof irrelevance on the
   ``unwrap`` subterms. Therefore to decide such a conversion we would
   need to identify the type as a record, perform the eta expansion
   and somehow avoid infinitely recursing when ``A`` is a relevant
   type (as mentioned it's not certain that this can be made to work).

Once we have forbidden cumulativity, things are much simpler. The
essential property is that being a proof irrelevant term is stable by
well-typed substitution. The details are further explained in
:ref:`the Kernel Side section of the implementation chapter
<implem-kernel>`.

Agda and Lean forbid cumulativity for all universes, so their
developers never had to question whether |SProp| should have it.

|SProp| related primitives
+++++++++++++++++++++++++++

Empty
*****

The quintessential irrelevant type, we should have :math:`\Empty
\rightarrow A` for all :math:`A`.

The empty type brings power to the theory because its elements can be
eliminated to produce a term in any other type, including relevant
types. Without this relevant eliminations, we could interpret |SProp|
as a universe containing only the singleton type.

From :math:`\Empty` we can build :math:`\Unit \coloneqq \Empty
\rightarrow \Empty`.

Using large elimination, we can build an irrelevant type for any
decidable property. Such a property is associated to a boolean
:math:`b`, so the associated type is just

.. math::
   \If b \Then \Unit \Else \Empty.

See :ref:`translation` for the full generality of what types can be
produced from :math:`\Empty`.

Box
***

The box type, lifting an irrelevant type to the relevant world. It
emulates (in the sense of :ref:`emulated-ind`) the following inductive
type with unrestricted eliminations:

.. coqtop:: all reset

   Inductive Box (A:SProp) : Type := box : A -> Box A.
   Check Box_rect.

A primitive |Box| is equivalent to allowing constructors of
inductive types to have irrelevant arguments: we can translate the
later to constructors taking boxed arguments.

When we have a single impredicative |SProp|, |Box| lives in the
lowest relevant universe (in Coq, this means |Prop|). When we have
predicative :math:`\SProp_i` the box of a type lives in the relevant
universe at the same predicative level.

Squash
******

Squashing a relevant type :math:`A` is to produce an irrelevant
approximation :math:`\Squash\ A` which emulates the following inductive
type with elimination restricted to irrelevant predicates:

.. coqtop:: in reset

   Inductive Squash (A:Type) : SProp := squash : A -> Squash A.

.. coqtop:: all

   Check Squash_sind.

When |SProp| is impredicative, ``Squash`` does not need to be a
primitive type:

.. coqtop:: in reset

   Definition Squash (A:Type) : SProp := forall P : SProp, (A -> P) -> P.
   Definition squash A (a:A) : Squash A := fun P f => f a.
   Definition Squash_sind A (P : Squash A -> SProp)
     (f : forall a, P (squash A a)) (s:Squash A)
     : P s
     := s (P s) f.

(``f`` has type ``forall a : A, P (squash A a)`` which by proof
irrelevance is convertible to ``A -> P s``)

The restriction on ``Squash``'s eliminations is similar to the
restriction in :ref:`Inductives and Prop <ind-and-prop>`: we can allow any inductive type
(still requiring the positivity condition) in |SProp| with
eliminations restricted to |SProp|. From this perspective, |SProp| is
similar to |Prop| but with a stricter condition for singleton
elimination.

When |SProp| is predicative, the squash of a type lives at the same
level as the original type, and it eliminates to any level of |SProp|,
including higher ones. These unlimited eliminations require it to be
primitive, unlike the impredicative |SProp| case.

Dependent Pairs
===============

The type of irrelevant dependent pairs

.. math::
   A : \SProp, B : A → \SProp ⊢ Σ_s (x:A)\ (B\ x) : \SProp

may be implemented using the dependent pairs from the relevant world
together with Box and Squash as

.. math::

   \Squash\ (Σ (x:\mathtt{Box}\ A)\ (\mathtt{Box}\ (B\ (\mathtt{unbox}\ x)))).

Eliminating the squash to define the projections works since their
types are irrelevant. Note that this doesn't use proof irrelevance.

Since we have proof irrelevance, we have definitional :math:`η` for
irrelevant pairs.

Irrelevant pairs may also be defined impredicatively (when the theory
allows impredicativity) as

.. math::
   Π (P:\SProp), \left(Π (x:A), B\ x → P \right) → P.

In this formulation proof irrelevance is necessary to get a well-typed
second projection.

If for some reason we work in a theory without Box or Squash,
dependent pairs in the relevant world are not sufficient to define
irrelevant pairs, which must then be provided as a separate primitive.

Consistency with Proof Irrelevance
-----------------------------------

Consistency can be obtained as a corollary of the proof in
:ref:`sprop-dec`, but this is not useful when we want to know which
axioms are consistent in a type theory with |SProp|. For this, we need
to come up with some models.

The work in the section has already been published in
:cite:`Gilbert:POPL2019`.

Translation to Extensional Type Theory
++++++++++++++++++++++++++++++++++++++

.. raw:: latex

   \begin{figure}

.. math::

       & \trad{\Type_i}{} && := \Sum{A}{\Type_i}{\Unit}\\
       & \trad{\SProp_i}{} && := \Sum{A}{\Type_i}{\Prod{x \ y}{A}{x = y}}\\
       & \trad{x}{} && := x\\
       & \trad{\ProdT{x}{A}{B}}{} && := (\Prod{x}{\Trad{A}{}}{\Trad{B}{}} ; \tUnit) \\
       & \trad{\ProdS{x}{A}{B}}{} && :=
                                      (\Prod{x}{\Trad{A}{}}{\Trad{B}{}}
                                      ; \\
   & && \phantom{ := ( } \fun{f \ g}{\Prod{x}{\Trad{A}{}}{\Trad{B}{}}}
                                      \pi_\Pi \ \Trad{A}{} \  \TradProof{B}{} \ f \ g) \\
       & \trad{\fun{x}{A}{M}}{} && := \fun{x}{\Trad{A}{}}{\trad{M}{}} \\
       & \trad{M N}{} && := \trad{M}{}\ \trad{N}{}\\
       & \trad{\sEmpty}{} && := (\Empty ; \fun{x \ y}{\Empty}{\pi_\Empty
                              \ x \ y}) \\
       & \trad{\sEmptyRect{P}{t}}{} && := \EmptyRect{\trad{P}{}}{\trad{t}{}} \\
       % & \trad{\sUnit}{} && := (\Unit ; \fun{x \ y}{\Unit}{\pi_\Unit \ x
       %                       \ y}) \\
       % & \trad{\stUnit}{} && := \tUnit  \\
       % & \trad{\sUnitRect{P}{p}{t}}{} && := \UnitRect{\trad{P}{}}{\trad{p}{}}{\trad{t}{}} \\
       & \trad{\tBox\; A}{} && := (\pi_1 \trad{A}{}, \tUnit) \\
       & \trad{\bx{x}}{} && := \trad{x}{} \\
       & \trad{\unbox{P}{f}{a}}{} && :=  \trad{f}{} \ \trad{a}{} \\
       & \Trad{A}{} && := \projOne{\trad{A}{}} \\
       & \TradProof{A}{} && := \projTwo{\trad{A}{}}

       \\
       & \pi_\Empty \ x \ y && := \EmptyRect{(x=y)}{x} \\
       % & \pi_\Unit \ x \ y && := \UnitRect{(\fun{t}{\Unit}{t=y})}
       %                  {(\UnitRect{(\fun{t'}{\Unit}{\tUnit=t'})}{(\refl{}{\tUnit})}{y})}{x} \\
       & \pi_\Pi \ A \ \pi_B \ f \ g && :=  \funext{(\fun{x}{A}{\pi_B (f
                                        \ x) (g \ x)})} \\

.. raw:: latex

     \caption{Syntactical Translation from type theory with \SProp~to ETT}
     \label{fig:translation}
   \end{figure}

Following the notion of syntactical translations advocated in
:cite:`boulier:hal-01445835`, we prove consistency of type theory with
|SProp| by a type preserving translation into :ref:`ett`.

The translation is described in Figure :latex:`\ref{fig:translation}`.

There must not be cumulativity between |SProp| and relevant universes:
we need to know which :math:`Π` types are irrelevant. Once we have
forbidden cumulativity of |SProp|, we may introduce a syntactic
difference between relevant and irrelevant product types, justified by
:ref:`relevance-unicity`. In the proof of decidability the syntactic
mark is about the domain of the products, whereas for the translation
it is about the codomain, but the principle is the same.

Carneiro's thesis :cite:`Carneiro2019` also shows soundness with a
pre-processing step (6.1 "Proof splitting") to introduce this
syntactic distinction, but then goes directly to a model using ZFC
instead of going through ETT.

If we want to handle cumulativity, we need a way to forget the proof
of irrelevance when cumulativity is used. This has not been fully
investigated.

The idea of the translation is to see an inhabitant of |SProp| as a pair
of type in ETT together with a proof that it has proof irrelevance.
Therefore, |SProp| is translated as the following dependent sum:

.. math::

   \trad{\SProp_i}{} := \Sum{A}{\Type_i}{\Prod{x \ y}{A}{x = y}}.

The rest of the translation is rather straightforward, but for the
fact that we need to provide an additional proof that type
constructors which end in |SProp| have proof irrelevance. For instance,
the fact that a dependent product whose codomain is irrelevant is
itself irrelevant can be proven using functional extensionality in
ETT. Identity types are proof irrelevant in ETT so we can also translate
identity types in |SProp|.

Definitional proof irrelevance is then modeled by the fact that every
inhabitant of |SProp| is a mere proposition together with the
reflection rule of ETT.

If we need to translate without the application congruence axiom we
can also translate to ETT with annotated applications.

The following two properties are proved by mutual induction (although
we state them separately for readability).

- **Preservation of conversion:**

  For every term :math:`t` and :math:`u` of type theory with |SProp|,
  if :math:`\typ{\Gamma}{t \conv u}{A}` then :math:`\Trad{\Gamma}{s}
  \vdash \trad{t}{s} \conv \trad{u}{s}` in ETT.

   By induction on the proof of congruence.

   The structure of the term is preserved by the translation, so
   :math:`\beta`-reduction and congruence rules are automatically
   preserved. The only interesting case is the rule for definitional
   proof-irrelevance which says that when
   :math:`\typ{\Gamma}{A}{\SProp}`, then :math:`t` and :math:`u` are
   convertible. But by correctness of the translation, we know that
   :math:`\typ{\Trad{\Gamma}{s}}{\trad{A}{s}}{\Sum{A}{\Type_i}{\Prod{x
   \ y}{A}{x = y}}}` and :math:`\trad{t}{s}`, :math:`\trad{u}{s}` have
   type :math:`\Trad{A}{s}`. Thus we can form of proof of
   :math:`\trad{t}{s} = \trad{u}{s}` by :math:`\TradProof{A}{s} \
   {\trad{t}{s}} \ {\trad{u}{s}}`. From which we deduce
   :math:`\trad{t}{s} \conv \trad{u}{s}` by the reflection rule.


- **Correctness of the syntactical translation:**

  For every typing derivation of type theory with |SProp|
  :math:`\typ{\Gamma}{M}{A}`, we have the corresponding derivation
  :math:`\typ{\Trad{\Gamma}{s}}{\trad{M}{s}}{\Trad{A}{s}}` in ETT.

    By induction on the typing derivation.

.. note::

  If we have impredicative |SProp|, we use an extensional type theory
  with impredicative |Prop|, and use :math:`\trad{\SProp}{} :=
  \Sum{A}{\Prop}{\Prod{x \ y}{A}{x = y}}`.

This gives us consistency of type theory with |SProp| and classical
axioms such as function extensionality, UIP, excluded middle or
choice.

Univalent Model
+++++++++++++++

.. todo? abel wants us to say more here but not sure what

We can modify the translation for type theory with |SProp| and *no
UIP* to target the Homotopy Type System (HTS) :cite:`voevodskyHTS` of
Voevodsky.

In HTS, types are divided between pretypes and "fibrant" types.
Fibrancy is preserved by the usual constructions (product types,
inductives types, etc). However elimination of fibrant inductives is
restricted to fibrant types.

There are 2 identity types: the strict equality, a pretype which
satisfies the reflection rule of extensional type theory (as such we
represent it with :math:`≡`), and the fibrant equality :math:`=`,
which is the usual inductive.

The separation between strict and fibrant equality allows us to have
equality reflection for the strict equality without implying
uniqueness of proofs of fibrant equality: the usual proof of UIP from
reflection breaks down since we can't eliminate fibrant equalities to
produce strict ones.

Fibrant equality satisfies the univalence axiom: 2 fibrant types are
fibrantly equal if there is an equivalence between them.

When translating to HTS we have a problem: we want to translate types
to fibrant types in order to be able to translate the univalence axiom
to its implementation in HTS, but we also want to translate |SProp| to
:math:`Σ (A:\Prop) Π (x\; y : A), x ≡ y` (where |Prop| comes from the
propositional resizing rule, for impredicativity). By the rules we
have given this is not a fibrant type.

For this we rely on work by Thierry Coquand :cite:`bishop-coquand`
showing that a universe of strict propositions can be built in the
cubical model. This then allows us to extend HTS by such a universe
which we translate |SProp| to.

The translation then shows that |SProp| is compatible with univalence.

.. _sprop-dec:

Decidability and Reducibility with Proof Irrelevance
-------------------------------------------------------------------------------

We only prove decidability for a reduced type theory in order to keep
the proof at a reasonable size. See :ref:`sec-limitations` for more
details.

This is formalized in Agda, with source available at
https://github.com/SkySkimmer/logrel-tt commit
``0cdfbc81e4f3a326e42b92f2c7826ce4953a3e1a``.

Idea of the proof
+++++++++++++++++

We base our proof on the proof by Abel et al. :cite:`AbelDecidable`.
Let us first explain enough of the original proof to understand what
we need to change.

The proof applies to a dependent type theory with a universe of small
types :math:`U` and a small type of natural numbers :math:`\Nat : U`
with large elimination. See :ref:`sec-limitations` for discussion of
the power of this theory.

Terms and types are in the same syntactic domain. The term formers are

- Product types :math:`Π A \tri B`. Separating the domain and codomain
  using :math:`\tri` instead of a comma distinguishes the De Bruijn
  index syntax from the named variable syntax.
- Lambda abstractions :math:`λ\ t`. The original work by Abel et al.
  uses lambda abstractions without a type annotation for the domain,
  losing unique typing. We follow this choice in this thesis.

  Note that adding the type annotations is trivial, touching about 25
  lines out of 12000 in the Agda formalization.
- Applications :math:`t ∘ u`.
- Variables :math:`i_k`. We use De Bruijn indices, written as the
  subscript :math:`k`, replacing alpha renaming issues with explicit
  lifting. We may omit the subscript when its specific value is not
  interesting: for instance we may talk about the newest bound
  variable :math:`i_0`, but when we say that a variable is bound to a
  type in a context we only say :math:`i : A ∈ Γ`.
- The universe of small types :math:`U`.
- Term formers for natural numbers: the type of natural numbers
  :math:`\Nat`, the zero :math:`0` and successor :math:`S\ t`
  constructors, and the eliminator :math:`\natrec F\ z\ s\ n`
  (where a variable of type :math:`\Nat` is bound in :math:`F`).

  Note that the successor and eliminator always appear fully applied.
  Naturals have large elimination, i.e. :math:`F` may be a large type
  such as :math:`U`.

Neutral terms and weak head normal forms are simple syntactic
properties.

Since we have a finite hierarchy of universes, not all types have a
type and we need separate judgments for types and terms. As such, the
type theory is defined by mutually defined inductive types:

- **Well-formed context:** :math:`⊢ Γ`
- **Well-formed type:** :math:`Γ ⊢ A`
- **Typing:** :math:`Γ ⊢ t : A`
- **Conversion for types:** :math:`Γ ⊢ A ≡ B`
- **Conversion for terms:** :math:`Γ ⊢ t ≡ u : A`

Once we have the typing judgment we can also define well-typed weak
head reduction for terms :math:`Γ ⊢ t ⇒ u : A` and for types :math:`Γ
⊢ A ⇒ B`. It is trivially a subset of conversion. Since we only reduce
at the head, it is deterministic and we dodge confluence issues
(confluence can be seen semantically as a corollary of some of the
lemmas we prove, but is never explicitly stated or proved).

Both conversion judgments are generated by reflexivity, symmetry,
transitivity, congruence rules for each term former, eta equality at
function types, beta reduction and reduction for :math:`\natrec`.
Because of the power of transitivity, we cannot directly prove
inversion lemmas (as defined in :ref:`inversion`) or decidability.

Instead, we do through alternative characterizations of conversion
based on reduction:

- **Algorithmic equality** between terms :math:`Γ ⊢ t \Conv u : A`
  and between types :math:`Γ ⊢ A \Conv B`. These are mutually defined
  inductive types which can be seen as the trace of the conversion
  decision algorithm: first reduce to weak head normal forms, then
  apply congruence rules and continue recursively on subterms.

  We need to treat neutral terms specially when deciding conversion,
  so there is also a judgment for algorithmic equality of neutral
  terms :math:`Γ ⊢ k \ConvN l : A`. There is no type version: large
  types are never neutral, so equality between neutral terms is enough to
  account for equality between neutral types.

  Normally we compare terms at a unique type, but neutral terms break
  this invariant: even if we start by comparing two neutral terms at
  the same type :math:`t ∘ u : T` and :math:`t' ∘ u' : T`, the heads
  must be compared at possibly different types. For instance if
  :math:`f` and :math:`P` are variables such that :math:`f : Π ( x\
  y:\Nat ), P\ (x+y)` (where :math:`+` is defined using
  :math:`\natrec` and therefore computes), the terms :math:`f\ 0\ 1`
  and :math:`f\ 1\ 0` both have type :math:`P\ 1` even though the
  heads have different types.

  However, the base case for variables does not need type information
  and if neutral terms are equal then their types must also be equal
  (even without domain annotations for lambda abstractions neutral
  terms still have unique typing). Therefore for neutral terms we can
  decide whether they are convertible at a unique (up to conversion)
  common type or not convertible at any type.

  The original work by Abel et al. uses :math:`Γ ⊢ t \Conv u : A` in
  the paper typesetting and :agda:`Γ ⊢ t [conv↑] u : A` in Agda code,
  we follow this choice.

- **Generic equality** between terms :math:`Γ ⊢ t ≅ u : A` and
  between types :math:`Γ ⊢ A ≅ B`. We want to make definitions and
  proofs about both conversion and algorithmic equality, so we define
  *generic equalities* which may be instantiated by either.

  Since we want to instantiate them by algorithmic equality generic
  equalities also have a form for neutral terms :math:`Γ ⊢ k ∼ l : A`.
  When instantiating generic equality with conversion both forms are
  instantiated by the conversion judgments.

  Generic equalities must be sound with regards to conversion,
  satisfy congruence rules (but the congruence rules for the
  eliminators are restricted to the neutral equality), be symmetric
  and transitive, be compatible with each other (for instance neutral
  equality must imply unqualified equality), be compatible with
  weakening (if two terms or types are equal so is their weakening)
  and reduction (if two terms or types reduce to equal weak head
  normal forms then they are equal), and the unqualified equality must
  satisfy eta equality of functions.

  Note that the congruence rules for term formers without subterms
  such as :math:`0` amount to reflexivity at that term in well-formed
  contexts.

- **The logical relation** between terms :math:`Γ ⊩_ℓ t ≡ u : A / \mathcal{A}`
  and between types :math:`Γ ⊩_ℓ A ≡ B / \mathcal{A}` (which may also be read
  respectively as ":math:`t` is reducibly equal to :math:`u` and
  :math:`A` is reducibly equal to :math:`B`).

  It is defined in successive layers for each type level :math:`ℓ` (in
  our case small then large types), with each layer depending on the
  previous, as part of an inductive-inductive-recursive block. The
  other components of the block are

  - Reducible types :math:`Γ ⊩_ℓ A` read ":math:`A` is a reducible
    type". This is the inductive part of the block, the other
    components are recursively defined using an argument :math:`\mathcal{A}`
    of this type.
  - Reducible terms at a given type :math:`Γ ⊩_ℓ t : A / \mathcal{A}`.

  Quickly after its definition we prove the *irrelevance lemma*:
  reducible terms, type equality and term equality at different proofs
  :math:`\mathcal{A}` and :math:`\mathcal{A}'` of :math:`Γ ⊩_ℓ A` are logically
  equivalent. Additionally we implicitly do existential quantification
  by the type level.

  This justifies omitting the reducible type argument and the type
  level from our statements: for instance we will talk about reducible
  equality between terms :math:`Γ ⊩ t ≡ u : A`. In the formalization,
  the existential quantification is explicit and we do not omit the
  reducible type argument as the proofs are done by induction on it
  and on the type level.

  We have a contructor of reducible type for each type former:

  - The universe is a reducible type in well-formed contexts when we
    are at the level of large types.
  - Types which reduce to :math:`\Nat` are reducible at any level.
  - Types which reduce to any neutral element of the universe which is
    reflexive for the neutral equality are reducible at any level.
  - Small reducible types are also large reducible types.
  - Types which reduce to product types are reducible if the domain
    and codomain are reducible (all at a given level), and some side
    conditions are verified.

    The side conditions are complex and do not change when adding
    proof irrelevance so we do not detail them.

    Note that since the domain must be at the same level as the whole
    type, this proof method is incompatible with impredicativity.

  The other components are defined by cases, also based on reduction.
  For instance:

  - A term is reducible at the universe if it is a reducible small
    type. Because the universe is a reducible large type this is not
    circular.

  - Two terms are reducibly equal at a type which reduces to
    :math:`\Nat` if, inductively, either they reduce to both :math:`0`,
    to successors applied to reducibly equal natural number terms,
    or to neutral terms equated by the generic equality.

  This gives inversion lemmas for reducible equality by definition.
  For instance injectivity of constructors: from a proof that :math:`Γ
  ⊩_ℓ S\ m ≡ S\ n : \Nat / [\Nat]` for some :math:`[\Nat] : Γ ⊩_ℓ
  \Nat`, by the irrelevance lemma we can replace :math:`[\Nat]` by the
  appropriate constructor, then reduce to get a proof of :math:`Γ ⊩_ℓ
  m ≡ n : \Nat` (this sketch skips over some proofs of well-formedness
  of context and the like, which pose no difficulty).

The algorithmic equality is designed to make it easy to show
decidability of :math:`Γ ⊢ t \Conv u : A` given hypotheses :math:`Γ ⊢
t \Conv t : A` and :math:`Γ ⊢ u \Conv u : A`: the reflexivity
hypotheses (which after our discussion of neutral terms you may notice
are at the same type) provide structure for induction.

We then need to show soundness and completeness (from completeness and
a typing derivation :math:`Γ ⊢ t : A` we can get :math:`Γ ⊢ t \Conv t
: A` to use as argument for the decidability proof).

Soundness is relatively trivial (the constructors of algorithmic
equality are close to the rules of the type theory combined with
reduction), except for some subtlety regarding the treatment of
neutral terms which requires inversion lemmas for the typing
judgments. Completeness is more difficult.

We use the logical relation to go between typing judgments and
algorithmic equality:

- The *escape lemma* shows that reducible types are well-formed types,
  reducible terms are well-typed, and reducible equality implies the
  generic equality.
- The *fundamental theorem* shows that the typing judgments imply the
  corresponding logical relation judgment, for instance conversion
  implies reducible equality.

  The fundamental theorem is proved using validity judgments :math:`Γ
  ⊩_ℓ^v t : A / \mathcal{G} / \mathcal{A}` (and similar for types and
  both equalities) derived from the logical relation. Essentially the
  validity judgment is needed for closure by substitution of the
  logical relation.

Instantiating the generic equality by conversion (which trivially
satisfies the expected properties), we get inversion lemmas and
reducibility for the typing judgments.

Using these inversion lemmas, we can show soundness and the other
generic equality properties for algorithmic equality. Then by the
escape lemma and fundamental theorem, algorithmic equality is
logically equivalent to conversion.

Together with decidability of algorithmic equality on its reflexive
domain, we obtain our desired decidability of conversion.

Adding Proof Irrelevance
++++++++++++++++++++++++

We are now ready to add proof irrelevance and show that it does not
affect reducibility and decidability of conversion.

This at first glance means adding a universe of small strict
propositions |Prop| and a constructor to the conversion rule:

.. inference:: Prop-Irr

   \Gamma \vdash A : \Prop
   \Gamma \vdash a : A
   \Gamma \vdash b : A
   ----------------------------
   \Gamma \vdash a ≡ b : A

However we need a predicative system and we only have 1 level of
universes, such that :math:`\Pi A:\Prop, A \rightarrow A` is an
irrelevant type which itself has no type.

.. alternative idea to consider: use `type types = Typ of term * relevance`
.. ie terms aren't types, they are embedded in types if you provide a relevance
.. not sure if this works well, might be a pain with the 2 ways to build small Pi types

Instead we mark term and type judgments with the relevance of the
type: well-formed type :math:`\Gamma \vdash A \relevance{r}`,
well-typed term :math:`\Gamma \vdash e : A \relevance{r}`, conversion
for types :math:`\Gamma \vdash A ≡ B \relevance{r}` and for terms
:math:`\Gamma \vdash a ≡ b : A \relevance{r}`.

The meta-variable :math:`r` denotes the relevance, either
:math:`\relevant` for relevant or :math:`\irrelevant` for irrelevant.

.. note:: In Agda code we use the slightly different :agda:`%` for
          irrelevance, for better rendering between monospace and math
          fonts.

We also need to annotate context elements and domains of product
types, in order to provide sufficient information to our proofs (such
as the irrelevance lemma) before we have shown
:ref:`relevance-unicity`.

For most of the typing rules, adding relevance marks is straightforward. For instance

.. inference:: Pi-Ty

   \Gamma \vdash F \relevance{r_F}
   \Gamma, F \vdash G \relevance{r_G}
   -------------------------------------
   \Gamma \vdash \Pi F \relevance{r_F} \tri G \relevance{r_G}

.. inference:: Context-Extend

   \Gamma \vdash
   \Gamma \vdash A \relevance{r}
   -----------------------------
   \Gamma, A\relevance{r} \vdash

.. inference:: Var

   \Gamma \vdash
   i : A\relevance{r} \in \Gamma
   -----------------------------
   \Gamma \vdash i : A

Of particular interest are the rules for forming a type from a term
whose type is a universe:

.. inference:: U-Ty

   \Gamma \vdash A : U \relevance{\relevant}
   -----------------------------------------
   \Gamma \vdash A \relevance{\relevant}

.. inference:: Prop-Ty

   \Gamma \vdash A : \Prop \relevance{\relevant}
   --------------------------------------------
   \Gamma \vdash A \relevance{\irrelevant}

.. note:: The relevance is always :math:`\relevant` in the premise as
  types are always relevant terms (i.e. :math:`U` and |Prop| are
  relevant types). A small amount of experimentation indicates that
  this works better than leaving it free and later proving that is
  always :math:`\relevant`, at least when using Agda ``--without-K``.

.. note:: We have 1 universe per relevance: :math:`U =
  \Univ_\relevant` and :math:`\Prop = \Univ_\irrelevant`.

The irrelevance rule with marks is then

.. inference:: Irrelevance

   \Gamma \vdash a : A \relevance{\irrelevant}
   \Gamma \vdash b : A \relevance{\irrelevant}
   -----------------------------------------------
   \Gamma \vdash a = b : A \relevance{\irrelevant}

Well-typed reduction also needs relevance marks, for instance
reduction of types is now :math:`\Gamma \vdash A \longrightarrow B
\relevance{r}`, such that we can prove it implies :math:`Γ ⊢ A ≡ B
\relevance{r}` without additional hypotheses.

As a bonus, we may add an empty small irrelevant type :math:`\Empty :
\Prop`. Notice that all terms in the empty type must reduce to neutral
terms.

Marking the Logical Relation
++++++++++++++++++++++++++++

We still mark everything where a type appears, eg the logical relation
is :math:`\Gamma \Vdash_{\nicel} t = u : A \relevance{r}`.

Generic equalities must satisfy an additional property to provide
proof irrelevance. Specifically, the equality between neutral terms
must relate any two terms within its domain and which have the same
irrelevant type.

.. inference:: GenEq-irrelevance

   \Gamma \vdash k : A \relevance{\irrelevant}
   \Gamma \vdash l : A \relevance{\irrelevant}
   \Gamma \vdash k \sim k : A \relevance{\irrelevant}
   \Gamma \vdash l \sim l : A \relevance{\irrelevant}
   --------------------------------------------------
   \Gamma \vdash k \sim l : A \relevance{\irrelevant}

We do not require an analogous property for the generic equality
between arbitrary terms :math:`\_ \vdash \_ \cong \_ : \_
\relevance{\_}`, see :ref:`logrel-pi` for details.

Adapting the definition of the logical relation is mechanical work,
let's consider a few cases:

- universe: both universes are relevant large reducible types. The
  relevance is important for the reducible member of a universe: they must
  be reducible small types of the correct relevance. In other words
  the second component of :math:`\Gamma \Vdash_{\nicel} t :
  Univ_r \relevance{\relevant} / \mathcal{G}` is :math:`\Gamma \Vdash_{l'} t
  \relevance{r}`, and analoguously for reducibly equal terms.

- neutrals: the formation rule becomes

  .. inference:: Neutral-Reducible

     \Gamma \vdash A :\longrightarrow^*: N \relevance{r}
     \Gamma \vdash N \sim N : Univ_r \relevance{\relevant}
     ---------------------------------------------------
     \Gamma \Vdash_{\nicel} A \relevance{r}

  Once again the relevance and the universe need to be coordinated.

  The sub cases are straightforward to adapt, just add the relevance.

  In the irrelevant case, we still have components :math:`n \sim n`
  (resp. :math:`n \sim m`) in the definition of reducible terms (resp.
  reducibly equal terms). It is the neutral equality which should
  handle irrelevance.

Most properties (reflexivity, weakening, etc) are trivial to adapt.

.. note::

   We need the relevance marks on domains of product types to get the
   irrelevance lemma.

   When both derivations of :math:`Γ ⊩_ℓ A \relevance{r}` are from the
   product type case, we know that :math:`A` reduces to some
   :math:`\Pi F \relevance{r_F} \tri G` from one derivation, and to
   some :math:`\Pi F' \relevance{r_F'} \tri G'` from the other, along
   with associated properties such as :math:`\Gamma, F \relevance{r_F}
   \Vdash_\ell G \relevance{r}`. Since reduction is deterministic we know
   that the reducts are equal syntactically

   .. math::
      \Pi F \relevance{r_F} \tri G = \Pi F' \relevance{r_F'} \tri G',

   and so the relevances are also equal.

   If we didn't include the mark inside the terms, we would still need
   to know that some mark exists and is used in the sub-properties,
   but we would not be able to prove that the one used in
   :math:`\mathcal{G}` is the same which is used in
   :math:`\mathcal{G}'`.

Adapting the rest of the proofs to add relevance marks is essentially
a matter of fixing the statements, with no interesting insights. There
are then 3 interesting parts: dealing with proof irrelevance in the
fundamental theorem (showing that the logical relation is inhabited from
a judgment of our type theory), relevance unicity (a new result,
analogous to unicity of typing), and dealing with proof irrelevance in
the conversion algorithm.

.. _logrel-pi:

Proof irrelevance of the logical relation
+++++++++++++++++++++++++++++++++++++++++

When proving the fundamental theorem, we have a new case where
:math:`\Gamma \vdash t ≡ u : A \relevance{\irrelevant}` is given by
the proof irrelevance rule. We have sub-derivations :math:`\Gamma
\vdash t : A \relevance{\irrelevant}` and :math:`\Gamma \vdash u : A
\relevance{\irrelevant}`, and by induction

- :math:`\mathcal{G} :: \Vdash^v \Gamma`
- :math:`\mathcal{A} :: \Gamma \Vdash^v_1 A \relevance{\irrelevant} / \mathcal{G}`
- :math:`\Gamma \Vdash^v_1 t : A \relevance{\irrelevant} / \mathcal{G} / \mathcal{A}`
- :math:`\mathcal{G}' :: \Vdash^v \Gamma`
- :math:`\mathcal{A}' :: \Gamma \Vdash^v_1 A \relevance{\irrelevant} / \mathcal{G}'`
- :math:`\Gamma \Vdash^v_1 u : A \relevance{\irrelevant} / \mathcal{G}' / \mathcal{A}'`

We seek to establish the following goals:

- :math:`\mathcal{G}'' :: \Vdash^v \Gamma`
- :math:`\mathcal{A}'' :: \Gamma \Vdash^v_1 A \relevance{\irrelevant} / \mathcal{G}''`
- :math:`\Gamma \Vdash^v_1 t : A \relevance{\irrelevant} / \mathcal{G}'' / \mathcal{A}''`
- :math:`\Gamma \Vdash^v_1 u : A \relevance{\irrelevant} / \mathcal{G}'' / \mathcal{A}''`
- :math:`\Gamma \Vdash^v_1 t = u : A \relevance{\irrelevant} / \mathcal{G}'' / \mathcal{A}''`

By irrelevance of the validity judgment we have :math:`\Gamma
\Vdash^v_1 u : A \relevance{\irrelevant} / \mathcal{G} / \mathcal{A}`,
so the all but the equality are trivially available.

We therefore seek to establish a lemma of proof irrelevance for the
valid equality: if :math:`\Gamma \Vdash^v_{\nicel} t : A
\relevance{\irrelevant} / \mathcal{G} / \mathcal{A}` and :math:`\Gamma
\Vdash^v_{\nicel} u : A \relevance{\irrelevant} / \mathcal{G} / \mathcal{A}`
then :math:`\Gamma \Vdash^v_{\nicel} t = u : A \relevance{\irrelevant} /
\mathcal{G} / \mathcal{A}`.

This is trivial by definition of validity from proof irrelevance of
the logical relation:

.. inference:: Proof-Irrelevance-Logical

   \mathcal{A} :: \Gamma \Vdash_{\nicel} A \relevance{\irrelevant}
   \Gamma \Vdash_{\nicel} t : A \relevance{\irrelevant} / \mathcal{A}
   \Gamma \Vdash_{\nicel} u : A \relevance{\irrelevant} / \mathcal{A}
   --------------------------------------------------------------------
   \Gamma \Vdash_{\nicel} t ≡ u : A \relevance{\irrelevant} / \mathcal{A}

This final lemma is proven by induction on :math:`\mathcal{A}`

- :math:`A` is neither reducibly a universe, or reducibly :math:`\Nat`
  as these are at the wrong relevance.
- if :math:`A` is reducibly the empty type or a neutral type, we just
  need to use proof irrelevance for the generic equality on neutral
  terms.
- :math:`A` is reducible as a type from a lower level than
  :math:`\nicel` we recurse onwards.

This leaves reducible product types as the interesting case. We have
:math:`A \Rightarrow^* \Pi F \relevance{r_F} \tri G`, reductions :math:`t
\Rightarrow f` and :math:`u \Rightarrow g`, and knowing that :math:`f`
and :math:`g` under any well-typed weakening are reducible when
applied to a reducible argument.

We need to establish that :math:`f` and :math:`g` are reducibly equal
under any weakening when applied to a reducible argument. This is done
by proof irrelevance, recursing on the proof that :math:`G` is
reducible.

Then we prove that :math:`f` and :math:`g` are related by the generic
equality, using :math:`\eta` of the generic equality, :math:`f\; i_0`
and :math:`g\; i_0` being reducibly equal as we just showed.

.. _relevance-unicity:

Uniqueness of Relevance
+++++++++++++++++++++++

We don't have :ref:`type-unicity` due to our un-annotated lambdas. For
instance the identity term :math:`\lambda\ i_0` has type both
:math:`\Nat \rightarrow \Nat` and :math:`Univ_\relevant \rightarrow
Univ_\relevant`.

We do have uniqueness of relevance:

.. inference:: Uniq-Relevance

   \Gamma \vdash A \relevance{r}
   \Gamma \vdash A \relevance{r'}
   ------------------------------
   r = r'

.. note::

   Unicity of relevance is not used in the proof of decidability of
   conversion.

The proof is not especially interesting. One factor for this is that,
due to the lack of large universes, being convertible to a universe is
the same as being syntactically equal to it.

.. _decidable-proof:

Algorithmic Equality and Proof Irrelevance
++++++++++++++++++++++++++++++++++++++++++

Definition of algorithmic equality
**********************************

We need to modify the algorithmic equality on neutrals to have proof
irrelevance, starting from

.. code-block:: agda

  -- Neutral equality.
  data _⊢_~_↑_ (Γ : Con Term) : (k l A : Term) → Set where
    var-refl    : ∀ {x y A}
                → Γ ⊢ var x ∷ A
                → x PE.≡ y
                → Γ ⊢ var x ~ var y ↑ A
    app-cong    : ∀ {k l t v F G}
                → Γ ⊢ k ~ l ↓ Π F ▹ G
                → Γ ⊢ t [conv↑] v ∷ F
                → Γ ⊢ k ∘ t ~ l ∘ v ↑ G [ t ]
    natrec-cong : ∀ {k l h g a₀ b₀ F G}
                → Γ ∙ ℕ ⊢ F [conv↑] G
                → Γ ⊢ a₀ [conv↑] b₀ ∷ F [ zero ]
                → Γ ⊢ h [conv↑] g
                     ∷ Π ℕ ▹ (F ▹▹ F [ suc (var 0) ]↑)
                → Γ ⊢ k ~ l ↓ ℕ
                → Γ ⊢ natrec F a₀ h k ~ natrec G b₀ g l ↑ F [ k ]
    Emptyrec-cong : ∀ {k l F G}
                  → Γ ⊢ F [conv↑] G
                  → Γ ⊢ k ~ l ↓ Empty
                  → Γ ⊢ Emptyrec F k ~ Emptyrec G l ↑ F

(where :agda:`Γ ⊢ t [conv↑] v ∷ F` is the non-neutral algorithmic
equality, and :agda:`PE.≡` is syntactic equality)

We could just add relevance marks and a rule

.. code-block:: agda
  :dedent: 0

    irrelevance : ∀ {k l A}
                → Neutral k
                → Neutral l
                → Γ ⊢ k ∷ A ^ %
                → Γ ⊢ l ∷ A ^ %
                → Γ ⊢ k ~ l ↑ A ^ %

However there are then 2 ways to prove equalities for some terms: by
congruence and by irrelevance. This redundancy adds more cases to our
proofs, especially when they involve 2 neutral equality premises (like
transitivity or decidability). Instead, we separate neutral equality
into its relevant part (inhabited by congruence as previously) and its
irrelevant part (inhabited only by irrelevance). As such we get the
following definition (where :agda:`↓%` is :agda:`↓` specialised to
irrelevant terms):

.. _neutr-eq-def:

.. code-block:: agda

  -- Neutral equality.
  data _⊢_~_↑!_ (Γ : Con Term) : (k l A : Term) → Set where
    var-refl : ∀ {x y A}
             → Γ ⊢ var x ∷ A ^ !
             → x PE.≡ y
             → Γ ⊢ var x ~ var y ↑! A
    app-cong : ∀ {k l t v F rF G}
             → Γ ⊢ k ~ l ↓! Π F ^ rF ▹ G
             → Γ ⊢ t [conv↑] v ∷ F ^ rF
             → Γ ⊢ k ∘ t ~ l ∘ v ↑! G [ t ]
    natrec-cong : ∀ {k l h g a₀ b₀ F G}
                → Γ ∙ ℕ ^ ! ⊢ F [conv↑] G ^ !
                → Γ ⊢ a₀ [conv↑] b₀ ∷ F [ zero ] ^ !
                → Γ ⊢ h [conv↑] g
                     ∷ Π ℕ ^ ! ▹ (F ^ ! ▹▹ F [ suc (var 0) ]↑) ^ !
                → Γ ⊢ k ~ l ↓! ℕ
                → Γ ⊢ natrec F a₀ h k ~ natrec G b₀ g l ↑! F [ k ]
    Emptyrec-cong : ∀ {k l F G}
                  → Γ ⊢ F [conv↑] G ^ !
                  → Γ ⊢ k ~ l ↓% Empty
                  → Γ ⊢ Emptyrec F k ~ Emptyrec G l ↑! F

  record _⊢_~_↑%_ (Γ : Con Term) (k l A : Term) : Set where
    inductive
    constructor %~↑
    field
      neK : Neutral k
      neL : Neutral l
      ⊢k : Γ ⊢ k ∷ A ^ %
      ⊢l : Γ ⊢ l ∷ A ^ %

  data _⊢_~_↑_^_ (Γ : Con Term)
    : (k l A : Term) → Relevance → Set where
    ~↑! : ∀ {k l A} → Γ ⊢ k ~ l ↑! A → Γ ⊢ k ~ l ↑ A ^ !
    ~↑% : ∀ {k l A} → Γ ⊢ k ~ l ↑% A → Γ ⊢ k ~ l ↑ A ^ %

.. note::

   It may work just as well to have a single type for both relevant
   and irrelevant neutral equality, separating the relevances only by
   restricting the congruence and variable constructors to relevant
   terms. However I didn't experiment with this alternative.

.. note::

   Since we don't add an irrelevance rule for the non-neutral generic
   equality, the decision algorithm invoked on irrelevant terms
   inhabits the conversion by reducing the terms and :math:`\eta`
   expanding functions before invoking irrelevance. If we care about
   not doing extra work (for instance if we want to extract the
   decision algorithm) we would need to add such a shortcut rule.

Most properties are trivially preserved. For instance soundness of
neutral equality :agda:`soundness~↑ : ∀ {k l A rA Γ} → Γ ⊢ k ~ l ↑ A ^
rA → Γ ⊢ k ≡ l ∷ A ^ rA` has just 1 new case to consider where
:agda:`Γ ⊢ k ~ l ↑ A ^ rA` is between irrelevant terms, so inhabited
by :agda:`~↑%` applied to some proofs that :agda:`k` and :agda:`l`
irrelevantly inhabit :agda:`A`, so we can just apply the proof
irrelevance rule of the typing judgments.

Congruence of Algorithmic Equality
**********************************

We need to show that congruence is admissible for equality of neutrals
at any relevance (in order to show that it's part of a generic
equality, considering it isn't trivial by constructor after the
relevance separation).

This needs a small amount of reasoning about substitutions. Consider
the case of congruence for applications:

.. code-block:: agda

  app-cong′ : ∀ {Γ k l t v F rF G rG}
            → Γ ⊢ k ~ l ↓ Π F ^ rF ▹ G ^ rG
            → Γ ⊢ t [conv↑] v ∷ F ^ rF
            → Γ ⊢ k ∘ t ~ l ∘ v ↑ G [ t ] ^ rG

The relevant case :agda:`rG = !` is trivial by constructor
:agda:`app-cong`. In the irrelevant case, we need to show that
:agda:`k ∘ t` and :agda:`l ∘ v` both inhabit :agda:`G [ t ]`. We can
show this in 2 ways:

- by soundness, :agda:`Γ ⊢ k ≡ l ∷ Π F ^ rF ▹ G ^ %` and :agda:`Γ ⊢ t
  ≡ v ∷ F ^ rF`. By syntactic validity, :agda:`Γ ⊢ k ∷ Π F ^ rF ▹ G ^
  %` (and so on for the other terms), so :agda:`Γ ⊢ k ∘ t ∷ G [ t ] ^
  %` and :agda:`Γ ⊢ l ∘ v ∷ G [ v ] ^ %`. Then we need a lemma showing
  that substituting convertible terms :agda:`t` and :agda:`v` in
  :agda:`G` produces convertible results.

- alternatively, we use the congruence rule of the object type theory
  on the conversion proofs gotten by soundness, proving :agda:`Γ ⊢ k ∘
  t ≡ l ∘ v ∷ G [ t ] ^ %`. Then by syntactic validity both sides have
  type :agda:`G [ t ]`: the syntactic validity lemma invokes the
  substitution lemma for us.

The second method (invoking soundness, congruence in the object theory
and finally syntactic validity) requires less thought to extend to new
neutral term formers such as :agda:`natrec` compared to building a
ad-hoc reasoning for each.

Decidability of Algorithmic Equality
************************************

Our main statement is

.. code-block:: agda

  decConv↑ : ∀ {A B r Γ Δ}
           → ⊢ Γ ≡ Δ
           → Γ ⊢ A [conv↑] A ^ r → Δ ⊢ B [conv↑] B ^ r
           → Dec (Γ ⊢ A [conv↑] B ^ r)
  decConv↑Term : ∀ {t u A r Γ Δ}
               → ⊢ Γ ≡ Δ
               → Γ ⊢ t [conv↑] t ∷ A ^ r
               → Δ ⊢ u [conv↑] u ∷ A ^ r
               → Dec (Γ ⊢ t [conv↑] u ∷ A ^ r)

We decide conversion between types at the same relevance, and between
terms at the same type and relevance. We use the reflexivity
hypotheses to structure our recursion.

.. note:: About the context equality hypothesis:

   The 2 sides do not live in the same context, instead we require their
   contexts to be have pointwise convertible terms:

   .. code-block:: agda

      -- Equality of contexts.
      data ⊢_≡_ : (Γ Δ : Con Term) → Set where
        ε : ⊢ ε ≡ ε
        _∙_ : ∀ {Γ Δ A B r} → ⊢ Γ ≡ Δ
            → Γ ⊢ A ≡ B ^ r
            → ⊢ Γ ∙ A ^ r ≡ Δ ∙ B ^ r

   Using contexts equal up to conversion makes the comparison of product
   types easier: the reflexivity hypotheses for the codomains are in
   contexts extended by different domains, but once we have decided
   conversion for the domains we know that the contexts are equal.

When comparing neutrals terms, even when the terms have a common type
the subterms may have different types. For instance we may be
comparing applications of a variable of type :math:`\Empty → \Nat` and one
of type :math:`\Nat → \Nat`: both applications have type :math:`\Nat`. This is
troublesome when comparing irrelevant terms, as they are equal exactly
when their types are equal so we would need to complicate the
recursion to go back to comparing types.

Thankfully our separation of neutral equality between the relevant and
the irrelevant worlds ensures that we only need to compare irrelevant
terms at the same type. For instance, :agda:`Emptyrec-cong` (in the
:ref:`definition of neutral equality <neutr-eq-def>`) involves
irrelevant terms which are always at type :math:`\Empty`, and since
they are irrelevant there will be no further recursion.

Necessary Conditions: Why Booleans are not Irrelevant
+++++++++++++++++++++++++++++++++++++++++++++++++++++

We may wonder where exactly the proof would break down if we tried to
change the theory in an inconsistent or undecidable way, for instance
adding an irrelevant type of booleans.

The critical part is the fundamental lemma:

- In the case for congruence of the eliminator for booleans, we need
  that applying the eliminator to reducibly equal booleans produces
  reducibly equal results. To do this, we define reducible equality on
  booleans such that two terms are equal when they both reduce to the
  same constructor or to neutral terms.

- With the above definition for reducible equality, we do not have
  proof irrelevance for the logical relation.

.. _sec-limitations:

Limitations
+++++++++++

The object type theory is significantly weaker than the theories
implemented in Coq and Agda, so we should consider the potential
impact of these differences.

.. lots of "seems likely", it would be nice to substantiate a bit
.. somehow

.. todo? "I don't have the time" -> something else?

- Impredicativity of the irrelevant universe: the above proof method
  is unsuited to dealing with impredicativity.

- Universes: we only have 1 level of universes, which means being
  convertible to a universe is the same as being syntactically equal
  to it. Adding more universes would require fixing the proofs where
  this is used, especially in the definition of a type being reducibly
  equal to a universe. The levels used for the logical relation would
  also change (there is one for each universe, and a top level).
  Universes and proof relevance are related so this is something to be
  aware of (most importantly, since we can't deal with impredicativity
  we would need an irrelevant universe as well as a relevant one at
  each level), but generalizing the proofs seems quite feasible.

- Inductive familes and sigma types with definitional eta
  extensionality: this seems orthogonal to the issue of proof
  relevance and as such should not present any particular issues.
  Induction-recursion is a crucial feature of the meta-theory used in
  the proof, and as such would probably run into Gödel incompleteness
  if we tried to add it to the object theory. It is unclear whether
  induction-induction could be made to work.

- Type annotations on lambda abstractions: we can prove decidability
  of conversion, but not decidability of typechecking as the lack of
  type annotations on lambda abstractions can be used to encode
  undecidable unification problems. However a type theory with
  annotated lambdas may be translated to the un-annotated type theory
  while preserving head reductions, so this is not a problem.

- Relevance marks: the type theory implemented in Coq has relevances
  marks in contexts and for product type domains as in the object
  theory, and also a few more (notably the domain of lambda
  abstractions and for case eliminations). However they are not
  visible in the user syntax: by relevance unicity they are inferrable
  data. The translation between type theories with and without
  relevance marks could be more carefully described, but I don't have
  the time.

- Definitional uniqueness of identity proofs: having an irrelevant
  identity type changes the reduction significantly, such that it is
  difficult or maybe impossible to adapt this proof for it. Notice for
  instance that ``eq_rect x P v y e`` is in weak head normal form if
  and only if ``x`` is *not* convertible to ``y``, and as such weak
  head normal form is not a simple syntactic predicate.

  .. todo? abel asks "Also when eq_rect is restricted to SProp as motive?"
     but this is just squash of relevant eq, not sure what to say

- Box and squash: neither require anything fundamentally new from the
  logical relation. Squash is interesting in that it adds irrelevant
  terms which do not reduce to lambdas or neutral terms (such as
  ``squash 0``), so we would need generic equalities to have proof
  irrelevance even on non-neutrals. Extending the proof to allow
  squash may also increase the understanding of how exactly adding
  irrelevant booleans would break it. However I don't have the time.

Canonicity and irrelevant axioms
--------------------------------

.. note:: The following only holds when we do *not* have definitional UIP.

First we prove a lemma:

**Inconsistency from neutral terms**: if there is a neutral term of
relevant type in a context |Gamma| containing only irrelevant
variables, then that context is inconsistent.

   **Proof:**

   By induction on the term:

   - It cannot be a variable or an elimination of ``Squash`` since it
     is a relevant term.
   - If it is an elimination from an inductive type (which are all
     relevant), |Box| or an application, then the head is a smaller
     relevant neutral term also in |Gamma|, so we recurse on it.
   - If it is an elimination from the empty type then the head is a
     term in the empty type in |Gamma|.


This gives us **Canonicity in irrelevant contexts**: given a term
:math:`t` with a relevant type in a consistent context |Gamma| which
contains only irrelevant variables, :math:`t` reduces to an
introduction form. For instance if :math:`t` is in the type natural
numbers, then :math:`t` reduces to some :math:`S^n 0`.

   **Proof:**

   By normalisation, either :math:`t` reduces to an introduction form
   or to a neutral term. In the later case, |Gamma| must be
   inconsistent.

For instance this means the axiom of proof irrelevant double negation
elimination :math:`\forall P:\SProp, ((P \ra Empty) \ra Empty) \ra P`
preserves canonicity.

.. _compare-alt-irr:

Comparison With Other Irrelevance Systems
-----------------------------------------

Abel and Scherer's Irrelevant Intensional Type Theory
:cite:`Abel2012OnIA` directs definitional irrelevance around
applications: applications can be relevant :math:`t ∘ u` or irrelevant
:math:`t \div u`, irrelevant applications are equal regardless of the
arguments values. To form an irrelevant application, the head must be
an irrelevant function of type :math:`Π (x \div A), B`, and such
functions may be formed by irrelevant lambda abstractions :math:`λ (x
\div A), t`. Such irrelevant binders come with restrictions: the
variable is introduced irrelevantly to the context producing :math:`Γ,
x \div A`. Irrelevant variables may not be directly used, but when
typing the argument of an irrelevant application all variables of the
context are made relevant, and therefore accessible.

Squash types may also be expressed in terms of irrelevant arguments by
allowing them in the type of constructors of inductive types: then the
squash has a unique constructor of type :math:`Π (x \div A), \Squash\
A`. Note that such a type has only propositional proof irrelevance
(behaving more like :math:`\tBox\ (\Squash\ A)` according to our
definitions).

.. todo? I swear I saw some "weak pair" type explaining this in a
   related paper but can't find it again

It seems possible to translate from a theory with irrelevant arguments
to one with |SProp|, by translating bindings :math:`x \div A` to
bindings :math:`x : \Squash\ A`. Then the translation of irrelevant
application must unsquash all irrelevant variables from the context
before squashing the translated argument. For instance if the context
has one irrelevant variable :math:`y`:

.. math::

   [ t \div u ] = [t] ∘ (\Match\ y\ \with\ \mathtt{squash}\ y \Rightarrow
                                           \mathtt{squash}\ [u]\ \kwend{})

In this example :math:`y` is shadowed inside the match.

A full investigation of such a translation has not yet been done.


In the other direction, we need to deal with quantification over
|SProp| somehow while preserving conversions. An interesting exercise
is translating the statement of proof irrelevance using Leibnitz equality:

.. math::

   Π (P : \SProp) (Q : P → \Type) (x\ y : P), Q\ x → Q\ y

Suppose we translate |SProp| to a relevant universe, and keep
irrelevant bindings and applications, then we get

.. math::

   Π (P : \Type) (Q : Π (\_ \div P), \Type) (x\ y \div P), Q \div x → Q \div y

A function producing a value in an irrelevant type would be translated
to a regular (relevant) function, but all uses would be translated to
irrelevant applications. This seems like a sensible output but I have
not checked that it works in general.

In this sketch of a translation, we rely on the lack of cumulativity
from irrelevance to relevance in order to decide how to translate
bindings and applications. What does this mean for translations of
cumulative theories?

We may also compare with Miquel's "Implicit Calculus of Constructions"
:cite:`Miquel_2001`: in this system, irrelevant function types may use
the introduced variable relevantly in the codomain, such that
irrelevant bindings are only introduced by lambda abstractions. This
additional power indicates that it is stronger than simply having a
universe of strict propositions.
