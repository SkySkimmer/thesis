Dependent Type Theory
---------------------

Terms, Types and Contexts
+++++++++++++++++++++++++

Type theory is one way to describe the rules of mathematical
constructions and proofs in a precise way. In type theory, we
manipulate objects called "terms" which each come with a "type": we
write :math:`⊢ t : A` for a term :math:`t` with type :math:`A`. Types
specify the meaning of the term: it can be a natural number, a
function, etc. Types can also correspond to mathematical propositions:
we may have types for equality between terms, negation, etc. The type
of functions between two propositions can then be interpreted as an
implication from the first proposition to the other, and the terms
inhabiting that type are the proofs of such implications.

Note that types and terms have the same grammar: types are seen as a
special subset of terms.

In order to be able to define functions, we need a way to introduce a
fresh term corresponding to the argument. In prose, we may say
something like "the average of :math:`x` and :math:`y` is
:math:`\frac{x + y}{2}`" to define the averaging function.

In type theory we keep track of the introduced terms using a
"context": a list of variables each with their type. Then the
statements of the type theory (also called "judgments") look like
:math:`Γ ⊢ t : A` meaning ":math:`t` has type :math:`A` under context
:math:`Γ`", with :math:`Γ = x₁ : A₁, ..., xₙ : Aₙ`.

In order to make this work, we need 2 new judgments: :math:`⊢ Γ` for
":math:`Γ` is a well-formed context" and :math:`Γ ⊢ A` for ":math:`A`
is a well-formed type under context :math:`Γ`". Our 3 judgments are
mutually defined, for now we have

.. inference:: Ctx-empty: The empty context is well-formed.

   ----------
   ⊢\;

.. inference:: Ctx-extend: Contexts can be extended using a well-formed type.

   Γ ⊢ A
   x\textrm{ free in }Γ
   -------------
   ⊢ Γ, x : A

.. note::

   In the rest of this thesis we will rename variables implictly to
   keep the side condition ":math:`x` free in :math:`Γ`" true.

.. inference:: Var: variables from a well-formed context get their type from it.

   ⊢ Γ
   x : A ∈ Γ
   ------------
   Γ ⊢ x : A

Functions
+++++++++

Now we are ready to describe how functions work. First, the type for
functions (also called "product type"):

.. inference:: Pi-type

   Γ ⊢ A
   Γ, x : A ⊢ B
   --------------
   Γ ⊢ Π (x:A), B

:math:`Π (x:A), B` is the type of functions from :math:`A` to
:math:`B`. Notice that :math:`B` lives in the context extended
by :math:`x : A`: this dependency is why the type theory is called
"Dependent type theory". From a logical point of view, :math:`Π`
can be seen as universal quantification :math:`∀`.

For instance, if we have a type :math:`x = x` of reflexivity of
equality at a variable :math:`x`, then :math:`Π (x:A), x = x` is the
type of reflexivity of equality for type :math:`A`.

In :math:`Π (x:A), B`, type :math:`A` is the "domain" and :math:`B`
the "codomain" or output type. If :math:`x` does not appear in
:math:`B`, this non-dependent function type may also be written
:math:`A → B`, with the admissible rule

.. inference:: Arrow-type

   Γ ⊢ A
   Γ ⊢ B
   ---------
   Γ ⊢ A → B

Then we can create functions using lambda abstractions:

.. _infer-lambda:

.. inference:: Lambda

   Γ ⊢ A
   Γ, x : A ⊢ e : B
   ---------------------------
   Γ ⊢ λ (x:A), e : Π (x:A), B

In the term :math:`λ (x:A), e` and the type :math:`Π (x:A), B`, we say
that the variable :math:`x` is "bound" in :math:`e` and :math:`B`.
Variables which are not bound are called "free". We consider all terms
and types up to renaming of bound variables (alpha equivalence).

From the logical point of view, when :math:`B` does not depend on
:math:`x`: if assuming a proof of :math:`A` gives us a proof of
:math:`B`, then we have a proof that :math:`A → B`.

Functions can be used by applying them to an argument. To get the type
of such an application, we first need to define substitution:
:math:`a[x:=b]` is :math:`a` (which may be a term or a type) where
variable :math:`x` is substituted by term :math:`b`. The precise
definition of how substitution interacts with binders is subtle and
unchanged in this thesis, please refer to your lambda calculus
textbook. Informally, it means we replace occurences of :math:`x` by
:math:`b` in :math:`a`.

Then we can define application:

.. _infer-app:

.. inference:: App

   Γ ⊢ f : Π (x:A), B
   Γ ⊢ v : A
   --------------------
   Γ ⊢ f ∘ v : B[x:=v]

In the case where :math:`f` is a non-dependent function, this becomes

.. inference:: App-Arrow

   Γ ⊢ f : A → B
   Γ ⊢ v : A
   -------------
   Γ ⊢ f ∘ v : B

From the logical point of view: if we have a proof of :math:`A → B`
and a proof of :math:`A`, then we have a proof of :math:`B`.

Usually we write application without the :math:`∘` symbol: :math:`f\;
v` instead of :math:`f ∘ v`.

We have a dichotomy between the :math:`λ` terms and the application
terms: the first construct values in function types, and the second
make use of such values. We call the first kind of term "introduction"
terms, and the second "elimination" terms.

Computation and Conversion
++++++++++++++++++++++++++

Computation and conversion let us identify the application of a lambda
term with the body where the argument is substituted in: :math:`(λ
(x:A), e) ∘ v = e[x:=v]`. We will extend this as we add more
introduction and elimination terms to our theory.

We add 3 new judgments, defined simultaneously with the term, type
and context judgments:

- :math:`Γ ⊢ A ≤ B` "type :math:`A` is a subtype of :math:`B` under
  context :math:`Γ`".
- :math:`Γ ⊢ A ≡ B` "types :math:`A` and :math:`B` are convertible
  under context :math:`Γ`".
- :math:`Γ ⊢ t ≡ u : A` "terms :math:`t` and :math:`u` are convertible
  with type :math:`A` under context :math:`Γ`".

and the conversion rule:

.. inference:: Conversion

   Γ ⊢ t : A
   Γ ⊢ A ≤ B
   ----------
   Γ ⊢ t : B

The two conversion judgments are congruences: they are equivalence
relations, and if 2 terms or types are identical except for
convertible subterms (or subtypes) under :math:`Γ` extended by
whatever variables are bound around the subterms, then they are
convertible under :math:`Γ`. In other words we have rules such as

.. inference:: Lambda-congr

   Γ ⊢ A
   Γ, x : A ⊢ e₁ ≡ e₂ : B
   ------------------------------------------
   Γ ⊢ λ (x:A), e₁ ≡ λ (x:A), e₂ : Π (x:A), B

Conversion also contains the beta reduction:

.. _beta-conv:

.. inference:: Beta-conv

   Γ, x : A ⊢ e : B
   Γ ⊢ v : A
   ----------------------------------------
   Γ ⊢ \left(λ (x:A), e\right) ∘ v ≡ e[x:=v] : B[x:=v]

We may also add eta expansion for functions:

.. inference:: Eta-expand

   Γ ⊢ f : Π (x:A), B
   ---------------------------------
   Γ ⊢ f ≡ λ (x:A), f ∘ x : Π (x:A), B

The subtyping judgment is transitive and reflexive, and implied by
conversion. It is also compatible with product types in the following
way:

.. inference:: Pi-subtype

   Γ ⊢ A
   Γ, x : A ⊢ B₁ ≤ B₂
   ----------------------------
   Γ ⊢ Π (x:A), B₁ ≤ Π (x:A), B₂

.. note::

   We could also have contravariance in the domain:

   .. inference:: Pi-subtype-contravariant

      Γ ⊢ A₂ ≤ A₁
      Γ, x : A₁ ⊢ B
      -----------------------------
      Γ ⊢ Π (x:A₁), B ≤ Π (x:A₂), B

   but this is incompatible with some models like the set model.

Until we have :ref:`univ-cumul` the separation between subtyping and
conversion is not useful, so we may act as though we only have
conversion.

Computation (also called reduction) is not a simultaneously defined
judgment, rather it is an interesting property. It is generated by the
beta reduction (i.e. :math:`\left(λ\ x:A, e\right) ∘ v`
reduces to :math:`e[x:=v]`), transitivity and compatibility with the
structure of terms. New type formers also come with additional
reductions, for instance inductive types add reductions for the
eliminators applied to constructors.

Because reduction is directed, it is possible to ask what a term reduces to.
The rules of the type theory should ensure that there is a unique
normal form (i.e. a term which does not reduce further), and once the
necessary concepts are established we will have results such as
":math:`2 + 2` reduces to :math:`4`". Reduction is included in
conversion.

Metatheoretical Properties
++++++++++++++++++++++++++

The work of this thesis is to extend type theory while maintaining
nice properties. To do this, first we need some nice properties to
maintain.

Weakening and substitution for judgments
========================================

Weakening means if we have a well-formed term or type in some context,
we may freely extend the context with variables of well-formed types.
In other words, the following rule is admissible:

.. inference:: Wk

   Γ ⊢ t : A
   Γ ⊢ B
   -----------------
   Γ, x : B ⊢ t : A

(Remember that by convention we have picked an :math:`x` which does
not appear in :math:`Γ`, and so is not free in :math:`t` or :math:`A`.)

We may also substitute a variable by an appropriately typed term:

.. inference:: Subst

   Γ, x : A, Δ ⊢ t : T
   Γ ⊢ v : A
   --------------
   Γ, Δ[x:=v] ⊢ t[x:=v] : T[x:=v]

Subject Reduction
=================

Reducing a term should not change its type: if :math:`Γ ⊢ t : A` and
:math:`t` reduces to :math:`u` then :math:`Γ ⊢ u : A`.

Note that when subtyping is not the same as conversion, :math:`u` may
have more types than :math:`t`.

Strengthening
=============

If :math:`x` does not appear free in :math:`Δ`, :math:`t` and
:math:`A`, and if :math:`Γ, x : T, Δ ⊢ t : A` then :math:`Γ, Δ ⊢ t :
A`. In other words we can remove unused variables from the context.

Syntactic Validity
==================

Syntactic validity means that the semantic sub-judgments of a valid
judgment are also valid. For instance, if :math:`Γ ⊢ t : A` then
:math:`⊢ Γ` and :math:`Γ ⊢ A`.

.. _inversion:

Inversion of Judgments
======================

Inversion of a judgment lets us get judgments about the subterms
involved.

- **inversion of context extension:** if :math:`⊢ Γ, x : A` then
  :math:`Γ ⊢ A` (and :math:`x` is free in :math:`Γ`).

  .. note:: we can also get :math:`⊢ Γ`, but this is redundant with
            :math:`Γ ⊢ A` and syntactic validity so we will omit other
            such side results for the other inversions.

- **inversion of variables:** if :math:`Γ ⊢ x : A` then :math:`x : A ∈
  Γ`.

- **inversion of product types:** if :math:`Γ ⊢ Π (x:A), B` then
  :math:`Γ, x : A ⊢ B`.

- **inversion of lambda abstractions:** if :math:`Γ ⊢ λ (x:A), e : T`
  then there exists :math:`B` such that :math:`Γ, x : A ⊢ e : B` and
  :math:`Γ ⊢ Π (x:A), B ≤ T`.

- **inversion of application:** if :math:`Γ ⊢ f ∘ v : T` then there
  exist :math:`A`, :math:`x` and :math:`B` such that :math:`Γ ⊢ f : Π
  (x:A), B` and :math:`Γ ⊢ v : A` and :math:`Γ ⊢ B[x:=v] ≤ T`.

There are also inversions for the conversion judgments, which look
like injectivity rules. For instance:

- **injectivity of product types:** if :math:`Γ ⊢ Π (x:A₁), B₁ ≡ Π
  (x:A₂), B₂` then :math:`Γ ⊢ A₁ ≡ A₂` and :math:`Γ, x : A₁ ⊢ B₁ ≡ B₂`
  (the choice of :math:`A₁` to extend :math:`Γ` is unimportant, by substitution)

Note that we only have injectivity for the introduction forms. With
the elimination forms, reduction may get involved. For instance,
:math:`⊢ 0 + 0 ≡ 0 * 1` even though :math:`+` and :math:`*` are not
convertible, and neither are :math:`0` and :math:`1` (where :math:`0 +
0` is the infix notation for application of addition to :math:`0` and
:math:`0`, and so on for :math:`0 * 1`).

We can generalize this issue by introducing the concepts of head
normal forms and neutral terms.

- each elimination form has a subterm which is considered the "head".
  For application :math:`f ∘ v` this is :math:`f`.

- a term is neutral if it is a variable or if it is an elimination
  whose head is neutral.

- a term or type is in head normal form if it is neutral or if it is
  one of the introduction forms (currently :math:`Π` or :math:`λ`). If
  it is a lambda abstraction :math:`λ (x:A), e` the body :math:`e`
  must also be head normal (without this condition we would define
  weak head normal form).

Then each term former is injective for conversion on head normal
terms: introduction forms are always injective, elimination forms are
injective when the head is neutral, and if 2 variable terms are
convertible they are the same variable.

Head normal terms built from different term formers are never
convertible, unless one of them is a lambda. In that case we need to
eta expand the other: when :math:`f` and :math:`e` are in HNF,
:math:`Γ ⊢ f ≡ λ (x:A), e : X` if and only if :math:`Γ, x : A ⊢ f ∘ x
≡ e : B` (with :math:`B` gotten from inversion given :math:`Γ ⊢ λ
(x:A), e : X`).

.. note::

   Using head normal form instead of weak head normal form means that
   the above eta expansion only considers normal forms, without having
   to reduce :math:`e` after expanding. This is only a convenience for
   that remark, usually we talk about weak head normal forms and
   interleave reduction and inversion.

.. note::

   This last "no confusion" property will become weaker as we add
   types with definitional eta equalities like :ref:`Records
   <primitive-record>`, and of course when we add proof irrelevance.

.. _type-unicity:

Uniqueness of Typing
====================

When subtyping coincides with conversion, uniqueness of typing is
easily stated:

.. inference:: Uniq-type

   Γ ⊢ t : A
   Γ ⊢ t : B
   -----------
   Γ ⊢ A ≡ B

The situation when we have real subtyping is explained in
:ref:`univ-meta`.

Normalisation, Confluence and Canonicity
========================================

All terms reduces to a unique normal form. Equal terms have
syntactically equal normal forms, if we include eta expansion in the
reduction. Once we introduce proof irrelevance, this becomes syntactic
equality up to irrelevant subterms.

Canonicity means that in the empty context normal terms are built from
introduction forms (binders such as lambda abstractions will also
contain neutral subterms). Combined with inversion lemmas, this
implies for instance that every natural number term in the empty
context reduces to a finite number of successors applied to zero, i.e.
a natural number value.

.. note:: Normalisation vs Reducibility

   Normalisation is not equivalent to having all terms reduce to a
   weak head normal form. Consider for instance a system with natural
   numbers and some term ``x`` which reduces to ``S x``. The term ``S
   x`` is weak head normal, but ``x`` is not normal, and has no normal
   form.

Decidability
============

The above properties are in support of decidability of the various
judgments. Decidability makes it possible to implement proof assistants
so that we don't have to check our derivations by hand.

Consistency
===========

If we want to use our terms to represent proofs, we need to ensure
that not everything has a proof. As such we soon (in
:ref:`notable-inductives`) have a type :math:`\Empty` which is not
inhabited in the empty context.

Example: functions over natural numbers
+++++++++++++++++++++++++++++++++++++++

We add

- `ℕ` such that `⊢ ℕ`
- `0` such that `⊢ 0 : ℕ`
- `S` such that `Γ ⊢ S : ℕ → ℕ`

- `natrec (x. T) z s n` such that

  - `natrec (x. T) z s 0` reduces to `z`
  - `natrec (x. T) z s (S n)` reduces to `s n (natrec (x. T) z s n)`

  with the typing rule

.. nb messed up indentation to avoid horizontal overflow
.. inference:: natrec

   Γ, x : ℕ ⊢ T
   Γ ⊢ z : T
   Γ ⊢ s : Π (a:ℕ), T[x:=a] → T[x:=S\; a]
   Γ ⊢ n : ℕ
   -------------------------------------
   Γ ⊢ \natrec\; (x. T)\; z\; s\; n : T[x:=n]




.. note:: For now we have no way to abstract over types, but if we
          did we could present `natrec` as something whose type is a
          `Π` instead of being forced to treat `(x. T)` specially.

This is enough to define structurally recursive functions on natural
numbers. For instance, addition is `add := λ n m. natrec (x. ℕ) n
(λ a. S a) m` (with recursion on the second argument).

On the other hand, all our types are formed from `ℕ` and `→`: we have
no way to make a type be about a specific number `n`, and so we have
no way to prove properties about the functions we define inside the
type theory.

Let's introduce a type for equalities between natural numbers to help:

- `t = u` such that

  .. inference:: eq-type

     Γ ⊢ t : ℕ
     Γ ⊢ u : ℕ
     -----------
     Γ ⊢ t = u

- `refl` such that `⊢ refl : Π (x:ℕ), x = x`

The general form for eliminating equalities is slightly complicated,
so we wait until we explain inductive families to present it and for
the sake of this example we instead provide injectivity of successor
as a primitive:

- `S-inj` such that `⊢ S-inj : Π (x y:ℕ), x = y → S x = S y` and
  `S-inj a b (refl a)` reduces to `refl (S a)` (NB: when `S-inj a b
  (refl a)` is well typed `a` and `b` are convertible).

This is enough to show commutativity of `add` internally to our
theory. As a first step we show that `add 0 n = n`:

.. code-block:: text

   ⊢ λ (m : ℕ).
     natrec (n. add 0 n = n)
            (refl 0)
            (λ (m' : ℕ) (e : add 0 m' = m'). S-inj (add 0 m') m' e)
            m
     : Π (m : ℕ), add 0 m = m

Conversion is involved to make the arguments of `natrec` fit:

- `refl 0 : 0 = 0` needs to have type `add 0 0 = 0`. `add 0 0` reduces
  to `0` so this subterm has the right type.
- `S-inj (add 0 m') m' e : S (add 0 m') = S m'` needs to have type
  `add 0 (S m') = S m'`. `add 0 (S m')` reduces to `S (add 0 m')` so
  this also works.

The remainder of the commutativity proof is left as an exercise to the
reader.

With new types and terms, we need to update the definitions of neutral
and weak head normal forms:

- `natrec (x. T) z s n` is neutral when `n` is neutral.
- `S-inj a b e` is neutral when `e` is neutral.
- The type `nat` and equality types are weak head normal introduction
  forms (note that we have no way to define neutral types, and types
  have no head reductions).
- `0`, the applied successor `S n` and the applied reflexivity proof
  `refl a` are weak head normal introduction forms.

Then all terms have normal forms, and we have canonicity.

Our other desired properties are also preserved: this is a
well-behaved type theory.

The reduction rule of `S-inj` has only been useful for canonicity this
far: we have no way to eliminate proofs of equalities, so we don't
care about their values.

Proof assistants, especially Coq
--------------------------------

Proof assistants are computer programs which keep track of the details
of typing derivations for us, check them to make sure we didn't make a
mistake and help us produce them.

There are many proof assistants, not all based on dependent type
theory. In this thesis we focus on Coq, and also consider Agda and
Lean, all of which use dependent type theory. In the following short
presentation of Coq, you should assume that Agda and Lean have similar
capabilities.

We don't present the full power of Coq, only enough to understand the
code examples we use in the thesis.

A proof assistant carries a global context of checked proof terms
bound to names, in other words definitions. Coq has a basic command to
add a term as a definition:

.. coqtop:: reset in

   Definition some_def : nat := 0 + 0.

``0 + 0`` is syntactic sugar for the application ``Nat.add 0 0``,
where ``Nat.add`` is a previous definition (from Coq's standard
library).

We can also ask Coq to check a term without making a definition:

.. coqtop:: all

   Check 0 + 1.

(``1`` is syntactic sugar for ``S 0``)

Definitions reduce to their value. We can see what terms reduce to
using the following command:

.. coqtop:: all

   Compute 1 + some_def.

Coq has powerful facilities to let us avoid typing parts of terms.
Consider the polymorphic identity function

.. coqtop:: in

   Definition id (A:Type) (a:A) : A := a.

(the meaning of ``Type`` is explained in the next section, :ref:`universes`)

If we apply it as ``id A 0``, by inversion we must have ``A ≡ nat``.
We can indicate to Coq that it should automatically infer a subterm by
replacing it with a hole ``_``:

.. coqtop:: all

   Check id _ 0.

Such holes can be automatically inserted using the "implicit argument"
system:

.. coqtop:: all

   Arguments id {_} _.
   Check id 0.

Equivalently we could have written

.. coqdoc::

   Definition id {A} (a:A) := a.

Using curly brackets ``{}`` around an argument means it is implicit.
In the above ``Definition`` we omitted the types for ``A`` and for the
result of ``id``: Coq automatically replaced these omissions by holes
and solved them.

Coq syntax for the term formers we have already introduced:

- Product types: :math:`Π (x:A), B` is ``forall x : A, B``. The type
  of ``x`` may be omitted.

  :math:`A → B` is ``A -> B``.

- Lambda abstractions: :math:`λ (x:A), e` is ``fun x : A => e``. The
  type of ``x`` may be omitted.

- Applications: :math:`a ∘ b` is ``a b``.

We can only ask Coq for judgments about terms, not conversions, except
by crafting a term which depends on a conversion. For instance

.. coqdoc::

   Check (fun (f:A -> nat) (x:B) => f x).

succeeds if and only if :math:`B ≤ A`.

.. _universes:

Universes
---------

How to formalize a polymorphic concept? Consider the example of lists
whose elements all have the same type. We could introduce special type
and terms formers ``list A`` (which is a type when ``A`` is a type),
``nil A : list A``, ``cons A : A → list A → list A`` and some
eliminators. But derived notions about lists also need to depend on
the abstract type ``A``: shall we continue to introduce a new basic
term formers for the ``length`` function (such that ``length A : list
A → nat``)?

If only we had a type ``Type`` to be the type of types. Then we could
have ``list : Type → Type``, ``nil : forall A : Type, list A``,
``cons : forall A : Type, A → list A → list A`` and whatever
eliminator we like as (for now) the primitives providing lists, and
``length : forall A : Type, list A → nat`` is just some defined
notion.

A type of types (also called universe or sort) is itself a type, so it
sounds like it should be an element of itself. Sadly having ``Type :
Type`` is inconsistent (:cite:`Hurkens_1995`) so we must be a bit
smarter.

The usual solution, implemented in all of Coq, Agda and Lean is to
have a so-called universe hierarchy: we have not just one ``Type`` for
all types, but instead we have some ``Typeᵢ`` for varying ``i``. We
ensure that we never have ``Typeᵢ : Typeᵢ``, but we may have ``Typeᵢ :
Typeⱼ`` when ``i`` and ``j`` are different. In Coq ``Typeᵢ`` is
written ``Type@{i}``.

More general systems are possible, but for the purpose of this thesis
we will consider that universe indices are natural numbers, or special
(and so explained separately). The main point of this thesis is to
introduce and examine the behaviour of one such special universe.

.. note::

   In some cases we only have a finite number of universes, which
   corresponds to only allowing some natural numbers as universe
   indices. For instance in section :ref:`sprop-dec` we prove
   decidability of conversion for a type theory simplified to have
   only ``Type₀`` and our special universe, and no ``Type₁``. This
   means not all types belong to some universe.

With ``Type : Type`` the rules involving universes could be very
simple:

.. _univ-is-type:

.. inference:: univ-is-type

   ------------------
   \Gamma \vdash \Type

.. _univ-to-type:

.. inference:: univ-to-type

   \Gamma \vdash A : \Type
   ----------------------
   \Gamma \vdash A

.. _type-to-univ:

.. inference:: type-to-univ

   \Gamma \vdash A
   ----------------------
   \Gamma \vdash A : \Type

With a universe hierarchy, we can keep :ref:`univ-is-type
<univ-is-type>` and :ref:`univ-to-type <univ-to-type>` (annotating
``Type`` with an arbitrary universe index). However :ref:`type-to-univ
<type-to-univ>` must change: we can't use an arbitrary level there at
risk of going back to ``Type : Type``.

We adapt by adding a rule forming an inhabitant of a universe for
every rule forming a type. For now we only have 2 type formers: the
previously introduced product types, and the newly added universes.

Each universe contains the universes which have smaller indices:

.. inference:: univ-in-univ

   i < j
   ------------------------------
   \Gamma \vdash \Type_i : \Type_j

Note that this means ``Type₀`` (called ``Set`` in Coq) is slightly
special: it contains no universe. The types which it does contain are
called "small types". For now the only small types are built from
assumptions in the context, but for instance once we introduce natural
numbers they will be small types.

Product types belong to the maximum of the domain and codomain's
universes (when they exist).

.. inference:: Pi-in-univ

   \Gamma \vdash A : \Type_i
   \Gamma, x : A \vdash B : \Type_j
   ------------------------------------
   \Gamma \vdash \Pi (x:A). B : \Type_{max(i,j)}

When we have a universe for every natural number (such as in Coq),
every universe has a type (the successor universe), and from this we
can show that every type has a universe. We also have all product
types: :math:`max(i,j)` always exists.

.. _univ-cumul:

Universe Cumulativity
+++++++++++++++++++++

Universe cumulativity means that if ``A : Typeᵢ`` and ``i ≤ j`` then
we also have ``A : Typeⱼ``: every universe includes the smaller ones.

Coq has universe cumulativity, but other systems such as Agda and Lean
don't. Instead they have explicit lifting: functions ``liftᵢ,ⱼ : Typeᵢ
→ Typeⱼ`` for every ``i < j``, such that ``liftᵢ,ⱼ A`` is in bijection
with ``A`` (with cumulativity they would be convertible). Usually the
lift is provided by tweaking the formation rules of inductive types.

Universe Variables and Universe Polymorphism
++++++++++++++++++++++++++++++++++++++++++++

Consider the product of type ``A × B``. To avoid providing an
embedding from larger to smaller universes, we need to have :math:`× :
\Type_i → \Type_j → \Type_{max(i,j)}`, but which ``i`` and ``j`` are
these?

We can avoid picking by using abstract variables for ``i`` and ``j``,
keeping a record of the relations between variables (such as ``i <
j``) to ensure that we can always assign specific numbers to them and
get a well-typed judgment.

In fact in Coq, most universes are built from variables and ``Set``,
with successor and maximum expressions allowed only in the codomain of
the type of judgments (such as the type for ``×`` above).

If we have a type involving universe expressions (also called
"algebraic universes") it can be transformed into a type using only
variables by generating fresh variables. For instance we can generate
a fresh ``k`` such that ``i ≤ k`` and ``j ≤ k``, then :math:`× :
\Type_i → \Type_j → \Type_k` by cumulativity.

This is still not enough to get the flexibility we want: if we have
some type ``nat : Type₀``, by cumulativity ``nat × nat`` is well-typed
(:math:`0` being smaller than every natural, it can also be assumed
smaller than every variable) but its type is still
:math:`\Type_{max(i,j)}` even though a pair of natural numbers could
be a small type.

To fix this Coq provides universe polymorphism: constants and
inductive types may quantify over the universes and universe
constraints they use, and later be instantiated by different
universes. For instance:

.. coqtop:: none reset

   Set Printing Universes.

.. coqtop:: in

   Polymorphic Inductive prod@{a b} (A:Type@{a}) (B:Type@{b}) :=
     pair : A -> B -> prod A B.

.. coqtop:: all

   About prod.
   Check prod nat nat.

.. note::

   The ``prod`` type found in Coq's standard library does not use
   universe polymorphism, instead it uses "template polymorphism". It
   is a similar but more ad-hoc system which does not matter to this
   thesis.

.. _univ-prop:

Universe of Propositions
++++++++++++++++++++++++

Coq provides a special universe |Prop| whose elements are intended to
represent propositions, i.e. logical statements whose proof values are
of little interest. |Prop| was introduced in Coq version 4 (1987) to
help with extraction: Coq terms can be translated to terms in
non-dependent languages such as OCaml while erasing subterms whose
types are propositions. As such the rules for defining elements of
|Prop| are focused on ensuring that computation after erasure does not
depend on the value of the erased subterms.

|Prop| is parallel to ``Set`` in the hierarchy in that ``Prop :
Type₁``.

|Prop| is impredicative: product types are propositions whenever their
codomain is a proposition, regardless of the universe of the domain.

.. inference:: Pi-in-Prop

   \Gamma \vdash A
   \Gamma, x : A \vdash B : \Prop
   ----------------------------------
   \Gamma \vdash \Pi (x:A). B : \Prop

For consistency we must avoid having any universe belong to an
impredicative universe, so there is no ``Typeᵢ : Prop``.

On the other hand nothing stops us from having multiple impredicative
universes, and Coq used to have ``Set`` impredicative (it can still be
made impredicative with a command line option).

.. _univ-meta:

Metatheory of Universes
+++++++++++++++++++++++

Adding a universe hierarchy preserves our expected properties:
substitution for judgments, decidability, consistency, etc.

With cumulativity, uniqueness of typing becomes existence of a
principal type: if :math:`t` is typed in :math:`Γ` then there exists
:math:`A` such that :math:`Γ ⊢ t : A` and for all :math:`B`, if
:math:`Γ ⊢ t : B` then :math:`Γ ⊢ A ≤ B`. :math:`\Type_{u+1}` (if it
exists, for finite hierarchies) is the principal type for
:math:`\Type_u`.

Universes add a new introduction form for types.

We have new inversions related to universes:

- If :math:`Γ ⊢ t ≡ \Type_u`, then :math:`t` reduces to
  :math:`\Type_u` (with universe variables, :math:`t` reduces to
  :math:`\Type_v` for some :math:`v` equal to :math:`u`).

  If :math:`Γ ⊢ t ≤ \Type_u` (resp. :math:`Γ ⊢ \Type_u ≤ t`) then it
  reduces to :math:`\Type_v` with :math:`v ≤ u` (resp. with :math:`u ≤
  v`).

- All types of types are universes: if :math:`Γ ⊢ A : T` and :math:`Γ
  ⊢ A` then :math:`T` is convertible to some universe.

  If we have the full infinite hierarchy then every type has a
  universe: if :math:`Γ ⊢ A` then there exists :math:`u` such that
  :math:`Γ ⊢ A : \Type_u`.

- inversion for :math:`Π`: if a product type belongs to some universe,
  then domain and codomain belong to suitable universes.

Inductive Families
------------------

An inductive family :math:`\mathit{Ind}` is given by the following data:

- a context of "parameters" :math:`Γ_P = p₁ : P₁, ..., pₘ : Pₘ`.
- a context of "indices" :math:`Γ_I = i₁ : I₁, ..., iₙ : Iₙ`, which may
  depend on the parameters (i.e. :math:`Γ_P, i₁ : I₁, ..., iₖ : Iₖ ⊢
  Iₖ₊₁` for :math:`0 \leq k < n`). When there are no indices we may
  also say that :math:`\mathit{Ind}` is an inductive **type**.
- a universe :math:`u`.
- a list of "constructors" :math:`Γ_C = c₁ : C₁, ..., cₗ : Cₗ`. Each
  constructor type :math:`Cᵢ` may not depend on the other
  constructors, and may depend on the parameters and the inductive:
  :math:`Γ_P, Ind : Π\; Γ_P\; Γ_I, \Type_u ⊢ Cᵢ`.

  Additionally, each :math:`Cᵢ` must be of the form

  .. math::
    Π\; Γ_{X,i}, Ind\; Γ_P\; v_{i,1}\; ...\; v_{i,n}

  The codomain is the inductive applied to exactly the parameter
  variables, and then some arbitrary values for the indices.

  Using :math:`Γ_{X,i} = x_{i,1} : X_{i,1}, ..., x_{i,a_i} :
  X_{i,a_i}`, each :math:`X_{i,k}` must either not refer to
  :math:`\mathit{Ind}` (in which case it is called a "nonrecursive argument" of
  :math:`cᵢ`), or be of the form

  .. math::
     Π\; Γ_{Y,i,k}, Ind\; Γ_P\; w_{i,k,1}\; ...\; w_{i,k,n}

  i.e. a product type whose codomain is the inductive at some indices,
  and such that no variable of :math:`Γ_{Y,i,k}` and no :math:`w` refers to
  :math:`\mathit{Ind}`. In this case we say that :math:`\mathit{Ind}` occurs "strictly
  positively" in :math:`X_{i,k}` and :math:`x_{i,k}` is a "recursive
  argument" of :math:`cᵢ`.

Finally there is a condition on the universe: it must be greater or
equal than each of the universes of the types of the arguments.

Then :math:`⊢ Ind : Π\; Γ_P\; Γ_I, \Type_u` and for each :math:`i`,
:math:`⊢ cᵢ : Π\; Γ_P, Cᵢ`.

A type theory with inductive families also provides a way to eliminate
them, which among other things expresses that all inhabitants are
recursively built from the constructors. This can be done in 2 ways:

- Providing primitive elimination operators for each inductive family.
  The operator :math:`Ind\_elim_{u_T}` (universe polymorphic over
  :math:`u_T`) has type

  .. math::

     Π\; & Γ_P\; \left(T : Π\; Γ_I, Ind\; Γ_P\; Γ_I → \Type_{u_T}\right),\\
       & \left(Π\; Γ_{X,1}\; Γ_{R,1}, T\; v_{1,1}\; ...\; v_{1,n}\; (c₁\; Γ_P\; Γ_{X,1})\right) →\\
       & ... →\\
       & \left(Π\; Γ_{X,l}\; Γ_{R,l}, T\; v_{l,1}\; ...\; v_{l,n}\; (cₗ\; Γ_P\; Γ_{X,l})\right) →\\
       & Π\; Γ_I\; (z : Ind\; Γ_P\; Γ_I), T\; Γ_I\; z

  where :math:`Γ_{R,i}` contains an "induction hypothesis" variable
  for each recursive argument :math:`x_{i,k}` of :math:`c_i`. The type
  of the variable is then

  .. math::

     Π\; Γ_{Y,i,k}, T\; w_{i,k,1}\; ...\; w_{i,k,n}\; \left(x_{i,k}\; Γ_{Y,i,k}\right)

  In other words, in order to inhabit a type family :math:`T` (called
  the "motive" of the elimination) for all instantiations of the
  inductive, it suffices to provide functions inhabiting it for each
  constructor provided it is inhabited for the recursive arguments.

  When applied to a constructor of the inductive type, the elimination
  operator reduces to the function given for that constructor, applied
  to the constructor's arguments and to the eliminator applied to the
  recursive arguments.

  Using :math:`Elim = Ind\_elim_{u_T}\; Γ_P\; T\; F_1\; ...\; F_l`:

  .. math::

     & Elim\;
       v_{i,1}\; ...\; v_{i,n}\; \left(c_i\; \Gamma_P\; \Gamma_X\right)
     \equiv\\
     & F_i\; \Gamma_X
       \left(\lambda\; \Gamma_{Y,i,1},
         Elim\;
           w_{i,1,1}\; ...\; w_{i,1,n}\; \left(x_{i,1}\; \Gamma_{Y,i,1}\right)\right)\\
       & \hspace{1em} ...
       \left(\lambda\; \Gamma_{Y,i,a_i},
         Elim\;
           w_{i,a_i,1}\; ...\; w_{i,a_i,n}\; \left(x_{i,a_i}\; \Gamma_{Y,i,a_i}\right)\right)

- We can separate case distinction and recursion in 2 related
  primitive operations.

  First for case distinction we provide a "match" operator:

  .. coqdoc::

     match e as x in Ind _ Γ_I return T with
     | c₁ Γₓ,₁ => F₁
     ...
     | cₗ Γₓ,ₗ => Fₗ
     end

  (because unicode has no capital letter subscripts we use ``Γ_I``
  instead of :math:`Γ_I` and ``Γₓ,ᵢ`` instead of :math:`Γ_{X,i}` in
  code)

  ``e`` is an expression at some instantiation :math:`Ind\; e_{P,1} \;
  ...\; e_{P,m}\; e_{I,1}\; ...\; e_{I,n}` of the inductive family.
  :math:`Γ_I` and :math:`x : Ind\; e_{P,1}\; ...\; e_{P,m}\; Γ_I`
  are bound in the type :math:`T`. :math:`Γ_{X,i}` is bound in each
  :math:`F_i` which must have type :math:`T\; v_{i,1}\; ... \;v_{i,n}\;
  \left(c_i\; e_{P,1}\; ...\; e_{P,m}\; Γ_{X,i}\right)`.

  The entire ``match`` then has type :math:`T\; e_{I,1}\; ...\;
  e_{I,n}\; e`.

  The :math:`\_` in :math:`Ind\; \_` stands for the parameters. Since
  they are invariant for the whole match, we don't bind variables for
  them in the return type.

  We may omit each of the annotations ``as x``, ``in Ind _ Γ_I`` and
  ``return T``, leaving them to be inferred by the proof assistant and
  the reader.

  When ``e`` is the applied constructor :math:`c_i` applied to some
  arguments, the match reduces to :math:`F_{X,i}` with :math:`Γ_{X,i}`
  bound to the corresponding arguments.

  Then we provide a "fix" operator for recursion:

  .. coqdoc::

     fix F (a₁ : A₁) ... (aᵣ : Aᵣ) {struct aᵢ} : T := e

  We say that ``aᵢ`` is the "principal argument" or "decreasing
  argument" of the fixpoint. It must be of some inductive type.
  :math:`F : Π\; (a₁ : A₁) ... (aᵣ : Aᵣ), T` and each of the ``a₁`` to
  ``aᵣ`` are bound in ``e`` whose type must be ``T``.

  When the fixpoint is applied such that the value for the principal
  argument is a constructor, it reduces to ``e`` applied to the same
  arguments and with the fixpoint substituted for ``F``.

  There is a side-condition for the fixpoint to be well-formed:
  appearances of ``F`` in ``e`` must be applied such that the value
  for the principal argument is syntactically smaller than ``aᵢ``.

  "Syntactically smaller" means it comes from a recursive argument of
  a constructor from a match on ``aᵢ``.

It is trivial to implement the operation eliminator using match and
fixpoint. The other direction is more difficult and depends on the
specification of "syntactically smaller". This specification is beyond
the scope of this thesis, in which we only need that the translation
to elimation operators remains possible.

The proof assistant Lean uses elimination operators, and provides
syntactic sugar to provide user syntax matches which are translated to
eliminators. Coq uses primitive fixpoints and matches, and
automatically provides elimination operators as defined terms.

Agda uses a more complex form of pattern matching than we described.
By default it is more powerful than elimination operators (it proves
:ref:`uip-def`), but with the flag ``--without-K`` it is equivalent to
them.

.. note::

   When the type theory lacks universe cumulativity, we can define
   lifts using inductive types instead of having a special language
   construct for lifts:

   .. coqdoc::

      Inductive Lift@{u v|u < v} (A:Type@{u}) : Type@{v} :=
        lift : A -> Lift A.

   This works so long as the condition on the universe of the
   inductive uses the cumulative formulation "greater or equal". In
   other words, we get explicit cumulativity through inductive types.

.. _notable-inductives:

Notable inductive types
+++++++++++++++++++++++

Let us consider some examples (in Coq syntax):

-
   .. coqtop:: in reset

      Inductive Empty : Prop := .

   ``Empty`` is an inductive type with no parameters, no indices and no
   constructors. Its universe is |Prop|.

   Because there are no constructors it is impossible to build values
   of ``Empty``, justifying its name. The eliminator expresses that
   anything may be proved from ``Empty``:

   .. coqtop:: out

      Check Empty_rect.

   Coq generates a non-dependent eliminator for inductive types in
   |Prop|, but we could still define the dependent version if
   necessary.

-
   .. coqdoc::

      Inductive unit : Set := tt : Unit.

   ``unit`` has a unique constructor ``tt`` which has no arguments. We
   may omit the type annotation, since there are no output indices to
   specify:

   .. coqdoc::

      Inductive unit : Set := tt.

   The eliminator expresses that to prove a property of some element
   of ``unit`` it suffices to prove it for ``tt``:

   .. coqtop:: out

      Check unit_rect.

-
  .. coqdoc:: 

     Inductive bool := true | false.

  An inductive type with 2 elements. We have omitted the type
  annotation for the inductive itself this time, as Coq can
  automatically infer it (its universe is |Set|).

  The non-dependent eliminator is an ``if`` combinator:

  .. coqtop:: in

     Scheme If := Minimality for bool Sort Type.

  .. coqtop:: out

     Check If.

  .. note::

     The condition is the *last* argument.

  .. coqtop:: in

     Definition xor b1 b2 := If _ (If _ false true b2)
                                  b2
                                  b1.

  .. coqtop:: all

     Compute (xor true true).

-
  .. _nat-inductive:
  .. coqdoc::

     Inductive nat := 0 | S : nat -> nat.

  The type of Peano natural numbers (i.e. unary numbers). To avoid
  having to write the output type, we can also specify constructor
  arguments in the following way:

  .. coqdoc::

     Inductive nat := 0 | S (n : nat).

  The eliminator is induction over natural numbers:

  .. coqtop:: all

     Compute nat_rect.

  Since ``S`` has a recursive argument, the eliminator uses ``fix`` in
  its implementation.

-
  .. coqdoc::

     Inductive eq {A} a : A -> Prop := eq_refl : eq a a.

  (``{A}`` means ``A`` is an implicit argument: Coq will automatically
  infer and insert the correct value by solving typing constraints.
  For instance ``eq 0 0`` must have ``A ≡ nat``. If we want to provide
  it explicitly we can use ``@eq``: the ``@`` locally disables
  insertion of implicit arguments)

  ``eq`` represents equality between values, as such Coq will print
  ``eq a b`` as ``a = b``:

  .. coqtop:: all

     Check eq 0 1.

  The eliminator expresses that ``eq`` is a Leibnitz equality, i.e. if
  2 values are equal any predicate inhabited for one is inhabited for
  the other:

  .. coqtop:: out

     Check eq_rect.

  Equality is especially important to us, since proof irrelevance is
  about equality. We will inspect the equality inductive with more
  depth in section :ref:`sec-eq`.

-
  .. _sigma-types:
  .. coqtop:: in

     Inductive Sigma A B := sigma : forall a : A, B a -> Sigma A B.
     Arguments sigma {_ _} _ _.

  A type for dependent cartesian products. We can define projections
  using ``match``:

  .. coqtop:: in

     Definition pr1 {A B} (x:Sigma A B) : A :=
       match x with sigma a _ => a end.

     Definition pr2 {A B} (x:Sigma A B) : B (pr1 x) :=
       match x with sigma a b => b end.

  Coq has automatically inferred the appropriate matching predicate to
  get the second match to typecheck:

  .. coqtop:: out

     Print pr2.

-
  .. coqtop:: in

     Inductive Sum A B := inl (a:A) | inr (b:B).

  A discriminated sum type. It may be interesting to the reader to
  prove that ``Sum A B`` is equivalent to ``Sigma bool (If Type A B)``
  (where ``If`` is partially applied), and that ``bool`` is equivalent
  to ``Sum unit unit``.

-
  .. _w-type:
  .. coqdoc::

     Inductive W A (B : A -> Type) :=
       w : forall a, (B a -> W A B) -> W A B.

  ``W`` may be used as a combinator to express recursive inductive
  types, instead of providing them as primitives.

  For instance, ``nat`` is reformulated as ``W bool (If Empty unit)``
  which involves no recursive type other than ``W``. We then have ``0
  = w true (fun e => match e with end)`` and ``S n = w false (fun _ =>
  n)``.

  Note however that because one of the arguments of ``w`` is a
  function, the reformulation of ``nat`` is not fully equivalent to
  ``nat`` without function extensionality. For instance ``w true (fun
  e => 0)`` is neither equal to ``0`` or to a successor.

.. _ind-and-prop:

Inductives and |Prop|
+++++++++++++++++++++

Because we want to be able to erase propositional values at extraction
time, we must put some additional restrictions on inductives declared
in |Prop|.

For instance, if we had ``bool : Prop``, then the extraction of ``If``
would somehow have to work without using the condition argument.

We first restrict inductives in |Prop| to only those which have at
most one constructor, with all arguments themselves in |Prop|.

Then extraction of a match on a 0-constructor inductive produces an
unreachable case (``assert false`` in OCaml), and for a match on a
1-constructor inductive we just extract the unique branch.

This condition allows for instance ``Empty``, ``unit``, and ``eq``.

We can then allow all other inductives, but with their eliminations
restricted to motives in |Prop|. For instance:

.. coqdoc::

   Inductive ex A (B : A -> Prop) : Prop :=
     ex_intro (a : A) (b : B a).

(Coq will print ``ex A B`` as ``exists x : A, B x``)

.. coqtop:: out

   Check ex_ind.

It is impossible to define a corresponding ``Exists_rect`` with ``P :
Exists A B -> Type``.

When elimination of a |Prop|-inductive is restricted to |Prop|, we say
that it is "squashed" to |Prop|. A squashed inductive is similar to
its impredicative encoding, but it has dependent elimination (this is
all justified through models of Coq).

For instance the impredicative encoding of ``ex`` is

.. coqdoc::

   Definition ex A B : Prop := forall P:Prop, (forall a, B a -> P) -> P.

Sometimes we may phrase the restriction on inductives in |Prop| in the
inverse direction: all inductives are allowed in |Prop|, but only some
may be eliminated to non-|Prop| types. These eliminable inductives are
said to have "singleton elimination".

.. _emulated-ind:

Emulated Inductives
++++++++++++++++++++++++++++++

Given the data specifying an inductive (i.e. the parameters, indices,
universe, and constructor types), we may consider it emulated by a
type family if that family has type :math:`Π\; Γ_P\; Γ_I, \Type_u`,
there exist terms which have the types of the constructors, a term
which has the type of the eliminator and the expected reduction rules
are verified.

Then any judgment using the inductive may be translated to a judgment
using the type family instead (if the type theory has primitive
fix-and-match, we first need to translate to eliminators).

This may be used to show that certain extensions to the rules of
inductive types are conservative.

Extension: non recursively uniform parameters
+++++++++++++++++++++++++++++++++++++++++++++

Consider the following type:

.. coqdoc::

   Inductive Acc {A} (R : A -> A -> Prop) (x : A) : Prop :=
     Acc_in : (forall y, R y x -> Acc R y) -> Acc R x.

We call ``Acc`` the "accessibility predicate", it is inhabited for
``x`` when there is no infinite decreasing (according to ``R``) chain
starting from ``x``. It may be used to express strong induction.

``x`` is introduced like a parameter, but in the recursive argument of
``Acc_in``, ``Acc`` is applied to a different value ``y``. By the
rules as we have given them this is not allowed.

We could try interpreting ``x`` as an index, such that the above code
would be syntactic sugar for

.. coqdoc::

   Inductive Acc {A} (R : A -> A -> Prop) : A -> Prop :=
     Acc_in : forall x, (forall y, R y x -> Acc R y) -> Acc R x.

However we now have an argument ``x : A : Type`` to the constructor,
so ``Acc`` must be squashed.

The type theories implemented in Coq, Adga and Lean allow such non
recursively uniform parameters, with the same universe-related rules
and match typing rule as ordinary parameters. The difference can be
relevant in subtle ways with yet-to-be-introduced nested inductives.

.. note::

   Adga has no universe |Prop| (it has a universe called |Prop| but
   that is actually this thesis's |SProp| which doesn't permit
   ``Acc``). As such ``Acc`` must live in some predicative universe,
   then the argument ``y`` to the recursive argument of ``Acc_in``
   forces it to live at least in the universe of ``A``. As such Agda's
   use of non recursively uniform parameters has nothing to do with
   ``Acc``.

.. note::

   We could have included this as part of the definition of inductive
   families, but then the elimination operator needs to vary across
   some of the parameters so it's easier to present as an extension.

Extension: Mutual and Nested Inductives
+++++++++++++++++++++++++++++++++++++++

Mutual Inductives
=================

We may define multiple inductives in parallel. For instance:

.. coqtop:: in

   Inductive even : nat -> Prop :=
   | even_0 : even 0
   | even_S : forall n, odd n -> even (S n)

   with odd : nat -> Prop :=
   | odd_S : forall n, even n -> odd (S n).

Where in a single inductive the recursive arguments must produce a
value from that inductive, with mutual inductives the recursive
arguments of a constructor for one of the inductives being defined may
produce values from another inductive in the block. They must share
the same parameters.

To be fully equivalent to the fix-and-match presentation (which needs
no change beyond recognising which arguments are recursive),
elimination operators need to eliminate both inductives at the same
time. This mutual elimination is not automatically defined by Coq, but
it can be generated on demand:

.. coqtop:: in

   Scheme even_elim := Induction for even Sort Prop
     with odd_elim := Induction for odd Sort Prop.

.. coqtop:: out

   Arguments even_elim P Q : rename.
   Check even_elim.


A block of mutual inductives may be encoded as a single inductive by
modifying the indices. For our example:

.. coqdoc::

   Inductive even_odd : Sum nat nat -> Prop :=
   | even_0 : even_odd (inl 0)
   | even_S : forall n, even_odd (inr n) -> even_odd (inl n)
   | odd_S : forall n, even_odd (inl n) -> even_odd (inr n).

   Definition even n := even_odd (inl n).
   Definition odd n := even_odd (inr n).

In general, the idea is to first pack up all indices of each inductive
into a ``Sigma`` (for our example both inductives have only 1 index so
there is nothing to do), then combine the indices from each inductive
using ``Sum``.

The combined inductive's universe must be the maximum of the universes
of the original inductives. Assuming they are truly mutually
dependent, i.e. each has a constructor using another, the universes
must actually all be the same across the block, so this is not a
problem.

The above argument does not apply if one of the inductives is
squashed. This is more complicated and the reduction from mutual to
single inductives is not of particular interest to this thesis so
let's move on.

Nested Inductives
=================

We may further generalize to allow recursive arguments returning
previously defined inductives instantiated by the one being declared:

.. coqdoc::

   Inductive list (A:Type) := nil | cons : A -> list A -> list A.
   Inductive tree := node : list tree -> tree.

We need that the instantiated constructor types of the inner inductive
are positive in the outer inductive.

Such nested inductives may be translated to mutual inductives (and
from there to single inductives):

.. coqdoc::

   Inductive tree := node0 : list_tree -> tree
   with list_tree :=
   | nil_tree
   | cons_tree : tree -> list_tree -> list_tree.

   Fixpoint relist_tree (l:list tree) : list_tree :=
     match l with
     | nil => nil_tree
     | cons a tl => cons_tree a (relist_tree tl)
     end.

   Definition node (l:list tree) : tree := node0 (relist_tree l).

(the ``Fixpoint`` command is a convenient way to make a ``Definition``
whose body is a ``fix``)

Defining the translated eliminator and showing that it has the
expected reduction rules is left as an exercise to the reader.

.. _primitive-record:

Extension: Records with Eta Expansion
+++++++++++++++++++++++++++++++++++++

As we have seen with :ref:`Sigma types <sigma-types>`, if an inductive
has 1 constructor, no indices and is not squashed we can define
projections to extract each argument of the constructor.

Such types are called records. They are amenable to an alternative
presentation based around "fields" instead of constructors, where each
field is an argument of the unique constructor. For instance:

.. coqdoc::

   Record Sigma A B := sigma { pr1 : A; pr2 : B pr1 }.

We can show that every element of such a record is equal to the
constructor applied to the projections:

.. coqdoc::

   Definition Sigma_eta A B (x:Sigma A B) : x = sigma (pr1 x) (pr2 x)
   := match x with sigma a b => eq_refl end.

When the record is also *not* recursive, we may turn this equality
into a conversion rule. In Coq this must be activated with ``Set
Primitive Projections.``.

The name of the option stems from the fact that eta conversion uses a
primitive representation of projection terms instead of allowing
matches on such "primitive records". If we had such matches, we would
have

.. coqdoc::

     match x with sigma a b => c a b end
   = match (sigma (pr1 x) (pr2 x)) with sigma a b => c a b end
   = c (pr1 x) (pr2 x)

requiring special handling. Since the combination of primitive
projections and definitional eta is sufficient to translate matches
(to ``let a := pr1 x in let b := pr2 x in c a b``) we require that
they are translated as syntactic sugar.

Extension: Induction-Induction, Induction-Recursion
+++++++++++++++++++++++++++++++++++++++++++++++++++

Induction-induction is a generalization of mutual induction, where one
inductive of the block may use another as an index, and may use the
constructors of the other in its constructor types.

Induction-recursion allows to define a fixpoint on an inductive at the
same time that we define it, and so allows using that fixpoint in the
constructor's type (applying it to the recursive arguments).

These are studied in depth in Forsberg's thesis :cite:`Forsberg2014`.

A typical use is to define type theories inside type theory, such that
no untyped term may ever appear. For this, we need to simultaneously
define well-formed contexts, types in a context, and terms of a
certain type in a context:

.. coqdoc::

   Inductive context :=
   | empty : context
   | cons : forall Γ, type Γ -> context

   with type : context -> Type :=
   | Pi : forall Γ (A:type Γ), type (cons Γ A) -> type Γ
   | ...

   with term : forall Γ, type Γ -> Type :=
   | lambda : forall Γ (A:type Γ) (B:type (cons Γ A)),
              term (cons Γ A) B -> term Γ (Pi Γ A B)
   | ...

The above code sample shows the use of induction-induction.
Induction-recursion is also necessary: to define the conversion rule
we need to define the conversion judgment. Conversion, through the
beta reduction rule, involves substitution. Substitution is defined
recursively on ``term``.

Both induction-induction and induction-recursion are stronger than
induction alone.

Metatheory of Inductive Families
++++++++++++++++++++++++++++++++

The usual properties are preserved when adding inductive types.

The inversions for inductive types bring no new concepts. For
instance, inductive types and constructors are injective for
conversion.

Normalisation for elements of an inductive family is interesting: they
are normal when they reduce to neutral terms, or when they reduce
to a fully applied constructor whose arguments are normal.

With :ref:`natural numbers <nat-inductive>`, this means elements of
``nat`` reduce to a stack of applications of the successor ``S``, then
either a neutral term or ``0``. In the empty context, there are no
neutral terms so natural numbers must reduce to :math:`S^n\; 0` for
:math:`n` a concrete natural number from the metatheory.

For booleans, in the empty context they reduce to either ``true`` or
``false``.

For elements of an identity type :math:`x = y`, in the empty context
they reduce to some ``eq_refl`` (implicitly applied to some type and
``z``), then by inversion :math:`x ≡ z ≡ y`.

Partially applied inductives, constructors and fixpoints may be
considered as their eta expansion when defining weak head normal
forms. In some formulations of type theory instead of having a
primitive unapplied ``Ind``, the primitive term former is the fully
applied inductive (and so on for constructors etc) so the unapplied
form never occurs.

Fully applied inductives and constructors are introduction forms.
Matches, fully applied fixpoints and fully applied elimination
operators are elimination forms with the head being respectively the
discriminee for matches, and the principal argument (whose type is the
inductive) for fixpoints and elimination operators.

.. _sec-eq:

Equality
--------

With inductive families, we have 3 different concepts of equality
between two terms :math:`t` and :math:`u`:

- "syntactic" or "structural" equality, when the terms are equal up to
  equality in the metatheory.

- conversion, also called "definitional" or "extensional" equality,
  when the terms are indistinguishable inside the type theory.

- "propositional" or "intensional" equality, when there is a term of
  type :math:`t = u`

Each is included in the next.

The following axioms can make it easier to work with dependent types
(including equality itself):

Function Extensionality
+++++++++++++++++++++++

Function extensionality means that if :math:`Π (x:A), f ∘ x = g ∘ x`
then :math:`f = g`. Even though this looks closely related to the
definitional eta rule, it requires an axiom to prove.

Otherwise, we could have a proof that ``λ (x:bool), x = If bool true
false``, but by canonicity and inversion in the empty context such a
proof would imply that these 2 implementations of identity for
booleans are convertible, which they are not.

Function extensionality makes functions less opaque, such that ``W
bool (If Empty unit)`` (from :ref:`W types <w-type>`) is fully
equivalent to ``nat``.

.. _uip-def:

Uniqueness of Identity Proofs
+++++++++++++++++++++++++++++

i.e. :math:`Π (A:\Type) (x\; y : A) (p\; q : x = y), p = q`.

This is equivalent to Streicher's axiom K: :math:`Π (A:\Type) (x:A)
(p:x = x), p = \mathtt{eq\_refl}`.

Without UIP, some intuitive results such as injectivity of the
``sigma`` constructor are not available: we only have ``sigma a b =
sigma a b' → b = b'`` when the type of ``a`` validates UIP.

Note that all types with decidable equality (such as ``bool`` and
``nat``) validate UIP by Hedberg's lemma :cite:`Hedberg1998`.

Univalence
++++++++++

Univalence means that equivalent types are equal. Here, equivalence is
a refinement of the concept of bijections (and is logically equivalent
to bijections, i.e. there is an implication in both directions but
composing the implications does not produce the identity function). It
is studied in-depth in :cite:`hottbook`.

Univalence implies function extensionality, and is inconsistent with
uniqueness of identity proofs: under univalence, the types of
equalities between types are equivalent to the types of equivalences
between types. For instance, :math:`bool = bool` is equivalent to
:math:`bool ≅ bool` which has 2 elements (one corresponding to the
identity function and one to negation).

This means that when we introduce definitionally proof irrelevant
types, if we want to be compatible with univalence we must be careful
to avoid allowing proof irrelevant equality types.

.. _ett:

Extensional Type Theory
+++++++++++++++++++++++

In mainstream mathematical practice, propositional and definitional
equality are not distinguished: when two values are equal they may be
substituted for one another without some intervening ``match`` on the
equality. However in type theory we cannot assume or prove
definitional equalities, we can only do so with propositional ones.

Extensional type theory (ETT) bridges the gap by adding the "equality
reflection" rule:

.. inference:: Equality-Reflection

   Γ ⊢ p : t =_A u
   --------------
   Γ ⊢ t ≡ u : A

In other words propositional equality "reflects" the conversion inside
the language of the type theory.

The usual type theory without equality reflection is called
"intensional" type theory (ITT).

Equality reflection makes type theory undecidable: for instance in
order to be able to conclude that variable :math:`x` is not
convertible to variable :math:`y`, we need to check that context is
consistent. Otherwise, all types including :math:`x = y` would be
inhabited.

Equality reflection proves both function extensionality and uniqueness
of identity proofs. Function extensionality is trivial by going
through eta equality.

We prove UIP by going through axiom K: let :math:`p : a =_A a`. Then

.. coqdoc::

   match p as px in eq _ _ x return px = eq_refl with
   eq_refl => eq_refl
   end : p = eq_refl

This fails in intensional type theory because :math:`x : A, px : a = x
⊬ px = eq\_refl`: the type of :math:`px` is not the type of any
:math:`eq\_refl`. With equality reflection however since we have the
assumption :math:`px : a = x` everything works out.

ETT is conservative over ITT with UIP and function extensionality
axioms by a model argument :cite:`Hofmann_1997`.

ETT can be translated to ITT with UIP, function extensionality and
congruence of application for the heterogeneous equality
:cite:`Oury_2005`. (The last axiom is because in some context we may
have :math:`nat → bool = nat → nat`, then :math:`(λ (x:nat), x) ∘ 0 ≡
0 : bool`. In other words it's an issue with beta reduction and
:math:`Π`-injectivity being unprovable inside the theory.)

With some changes it can be translated to ITT with UIP and function
extensionality :cite:`winterhalter:hal-01849166`. Specifically, we
need to annotate lambda abstractions with their codomain (written
:math:`λ (x:A). B, e`) and applications with the domain and codomain
of the head (written :math:`f ∘_{(x:A). B} v`). Then we block beta
reduction unless the indices match, i.e. the :ref:`Beta-conv
<beta-conv>` rule becomes

.. inference:: Beta-conv-ett

   Γ, x : A ⊢ e : B
   Γ ⊢ v : A
   ----------------------------------------
   Γ ⊢ \left(λ (x:A). B, e\right) ∘_{(x:A). B} v ≡ e[x:=v] : B[x:=v]

Other Types
-----------

There are other types which we may want to add to dependent type
theory:

- quotient inductive types (QITs) (:cite:`Altenkirch_2016`,
  :cite:`Altenkirch_2018` for the combination with
  induction-induction), as the name indicates they formalize the
  notion of quotienting by some relation.

- higher inductive types (HITs) (:cite:`hottbook`), from homotopy type
  theory. These are incompatible with UIP: it is possible to define a
  HIT ``Circle`` with a value ``point : Circle`` such that ``point =
  point`` is equivalent to integers.

- Coinductive types (:cite:`Gimenez_1995`), which allow to formalize
  things like infinite streams or non-termination.

- more exotic types which do not look like deformations of the
  inductive type concept, such as interval spaces from cubical type
  theory, etc.

We don't study these with any depth, but their addition and the
addition of proof irrelevance seem relatively orthogonal, at least for
decidability.

.. todo chapter conclusion??
