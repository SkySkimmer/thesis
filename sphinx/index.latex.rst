=====================================
 Not used text (but needs to be here)
=====================================

.. keep in sync with html.rst

--------------
Résumé Étendu
--------------

.. include:: introduction_french.rst

------------------
Extended Overview
------------------

.. include:: introduction.rst

---------------------
Dependent Type Theory
---------------------

.. toctree::

   pastwork

-----------------------------------------------------------
Dependent Type Theory with Definitional Proof Irrelevance
-----------------------------------------------------------

.. toctree::

   stt

--------------------------------------------
Interpreting Inductive Families As Fixpoints
--------------------------------------------

.. toctree::

   translation

------------------------------------------
Definitional Uniqueness of Identity Proofs
------------------------------------------

.. toctree::

   uip

--------------------------------------------------
Implementing Definitional Proof Irrelevance in Coq
--------------------------------------------------

.. toctree::

   implem

-------------------
Other Contributions
-------------------

.. toctree::

   other

-------------------
Future Perspectives
-------------------

.. toctree::

   future

.. toctree::
   zebibliography
