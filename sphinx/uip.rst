.. _uip:

Reduction Behaviour of Irrelevant Identities
--------------------------------------------

Extending a type theory with |SProp| to have definitional uniqueness
of identity proofs at first glance seems to be just about allowing the
inductive family representing identity in |SProp|:

.. NB: little trick with coqdoc/coqtop none to get nice names in
   eq_rect

.. also note Universe Checking for "faking" uip
.. ie compat with non uip having versions of Coq
.. (which don't have the special reduction)

.. coqdoc::

   Inductive eq {A} (a:A) : A -> SProp := eq_refl : eq a a.

.. coqtop:: none

   Unset Universe Checking.
   Inductive eq {A} (a:A) : forall b : A, SProp := eq_refl : eq a a.
   Set Universe Checking.

However unlike the primitives we have seen previously (``Empty``,
``Squash``, etc) it also requires special reduction behaviour to
preserve good properties of reduction.

Usually, when we match some discriminee which does not reduce to a
constructor, the match's reduction is also blocked:

.. coqtop:: all

   Eval lazy in fun x : nat => match x with 0 => true | S _ => false end.

Now suppose we match on an hypothesis of type ``eq 0 0``:

.. coqdoc::

   Definition m1 := fun x : eq 0 0 =>
     match x with eq_refl _ => nat -> nat end.

By proof irrelevance and since they have the same type, the
discriminee ``x : eq 0 0`` may be replaced by ``eq_refl 0``, so ``m1``
should be convertible to

.. coqtop:: in

   Definition m2 := fun _ : eq 0 0 =>
     match eq_refl 0 with eq_refl _ => nat -> nat end.

then by reduction to

.. coqtop:: all

   Eval lazy in m2.

Because we check conversion by reducing both sides then inspecting
their structure, and as the match in ``m1`` does not have the same
structure as ``nat -> nat``, we have to make sure that it does reduce
to ``nat -> nat``. Additionally, suppose we wished to apply some ``f :
m1 x`` to ``0`` (in a context where ``x : eq 0 0``), to check this we
need to find a way to convert ``m1 x`` to a product type: that way is
reduction.

Of course, we can't reduce every match on an ``eq`` to its single
branch: that would be ill-typed in general and so would break subject
reduction. We need to reduce

.. coqdoc::

   match e : eq a b as x in (eq _ y) return (P y x) with
     | eq_refl _ => v : P a (eq_refl a)
   end : P b e

to ``v`` if and only if ``a`` and ``b`` are convertible:

- if they are convertible ``e`` is convertible to ``eq_refl a``.
- if they are not convertible, ``e`` and ``eq_refl a`` do not have
  convertible types so there is no reduction to do.

This special reduction behaviour was already noted in
:cite:`Werner2008`.

.. note::

  Perhaps we could also reduce when ``P a (eq_refl a)`` and ``P b e``
  are convertible (for instance when ``P`` doesn't use its arguments),
  but we don't need to in order to keep the type theory tractable.

Since this makes reduction depend on conversion, there are severe
consequences on concepts used in metatheoretical proofs. For instance
the property of being in weak head normal form now involves the
negation of conversion. Since the proof in section :ref:`sprop-dec`
deeply uses weak head normal forms, it seems quite difficult and
perhaps impossible to add ``eq`` to it.

Another lost property: applying a substitution from variables to
neutral terms may unlock reductions, for instance the substitution
:math:`y \mapsto x` applied to a match on ``eq x y``. This may have an
impact when attempting to adapt the reduction machine used in Coq
:cite:`LazyMachine` as it uses neutrality to control the sharing
optimisations.

Termination of the Special Reduction
------------------------------------

Normalization of a theory with definitional UIP breaks down in the
presence of another impredicative universe: using

.. coqdoc::

  Axiom all_eq : forall P Q : Prop, eq P Q.

  Definition K (A B : Prop) (x:A): B := match all_eq A B with eq_refl => x end.

  Definition bot : Prop := forall P : Prop, P.

  Definition c : bot := fun X : Prop => K (bot -> bot) X (fun z : bot => z (bot -> bot) z).

we would have ``c (bot -> bot) c`` infinitely reduce to itself.

Using |SProp| instead of |Prop| poses the same issues.

This issue was identified by Abel and Coquand
:cite:`abel_coquand_failur_normal_impred_type_theor`.

There is no such known issue in predicative theories.

Alternatively, perhaps restricting irrelevant equalities to small
types (such that ``all_eq`` cannot be stated any more, since |Prop| is
a large type) may be sufficient to recover termination. This may be
related to the speculation in the following section, where we focus on
compatibility with univalence.

Hierarchy of Strictly Truncated Universes
-----------------------------------------

Allowing specific types with implicit identities may not imply
definitional UIP for all types. For instance, we could have
definitional UIP for natural numbers

.. coqdoc::

   Inductive nateq (n:nat) : nat -> SProp := nateq_refl : nateq n n.

without having UIP on arbitrary types.

More generally, we could imitate the homotopy levels defined in HoTT
to have a concept of "strictly truncated universe". Where in HoTT we
define

.. coqdoc::

   Record Contr (A:Type) := { center : A; contr : forall y, center = y }.

   Inductive hlevel := -2 | +1 : hlevel -> hlevel.

   Fixpoint IsTrunc n A := match n with
     | -2 => Contr A
     | +1 k => forall x y : A, IsTrunc k (x = y)
     end.

   Record hType n := { htype : Type; htrunc : IsTrunc n htype }.

We could have universe :math:`\Type^h_i` with a predicative level
:math:`i` and a strict homotopy truncation level :math:`h`. The strict
homotopy level ranges from :math:`-1` to :math:`∞` (there is no use
for a type of contractible types so we start at :math:`-1`, and
:math:`∞` is for types which are not truncated at any level). The
rules are

.. inference:: hUniv-in-hUniv

   i < j
   h < k ∨ k = ∞
   ----------------
   Γ ⊢ \Type^h_i : \Type^k_j

.. inference:: Pi-in-hUniv

   \Gamma \vdash A : \Type^k_i
   \Gamma, x : A \vdash B : \Type^h_j
   ------------------------------------
   \Gamma \vdash \Pi (x:A). B : \Type^h_{max(i,j)}

In other words the homotopy level is impredicative.

Then :math:`\SProp_i` is just :math:`\Type^{-1}_i`, i.e.

.. inference:: hUniv-irrelelance

   Γ ⊢ A : \Type^{-1}_i
   Γ ⊢ t : A
   Γ ⊢ u : A
   --------------
   Γ ⊢ t ≡ u : A

(if we want impredicativity of |SProp| we can add that all
:math:`\SProp_i` are convertible to each other or some such collapsing
rule)

Finally we use the strict homotopy level when defining equality types:
if :math:`A : \Type^h_i` and :math:`x, y : A` then :math:`x =_A y :
\Type^{h-1}_i` (at the edges :math:`-1 - 1 = -1` and :math:`∞ - 1 =
∞`).

This is difficult to implement: when using universe variables with a
graph of constraints, :math:`h - 1` is not cleanly encoded as new
graph constraints. We could try to generalize the graph system, but
any such generalization would risk performance. Universes already have
a significant performance cost with little visible benefit
(mathematicians rarely need to think about universes when working) so
type theory with strict homotopy levels may be better suited to
experimental proof assistants than to heavily used Coq. The practical
use of a hierarchy of strictly truncated types is also unclear.
