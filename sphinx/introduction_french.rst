Le texte de cette introduction est en grande partie dérivé de Gilbert
et al. :cite:`Gilbert:POPL2019`.

.. warning:: To English readers:

   This chapter is a translation of :ref:`Chapter 2 <intro-eng>` from
   English to French.

Le Besoin d'insignifiance des preuves
---------------------------------------------------


L'insignifiance des preuves, le principe selon lequel deux preuves quelconques d'une même proposition
sont égales, est au cœur du mécanisme d’extraction de Coq :cite:`letouzey04`.
Dans le calcul des constructions inductives (``CIC``), la théorie
sous-jacente de l'assistant de preuve Coq,
il existe un type (imprédicatif) |Prop| utilisé pour caractériser les types pouvant être vus comme
des propositions---par opposition aux types dont les habitants ont un
sens calculatoire, qui vivent dans l'univers prédicatif |Type|.
Cette information statique est utilisée pour extraire une
formalisation écrite en Coq
vers un programme purement fonctionnel, en effaçant en toute sécurité toutes les parties impliquant
des termes qui habitent des propositions, car un programme ne peut pas
utiliser ceux-ci de manière calculatoire. 

Afin de garantir concrètement qu'aucun calcul ne peut fuir de preuves
de propositions, Coq utilise une restriction de l’élimination dépendante des
types inductifs dans |Prop| dans les prédicats dans |Type|.
Cette restriction, appelée la *condition d'élimination du singleton*,
vérifie qu'une proposition inductive peut être éliminée dans un type seulement
quand:

* la proposition inductive a au plus un constructeur,
* tous les arguments du constructeur sont eux-mêmes
  non-calculatoire, c'est-à-dire, sont dans |Prop|.

Cependant, dans la version actuelle de Coq, l’élimination du singleton
permet d'être compatible avec l'insignifiance des preuves, mais n'est
pas assez pour l'avoir. Cela signifie que bien que deux preuves de la même
proposition ne peuvent pas être distinguées de manière pertinente dans
le système, on ne peut pas utiliser le fait qu'elles soient égales
dans la logique (sans axiome).




Considérons par exemple un mathématicien ou un informaticien 
qui définit les entiers bornés de la manière suivante:

.. coqdoc::
  Definition boundednat (k : nat) : Type := { n : nat & n <= k }.

Ici ``boundednat k`` est la somme dépendante d'un entier ``n``
avec une preuve (dans |Prop|) que ``n`` est plus petit que ``k``,
en utilisant la définition inductive

.. coqdoc::
  Inductive le : nat -> nat -> Prop :=
  | le0 : forall n, 0 <= n
  | leS : forall m n, m <= n -> S m <= S n.

Ensuite, notre utilisateur définit une coercition implicite de
``boundednat`` à ``nat`` afin qu'il puisse travailler avec des entiers bornés presque comme s'ils
étaient des entiers, mis à part des preuves supplémentaires de la borne.

.. coqdoc::
  Coercion boundednat_to_nat : boundednat >-> nat.

Par exemple, il peut définir l’addition d’entiers bornés simplement en
en s’appuyant sur l’addition d’entiers, modulo une preuve que le résultat est
toujours borné:

.. coqdoc::
   Definition add {k} (n m : boundednat k) (e : n + m <= k)
     : boundednat k
     := ( n + m ; e).

Malheureusement, quand il s’agit de raisonner sur des entiers bornés, le
la situation devient plus difficile. Par exemple, le fait que l’addition de
nombres naturels liés soit associative

.. coqdoc::
   Definition bounded_add_associativity k (n m p: boundednat k)
         e_1 e_2 e'_1 e'_2 :
    add (add n m e_1) p e_2 = add n (add m p e'_1) e'_2.

ne découle pas directement de l'associativité de l'addition sur les entiers,
car elle nécessite en outre une preuve que ``e_2`` est égal à
``e'_2`` (modulo la preuve d'associativité de l'addition de
entiers).

Cela devrait être vrai car ce sont deux preuves d'une même
proposition, mais ça ne vient pas automatiquement. Au lieu de cela, l'utilisateur
doit prouver que la preuve n'est pas pertinente pour ``<=`` *manuellement*.
Cela peut être prouvé (en utilisant le filtrage à la Agda définit par
le plugin Equations [#ff1]_) par induction sur la preuve de ``m <= n``:

.. coqdoc::
 Equations le_hprop {m n}  (e e' : m <= n) : e = e' :=
    le_hprop (le0 _) (le0 _) := eq_refl;
    le_hprop (leS _ _ e) (leS n m e') := ap (leS n m) (le_hprop e e').

Notons ici l'utilisation de la fonctorialité de l'égalité
``ap: forall f, x = y -> f x = f y``
ce qui nécessite un raisonnement explicite sur l'égalité.
Même si prouver l'associativité de l'addition était plus compliqué
qu'attendu, notre utilisateur est encore assez chanceux de raisonner
sur un type inductif qui est en fait une proposition homotopique, au sens
de la théorie homotopique des types (HoTT) :cite:`hottbook`.
En effet, ``<=`` satisfait la version propositionnelle (par opposition à
définitionnelle, qui est vrai par calcul) de l'insignifiance des
preuves, comme le montre le lemme ``le_hprop``.
En revanche, pour un type inductif arbitraire dans |Prop|, il n'y a aucune raison
pour que ce soit une  proposition homotopique, et donc
l'insignifiance des preuves, même sous sa forme propositionnelle, ne peut être prouvée
et doit être déclaré *comme un axiome*.

Dans un contexte où l'insignifiance des preuves pour |Prop| est native, il devient
possible de définir une opération sur les types qui transforme tout type en un
type dont le preuve sont insignifiante, et rend donc explicite dans le
système le fait qu’une valeur dans ``Squash T`` ne sera jamais
utilisée pour calculer. 

.. coqdoc::
  Definition Squash (T : Type) : Prop := forall P : Prop, (T -> P) -> P.

Cette opération vérifie le schéma d'élimination suivant (restreint aux
propositions)

.. coqdoc::
  forall (T : Type) (P : Prop), (T -> P) -> Squash T -> P.

ce qui veut dire qu'il correspond à la troncation propositionnelle 
:cite:`hottbook` de HoTT, initialement introduite comme le *bracket type*
par Awodey et Bauer :cite:`Awodey:2004:PT:1094469.1094484`.

L'importance de l'insignifiance (définitionnelle) des preuves pour simplifier
le raisonnement a été remarqué depuis longtemps, et divers travaux ont
essayé de promouvoir sa mise en œuvre dans des assistants de preuve
basés sur la théorie des types :cite:`Pfenning:2001:IEP:871816.871845,Werner2008`.
Ceci n’a cependant jamais été réalisé, principalement à cause du
malentendu fondamental que l'*élimination des singletons était la
bonne contrainte justifiant les propositions qui peuvent être éliminées dans un type*.

En effet, on peut penser à l’élimination des singletons comme une approximation *syntaxique*
pour savoir quels types sont naturellement de propositions homotopiques, et donc
peuvent être éliminés dans un type arbitraire sans faire fuir des morceaux de calcul
ou sans impliquer de nouveaux axiomes.
Cependant, l’élimination des singletons ne fonctionne pas pour les types de données indexés, car
il s'applique par exemple au type d'égalité de Coq

.. coqdoc::
  Inductive eq (A : Type) (x : A) : A -> Prop :=  eq_refl : eq A x x

Si l'insignifiance des preuves est valable pour l'égalité, alors
chaque égalité a au plus une preuve, un principe qui est connu sous le
nom d'unicité des preuves d'identité (UIP).
Par conséquent, en supposant l'insignifiance des preuves avec
l'élimination des singletons, on obtient un nouvel axiome dans la théorie, qui est par exemple incompatible
avec l’axiome d’univalence de HoTT.
Cela peut ne pas sembler trop problématique pour certains, mais une autre conséquence de
l’élimination des singletons en présence d’insignifiance
définitionnelle des preuves est
qu'il brise la décidabilité de la conversion. Par exemple, le
prédicat d'accessibilité:

.. coqdoc::
 Inductive Acc (A : Type) (R : A -> A -> Prop) (x : A) : Prop :=
    Acc_intro : (forall y : A, R y x -> Acc R y) -> Acc R x

satisfait au critère d'élimination des singletons, mais implémenter
l'insignifiance définitionnelle des preuves pour ce prédicat conduit à
une conversion indécidable et donc un vérificateur de type indécidable.


Une approche alternative consiste à faire comme dans Lean, où il y a
l'insignifiance définitionnelle des preuves avec l'élimination des singletons, mais ils ne mettent en œuvre qu'une
version partielle de l'insignifiance des preuves pour les types inductifs récursifs
satisfaisant l’élimination des singletons, qui est
restreint aux termes fermés. [#ff2]_

Mais cette implémentation partielle de l’algorithme de conversion ne
satisfait pas la réduction du sujet, ce qui est une propriété plus que
souhaitable pour un assistant de preuve.

Enfin, l’élimination des singletons ne parvient pas à capturer les types inductifs avec
plusieurs constructeurs tels que ``<=`` qui sont des propositions
homotopiques parfaitement valables et pourraient être éliminés dans
les types.



En y regardant de plus près, Coq et son univers imprédicatif |Prop| ne sont peut-être pas le seul
moyen d'implémenter l'insignifiance des preuves dans un assistant de
preuve.
Agda, qui a seulement une hiérarchie prédicative d'univers et pas de |Prop|, utilise plutôt
une notion d'arguments non pertinents :cite:`Abel2012OnIA`.
L’idée est de marquer dans le type des fonctions quels arguments peuvent être
considérés comme non pertinents. Par exemple, notre utilisateur peut encoder les
entiers bornés dans ce cadre, en spécifiant que le deuxième argument
de la paire dépendante est sans importance (comme indiqué par :agda:`.` dans la
définition de :agda:`. (n <= k)`):

.. code-block:: agda

  data boundedNat (k : ℕ) : Set where
    pair : (n : ℕ) → .(n ≤ k) → boundedNat k

Le fait que l’égalité des entiers naturels sous-jacentes implique
l’égalité des entiers naturels bornés est gratuit, grâce à
l'insignifiance du deuxième composant:

.. code-block:: agda

  piBoundedNat : {k : ℕ} (n m : ℕ) (e : n ≤ k) (e′ : m ≤ k)
               → n ≡ m → pair n e ≡ pair m e′
  piBoundedNat n m _ _ refl = refl

Cependant, dans cette approche, l'insignifiance des preuves n’est pas
une propriété du type considéré, mais plutôt une façon d'utiliser
l'argument donné une fonction. Nous espérons que |SProp| donnera plus
de flexibilité aux développeurs de preuves formelles.


Leçons de la théorie homotopique des types
---------------------------------------------------


Avant de plonger dans la définition précise d’une théorie des types
avec insignifiance des preuves, explorons ce qui la rend
difficile à introduire tout en maintenant la vérification du typage décidable et
en évitant d'induire des axiomes supplémentaires tels que UIP ou
l'extensionalité des fonctions.

|SProp| comme approximation syntaxique des propositions homotopiques
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

La première leçon de HoTT est que chaque type dans |SProp| doit être une
proposition homotopique, c’est-à-dire avoir le niveau homotopie $-1$.
Formellement, l’univers des propositions homotopiques ``hProp`` est défini comme

.. coqdoc::
  Definition hProp := { A : Type & forall x y : A , x = y }.

et donc il correspond exactement à l'univers des types satisfaisant
l'insignifiance des preuves *de manière propositionnelle*.
Comme nous l’avons mentionné plus haut, l’opérateur ``Squash``
qui transforme n'importe quel type en un habitant de |SProp|, correspond à
la troncation propositionnelle.

L'existence de |SProp| ne devient intéressante que si l'on peut
éliminer certaines définitions inductives dans |SProp| à des types arbitraires.
Sinon, |SProp| constitue une couche logique isolée correspondant
à la logique propositionnelle, sans aucune interaction avec le reste de la
la théorie des types.

La question est donc:

*Quels types inductifs de* |SProp| *peuvent être éliminés sur des types arbitraires?*

Bien entendu, pour préserver la cohérence, il convient de limiter cette option à
des types inductifs dont on peut montrer qu'ils sont des propositions homotopiques, mais cela
n'est pas suffisant pour préserver la décidabilité de la vérification du typage et
l'indépendance enverts UIP.

Flirter avec l'extensionalité
++++++++++++++++++++++++++++++++++++++++

Regardons d’abord une classe apparemment simple de propositions homotopiques,
les types contractibles. Ils constituent le niveau le plus bas de la hiérarchie
des types, pour lesquels non seulement il y a au plus un habitant à
égalité près, mais il y en a exactement un.
Bien sûr, le type unité dans |SProp| qui correspond à la proposition "vraie"

.. coqdoc::
  Inductive sUnit : SProp := tt : sUnit.

est contractible.

Nous dirons que le type d'unité peut être éliminé à n'importe quel type, et donc
nous obtenons un type d'unité avec une règle :math:`\eta`
définitionnelle telle que ``u`` est
convertible à ``tt`` pour tout ``u: sUnit``.
Mais en général, il ne faut pas s’attendre à ce que tout type contractible
dans |SProp| puisse être éliminé à un type arbitraire, comme nous allons le voir ci-dessous.

Types singletons
*******************************************

Un exemple typique d’un type contractible non trivial est le
type singleton. Pour tout type ``A`` et ``a: A``,
il est défini comme le sous-ensemble de points dans ``A`` égaux à ``a``:

.. coqdoc::
  Definition Sing (A : Type) (a : A) := { b : A & a = b }.

Si nous incluons les types singleton dans |SProp|, et permettons donc son élimination
à des types arbitraires, nous sommes conduits à une théorie de type extensionnelle, dans laquelle
l'égalité propositionnelle implique l'égalité définitionnelle. Cela
revient donc à ajouter UIP et l'indécidabilité de la vérification du typage, à la théorie. [#ff6]_
En effet, supposons que les types singletons puissent être éliminés de manière arbitraire
dans les types, alors il existe une projection ``pi1: Sing A a -> A`` qui
récupère le point du singleton. Mais alors, pour toute preuve
d'égalité
``e: a = b`` entre ``a`` et ``b`` dans ``A``,
en utilisant la congruence de l'égalité définitionnelle, il y a la chaîne suivante
d'implications

.. code-block:: text

      (a ; refl) ≡ (b ; e) : Sing A a
  =>  pi1 (a ; refl) ≡ pi1 (b ; e) : A
  =>  a ≡ b : A
  
et par conséquent, ``e: a = b`` implique ``a ≡ b``. À partir de cette
analyse, il est clair que |SProp| ne peut pas inclure tous les types contractibles.

Flirter avec l'indécidabilité
++++++++++++++++++++++++++++++++++++++++

Voyons maintenant d’autres types inductifs qui ne sont que des propositions
sans être contractible.
Le premier exemple canonique est le type vide

.. coqdoc::
  Inductive sEmpty : SProp := .

qui n'a pas d'habitant, avec un principe d'élimination qui
déclare que tout peut être déduit du type vide

.. coqdoc::
 sEmpty_rect : forall T : Type, sEmpty -> T.

Nous verrons que ce type peut être éliminé à n’importe quel type, et c’est
réellement le moyen principal de faire communiquer |SProp| avec
|Type|. En particulier, il permet la construction de programmes par
filtrage en utilisant une contraction dans |SProp| pour les branches absurdes.

L'autre type de définition inductive dans |SProp| qui peut être
éliminée dans n'importe quel type est la somme dépendante d'un type ``A`` dans
|SProp| et une famille dépendante sur ``A`` dans |SProp|, le cas zéro étant le type unité ``sUnit``.

Voyons maintenant des définitions inductives plus complexes.

Prédicat d'accessibilité
***************************************

Comme mentionné dans l’introduction, le prédicat d’accessibilité est une
proposition homotopique mais elle ne peut être éliminée dans tous les types
tout en conservant la décidabilité de la conversion, et donc de la vérification du typage.
Intuitivement, c’est parce que le prédicat d’accessibilité permet de définir
des points fixes avec une condition de garde *sémantique* (le fait que
chaque appel récursif soit sur les termes ``y`` tels que ``R y x``)
plutôt qu'une garde *syntaxique* (le fait que chaque appel récursif soit sur un
sous-terme syntaxique).
Ceci est problématique dans un contexte avec insignifiance des preuves
car une fonction qui est définie par induction sur un prédicat
d'accessibilité peut être dépliée infiniment souvent.
Pour comprendre pourquoi, considérons le lemme d’inversion

.. coqdoc::

  Definition Acc_inv A R (x : A) (X : Acc x)
    : forall y:A, R y x -> Acc y
    := match X with Acc_intro f => f end.

Ce lemme d’inversion est toujours définissable, car même si
l'élimination pour ``Acc`` est restreinte à son univers |Prop| le type
de retour est ce même |Prop| (par imprédicativité).

Par contre, supposons que l'élimination sur ``Acc`` ne sois pas
restreinte de manière à avoir l'éliminateur général

.. coqtop:: out

   Check Acc_rect.

Alors, à partir de cette inversion et en utilisant
l'insignifiance des preuves, l’égalité définitionnelle suivante peut être déduite pour tout prédicat
``P: A -> Type`` et fonction
``F : forall x, (forall y, r y x -> P y) -> P x`` et ``X : Acc x``

.. code-block:: text

    Acc_rect P F x X
  ≡ F x (fun y r => Acc_rect P F y (Acc_inv A R x X y r))

Dans un contexte ouvert, il est indécidable de savoir combien de fois
ce déploiement doit être fait. Même la stratégie selon laquelle il y a
au plus un déploiement peut ne pas prendre fin. En effet, supposons
que nous nous trouvions dans un contexte où il existe une preuve
``R_refl`` montrant que ``R`` est réflexif (ce qui est inconsistent
avec la preuve ``X``). Ensuite, appliquer le dépliage ci-dessus une
fois à ``F: = fun x f => f x (R_refl x)`` se réduit en

.. code-block:: text

  Acc_rect P F x X ≡ Acc_rect P F x (Acc_intro x (Acc_inv A R x X))

et le dépliage peut recommencer, à jamais.

Comme mentionné ci-dessus, si nous analysons la source de ce dépliage
infini, il est dû à l'appel récursif à ``Acc`` dans l'argument de
``Acc_intro`` sur une variable arbitraire `` y`` qui n'est pas
*syntaxiquement* plus petite que la variable ``x`` initiale, mais
*sémantiquement* gardée par la condition ``R y x``.
Cet exemple montre que l’élimination du singleton n’est pas un critère
suffisant pour savoir quand un type inductif dans |SProp| peut être éliminé dans n'importe quel type,
car il faut introduire quelque chose de similaire à la condition de garde syntaxique
sur les points fixes.

Voyons maintenant pourquoi ce n'est *pas une condition nécessaire* non plus.

Le bon et le mauvais inférieur ou égal
**************************************************************

La définition de inférieur ou égal donnée dans l'introduction ne
satisfait pas le critère d'élimination des singletons car elle a deux constructeurs.
Cependant, on peut facilement montrer que ``m <= n`` est une
proposition homooptique pour tous les entiers naturels ``m`` et ``n``.
C'est donc un bon candidat pour un inductif dans |SProp| qui soit éliminable dans n'importe quel type.
La raison pour laquelle il s’agit d’une proposition homotopique est cependant plus subtile que
ce que l’élimination des singletons demande habituellement, car tous
les arguments des constructeurs de ``le`` ne sont pas dans |SProp|.
Pour voir pourquoi c'est une proposition homotopique, il faut distinguer entre
arguments de constructeur forcés et non forcés. [#ff7]_
Un argument forcé est un argument qui peut être déduit des indices
du type de retour du constructeur, et qui ne sont pas
pertinents sur le plan des calculs.
Considérons par exemple le constructeur ``leS: forall m n, m le n -> m le S n``, ses deux premiers arguments ``m`` et
``n`` peuvent être calculés à partir du type de retour ``S m <= S n`` et sont donc forcés.
En revanche, l'argument de type ``m <= n`` ne peut pas être déduit
à partir du type et doit donc être dans |SProp|.

Cependant, être une proposition homotopique ne suffit pas, comme nous l'avons vu
avec les types singleton et le prédicat d'accessibilité.
Ici, la situation est encore plus subtile. Considérons la définition (propositionnellement)
équivalente de `` n <= m`` suivante, qui est en fait celle utilisée
dans la bibliothèque standard de Coq:

.. coqdoc::
  Inductive le' : nat -> nat -> SProp :=
    | le'_refl : forall n, n le' n
    | le'S : forall m n,  m le'  n -> m le' S n.

On peut aussi montrer que ``le' m n`` est une proposition homotopique, mais
``le`` et ``le'`` ne partagent pas le même principe d'inversion.
En effet, dans le contexte (absurde) ``e: le' (S n) n``, il y a deux façons
de former un terme de type ``le' (S n) (S n)``, soit en utilisant le ``le'_refl (S n)``,
soit en utilisant ``le'S (S n) n e``.
Cela signifie que permettre à `` le' m n`` d'être éliminé dans
n'importe quel type oblige à être capable de décider si le contexte
est absurde ou non, ce qui n'est évidemment pas
une propriété décidable de la théorie des types.
Pour ``m <= n``, la situation est différente, car le
type de retour des deux constructeurs ``le0`` et ``leS`` sont
orthogonaux, dans le sens où ils ne peuvent pas être unifiés.


Le filtrage dépendant à la rescousse
+++++++++++++++++++++++++++++++++++++++++++++++++++++++

Nous proposons un nouveau critère pour savoir quand un type dans |SProp| peut être éliminé
dans un type arbitraire, réparant et généralisant le critère
d'élimination des singletons. Ce critère est assez général pour distinguer
les définitions de ``<= `` et de ``le'``. En général, un
type inductif dans |SProp| peut être éliminé s'il satisfait les trois
propriétés suivantes :

1. Chaque argument non forcé doit être dans |SProp|.
2. Les types de retour des constructeurs doivent être orthogonaux deux à deux.
3. Chaque appel récursif doit satisfaire à une condition de garde syntaxique.

Pour justifier ce critère, nous fournissons une traduction générale de tout
type inductif satisfaisant ce critère à un type équivalent défini
comme point fixe, en utilisant des idées provenant du filtrage dépendant
:cite:`cockx2014pattern,cockx_devriese_2018`. En effet, regarder la
définition inductive de la droite (sa conclusion) vers la gauche (ses
arguments) nous permet de construire un *arbre des cas possibles* de
manière similaire à ce qui est fait dans le filtrage dépendant. 
Fournir cette traduction signifie également que nous évitons la
nécessité d'étendre notre système de base,
comme tous les types inductifs peuvent être encodés en utilisant les primitives existantes.

Rejeter les arguments du constructeur qui ne sont pas dans |SProp|
*********************************************************************************

La première propriété est la plus simple à comprendre: si un
constructeur de type inductif peut stocker des informations qui sont
pertinente sur le plan du calcul, il ne doit pas pouvoir être dans
|SProp|
(ou à minima, il ne doit pas être éliminable à un prédicat dans  |Type|).

Rejeter les définitions non orthogonales
****************************************************************************

L'idée de la deuxième propriété est que les indices du type de retour
de chaque constructeur doit déterminer dans quel constructeur nous sommes, en utilisant
des indices disjoints pour les différents constructeurs.
C'est une approximation syntaxique du critère d'orthogonalité.
C'est la propriété qui ne tient pas pour ``le'``.

Rejeter les points de fixation non terminaux
****************************************************************************

En plus des deux premières propriétés, nous avons également besoin
d’une condition de garde syntaxique sur les arguments d'un
constructeur récursif.
Cette condition de garde impose que la définition de point fixe résultante soit
bien fondée. Nous pouvons donc utiliser exactement la même condition syntaxique
que celle déjà utilisée pour les points fixes déjà implémentée dans la
théorie des types
(pas importe laquelle, pour autant qu’elle garantisse la terminaison).

Par exemple, dans le cas de ``Acc``, l'arbre des cas possibles induit la
définition suivante, qui est automatiquement rejetée par le
vérificateur de terminaison en raison de l'appel récursif non gardé ``Acc 'y``

.. coqdoc::
 Fail Equations Acc' (x: A) : SProp :=
    Acc' x := (forall y:A, R y x -> Acc' y).

Dérivation automatique des points fixes et des éliminateurs
****************************************************************************

Si les trois propriétés sont satisfaites, nous pouvons automatiquement déduire un
point de fixe dans |SProp| qui équivaut à la définition inductive.
Chaque constructeur correspond à une branche unique d’un cas, et
le type de retour dans chaque branche est la somme dépendante des
arguments non forcés du constructeur correspondant
(le cas zéro étant ``sUnit``).
Par exemple, pour ``<=``, le point fixe est décrit par

.. coqdoc::
  Equations le_f (n m : nat) : SProp :=
    0 le_f n := sUnit;
    S m le_f S n := m le_f n;
    S _ le_f 0 := sEmpty.

Notons que les branches ne correspondant à aucun constructeur reçoivent la valeur
``sEmpty``.
On peut alors aussi définir des fonctions correspondant aux constructeurs
et le principe d’élimination (dans |Type|) du type inductif de
départ. 

Travaux Reliés
--------------

L'insignifiance de preuves définitionnelle a été étdudiée par
d'autres, bien que son implémentation par des assistants de preuves soit récente:

- En Agda sur le principe des arguments non pertinents, depuis Agda
  2.2.8 en 2010, puis étendu pour la version 2.6.0 en 2019 pars un
  univers de propositions strictes (comme décrit dans cette thèse).
- En Lean (au moins depuis 2015 d'après de Moura et al. :cite:`de_Moura_2015`),
  en utilisant un univers de propositions strictes qui permet le prédicat d'accessibilité.
- En Coq depuis la version 8.10 en 2019.

Du coté théorique, nous avons par exemple

- Du travail sur les modèles de l'insignifiance de preuves, par
  example Miquel et Werner :cite:`Miquel_2003`.

- Une preuve de la confluence en théorie des types avec un type
  d'égalité dans un univers imprédicatif de propositions strictes pars
  Werner :cite:`Werner2008`, avec une conjecture de normalisation plus
  tard contredite pars Abel et Coquand
  :cite:`abel_coquand_failur_normal_impred_type_theor`.

- Du travail sur les arguments non pertinents (décrit rapidement plus
  tôt dans cette introduction), par exemple Abel et Scherer
  :cite:`Abel2012OnIA`. Nous comparons cette approche avec l'univers
  de propositions strictes dans la section :ref:`compare-alt-irr`.

- Du travail par les développeurs de Lean, par exemple la preuve de
  consistence par Carneiro :cite:`Carneiro2019`.


Contributions de cette thèse
------------------------------------

Dans cette thèse, nous proposons le premier traitement général d’une
théorie des types avec un univers des propositions avec des preuves
insignifiantes définitionnellement, grâce à des intuitions venant de
la notion de niveaux d'homotopie de HoTT et de la troncation propositionnelle.
Cette extension de la théorie des types n’ajoute aucun axiome supplémentaire
(mis à part l'existence d'un univers de preuves insignifiantes) et
est notamment compatible avec l'univalence.
Nous prouvons à la fois la cohérence et la décidabilité de la vérification du typage de
la théorie des types résultante.
Ensuite, nous montrons comment définir un critère presque complet pour détecter
quelles définitions inductives dans |SProp| peuvent être éliminées
dans n'importe quel type, en corrigeant et en étendant l’élimination des singletons.
Ce critère, inventé en collaboration avec Jesper Cockx,
utilise la méthodologie générale de l'unification sensible
au calcul et du filtrage dépendant :cite:`cockx2014pattern,cockx_devriese_2018`.
Notre présentation n’est pas spécifique à une théorie de type
particulière, ce qui est montré par une implémentation à la fois dans
Coq, en utilisant un univers imprédicatif des preuves insignifiantes,
et dans Agda, en utilisant une hiérarchie de prédicative d'univers de
preuves insignifiantes.


.. rubric:: Footnotes

.. [#ff1] http://mattam82.github.io/Coq-Equations/
.. [#ff2] Une description de ce problème est disponible à https://github.com/leanprover/lean/issues/654
.. [#ff3] Agda 2.2.8, https://github.com/agda/agda/blob/master/CHANGELOG.md\#release-notes-for-agda-2-version-228
.. [#ff4] Ajouté dans Agda 2.2.10.
.. [#ff5] Issue #2170, https://github.com/agda/agda/issues/2170
.. [#ff6] Cette remarque est initialement due à Peter LeFanu Lumsdaine.
.. [#ff7] Cette terminologie a été introduite par :cite:`brady04`.
