Implemented Theory
------------------

We add a new universe of strict propositions |SProp|. It is
impredicative, like the pre-existing universe of propositions |Prop|.
Unlike |Prop| it is isolated in the cumulativity relation:
:math:`\SProp \leq u` if and only if :math:`u = \SProp`.

Irrelevant Inductive Types
++++++++++++++++++++++++++

Like with |Prop|, any inductive type with well-typed and strictly
positive constructors may be declared in |SProp|, but elimination may
be restricted to motives in |SProp|. The condition for unrestricted
elimination is much stronger than for |Prop|: only inductives with no
constructors -- in other words, empty types -- avoid being squashed.

.. note::

   When an inductive type is squashed in |SProp| we must prevent
   defining proof relevant fixpoints (and not just matches) whose
   principal argument is that inductive. Otherwise it becomes possible
   to define fixpoints over the squashed accessibility predicate:

   .. coqdoc::

      Inductive Acc (x:A) : SProp :=
        Acc_in : (forall y, R y x -> Acc y) -> Acc x.

      Definition Acc_inv {x} (a : Acc x) : forall y, R y x -> Acc y
        := match a with Acc_in _ f => f end.

      Fixpoint Fix {P:A -> Type}
        (f : forall x, (forall y, R y x -> P y) -> P x)
        x (a:Acc x) {struct a} : P x
        := f x (fun y Rxy => Fix f y (Acc_inv a y Rxy)).

   This is because Coq is smart enough to know that ``Acc_inv a y
   Rxy`` is structurally smaller than ``a``.

This implicit squashing lets us define the ``Squash`` type as an
inductive itself.

About the elimination restriction
=================================

The :ref:`translation to fixpoints <translation>` gives a criterion
allowing unrestricted eliminations for inductives like

.. coqdoc::

   Inductive Unit : SProp := .

   Inductive NonZero : nat -> SProp := S_NonZero : forall n, NonZero (S n).

or even

.. coqdoc::

   Inductive le : nat -> nat -> SProp :=
   | le0 : forall n, le 0 n
   | leS : forall n m, le n m -> le (S n) (S m).

without affecting the power of the theory. The reduction rules for the
eliminators can be derived from the translation: for instance
``Unit_rect`` reduces unconditionally. Once the constructors take
arguments it becomes more complicated:

.. coqdoc::

   match p : NonZero (S n) with S_NonZero k => k end

must reduce to ``n`` even when ``p`` is blocked (analogously to the
:ref:`uip`), such that we need to extract ``n`` from the index of the
type of the discriminee.

The reduction for matches on ``p : le (S n) (S m)`` is even more
complex: it needs to produce a proof of ``le n m``. Such a proof is
itself a match, but infinite recursion may be avoided by restricting
the special reduction to relevant matches, leaving irrelevant matches
such as the proof of ``le n m`` to reduce only when ``p`` reduces to a
constructor (which by typing must be ``leS``).

This restriction works because thanks to proof irrelevance we only
need to reduce irrelevant terms for canonicity, in which case we are
in an empty context and the special reduction is not needed.

A second difficulty comes from the need to detect which types are
allowed by the translation. A fully general implementation would make
the kernel depend on dependent unification, introducing significant
complexity.

I originally implemented a version of Coq with |SProp| with an
approximation of the criterion based on "forced arguments" (as
explained by Cockx :cite:`cockx2014pattern`). This approximations
accepted ``Unit`` and ``NonZero``, but rejects types needing
projections to reduce (such as for the recursive argument of ``leS``),
types with multiple constructors (such as ``le``) and a variety of
types involving unification problems beyond the capability of forced
arguments.

In the end I decided to drop this system, judging that the
implementation complexity outweighed its benefits considering it
neither improved the power of the theory nor prevented the needed for
encodings on even simple examples.

Identity Types
==============

An implementation allowing non squashed identity types in |SProp|
(with the special reduction behaviour) was also made, but the
combination with impredicativity turned out to be undecidable. Perhaps
it will be reused if we can build confidence that restricting
irrelevant identities to be on small types is unproblematic (i.e.
``eq : forall A : Set, A -> A -> SProp`` with ``Set`` a universe which
is not the type of any other universe may have unrestricted
eliminations), or if we implement a fully predicative mode for Coq.

The implementation is also incomplete: the VM and native conversion
reduction systems do not take the special reduction into account,
causing incompleteness of the respective conversion checkers. It is
unclear how difficult changing this will be.

Until then, we can locally ignore universe constraints to get
irrelevant inductive types with unrestricted eliminations but without
the special reduction:

.. coqdoc::

   Unset Universe Checking.
   Inductive eq {A} (a:A) : A -> SProp := eq_refl : eq a a.
   Set Universe Checking.

This does not threaten consistency but breaks transitivity of the
implemented conversion and subject reduction. Canonicity may also be
preserved but I don't have a full proof.

.. note::

   The ``Universe Checking`` flag requires Coq version 8.11 or above,
   expected release early January 2020.

Relevant Inductive Types
++++++++++++++++++++++++

Proof relevant inductive type may have their constructors take proof
irrelevant arguments: they are implictly boxed. This lets us declare
|Box| as a normal inductive.

Primitive Records
+++++++++++++++++

Primitive records may be declared in |SProp| if all their fields are
irrelevant. This includes the type of irrelevant pairs:

.. coqdoc::

   Set Primitive Projections.
   Record sSigma (A:SProp) (B:A -> SProp) : SProp
     := ssigma { spr1 : A; spr2 : B spr1 }.

There is no squashing for primitive records, so records in |SProp|
with a relevant field are rejected.

This is the preferred way to declare irrelevant pairs, as the
inductive version is squashed, making it harder to work with (although
proof irrelevance and definability of its projections mean it is no
less powerful than the record version).

There is also a restriction for relevant records to avoid definitional
proof irrelevance escaping from |SProp| through the :math:`\eta`
expansion, for instance with the following record:

.. coqdoc::

   Fail Record Box (A:SProp) : Set := box { unbox : A }.

Relevant records are allowed to have irrelevant fields so long as they
also have at least one relevant field. For instance

.. coqdoc::

   Record sigS (A:Type) (B:A -> SProp) : Type := existS { pr1S : A; pr2S : B pr2S }.

.. _implem-kernel:

Kernel Side
-----------

We mark elements of the environment with their relevance: variables,
constants, inductive types (with their relevance as types, which is
the relevance as terms of the constructors) and record projections.

There are also marks inside terms for every binder (product types,
lambda abstractions, and (co)fixpoints. Note that the binders involved
in case eliminations such as pattern variables are internally
represented as lambdas, at least for now). Case eliminations are also
marked with their relevance (i.e. the relevance of their return type):
this could often be recovered from the branches but eliminations on
the empty type have no branches.

With all these marks, determining the relevance of a term is a
syntactic recursion along the head of applications down to either
something marked with its own relevance or something whose relevance
can be looked up in the environment. Critically for performance, it
involved no substitution or reduction.

Coq's kernel has 3 implementations of reduction: a walk over the
abstract syntax implementing the lazy Krivine abstract machine
:cite:`LazyMachine,Barras99`, a bytecode compiler/interpreter
:cite:`CompiledStrongReduction` and a compiler to OCaml which then
calls the OCaml compiler and dynamically links the result
:cite:`FullReduction`. Each is associated with a decision procedure
for conversion trusted by the kernel.

For each of these conversions, the basic idea is to weak head
normalize the arguments, compare the shape of the heads then recurse
on the "stack" of applications, case eliminations, etc around around
the heads. To implement proof irrelevance, we simply check whether the
terms to convert are marked irrelevant around each such recursive
step. In simplified code (from ``kernel/reduction.ml``, function and
exception names unchanged):

.. code-block:: ocaml

  let rec ccnv env x y =
    try eqappr env x y
    with NotConvertible
      when is_irrelevant env x && is_irrelevant env y -> ()

  and eqappr env x y =
    let hx, sx = whd_stack env x in
    let hy, sy = whd_stack env y in
    match hx, hy with
    | Var vx, Var vy ->
      if vx <> vy then raise NotConvertible;
      convert_stacks env sx sy
      (* calls ccnv on each argument for application,
         on branches for matches, etc *)
    | ...

.. note::

   As a performance optimisation, we do not unfold constants in the
   ``whd_stack`` calls. Then we converting two applications of the
   same constant, we can try converting their arguments and unfold
   only if that fails. When doing that unfolding, relevance has not
   changed so we directly recurse with ``eqappr`` instead of ``ccnv``.

.. note::

   Assuming the starting conversion problem is between terms sharing a
   common type, when we check for irrelevance it is always for terms
   which have a common type (just as it works in
   :ref:`decidable-proof`), so we do not need to check both terms.
   However the untrusted layers of Coq (for instance the tactic ``set
   (x:=term)``, see `past issue
   <https://github.com/SkySkimmer/coq/issues/13>`_) use the kernel
   conversion between terms which do not share a type. Checking
   relevance for both sides of the conversion problem then reduces
   |SProp|-related problems.

A similar addition needs to be made to the VM and native compute based
conversions.

.. _issues-cumul:

Issues with non-cumulativity
----------------------------

During normal term elaboration (transforming user syntax to a term of
type theory), we don't always know that a type is a strict proposition
early enough. For instance:

.. coqdoc::

   Definition constant_0 : ?[T] -> nat := fun _ : sUnit => 0.

While checking the type of the constant, we only know that ``?[T]``
must inhabit some sort. Putting it in some floating universe ``u``
would disallow instantiating it by ``sUnit : SProp``.

In order to make the system usable without having to annotate every
instance of :math:`\SProp`, we consider :math:`\SProp` to be a subtype
of every universe during elaboration (i.e. outside the kernel). Then
once we have a fully elaborated term it is sent to the kernel which
will check that we didn't actually need cumulativity of :math:`\SProp`
(in the example above, ``u`` doesn't appear in the final term).

This means that some errors will be delayed until ``Qed``:

.. coqtop:: in

   Lemma foo : Prop.
   Proof. pose (fun A : SProp => A : Type); exact True.

.. coqtop:: all

   Fail Qed.

.. coqtop:: in

   Abort.

By using ``Unset Elaboration StrictProp Cumulativity.`` Coq keeps
cumulativity disabled for |SProp| even during elaboration. This
ensures that conversion is complete even outside the kernel, at the
cost of surprising errors.

.. _incorrect-marks:

Incorrect Marks
+++++++++++++++

If cumulativity is used during elaboration, there is a possibility
that the conversion checker will see incorrect marks:

.. coqtop:: all

  Fail Definition late_mark :=
    fun (A:SProp) (P:A -> Prop) x y (v:P x) => v : P y.

The binders for ``x`` and ``y`` are created before their type is known
to be ``A``, so they're not marked irrelevant. This can be avoided
with sufficient annotation of binders (see ``irrelevance`` at the
beginning of this chapter) or by bypassing the conversion check in
tactics.

.. coqdoc::

   Definition late_mark :=
     fun (A:SProp) (P:A -> Prop) x y (v:P x) =>
     ltac:(exact_no_check v) : P y.

The kernel will re-infer the marks on the fully elaborated term, and
so correctly converts ``x`` and ``y``.

This system is still more limited than we like, for future work we
will attempt to add a concept of elaboration-time universe variables
to Coq (analogous to existential variables when compared with regular
variables). Such variables would be allowed to unify with |SProp| and
help us keep track of such unifications to update marks. As a bonus,
we hope to solve the following long-standing error:

.. coqtop:: all

   Fail Check forall A B, A -> B -> exists x : A, B.
