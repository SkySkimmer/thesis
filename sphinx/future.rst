Practical Concerns
------------------

The Coq implementation has some issues:

- The VM and "native" conversion decision procedures in the kernel
  ignore proof irrelevance, making them incomplete (but still sound).
  This needs some design work to tag function domains and/or free
  variables with their relevance. For instance, in "native" mode
  functions are represented by OCaml functions, so extracting
  information without applying them is difficult.

- Most of the Coq system still assumes cumulativity, leading to
  various issues described in :ref:`issues-cumul`. We have some ideas
  for how to progress (by introducing a separation between universe
  variables and meta-variables) but it's unclear how difficult this
  will be. Hopefully this will allow us to improve usability in other
  ways, for instance allowing to declare |Box| as an implicit
  coercion from |SProp| to other types.

- Tactic support will also need improvement, for instance setoid
  rewriting along relations in |SProp| is currently impossible since
  all the lemmas are stated with relations in |Prop|.

- Bugs are certainly lurking in the implementation, an overlook in De
  Bruijn index lifting has already been found leading to a proof of
  ``False`` (now fixed) (`issue #10904
  <https://github.com/coq/coq/issues/10904>`_).

  In the future perhaps the MetaCoq :cite:`sozeau:hal-02167423`
  project will help prevent such issues?

Usage of |SProp|
----------------

So far |SProp| has been used in small scale experiments with type
theory in type theory, for instance an (unpublished) implementation of
the setoid model by Simon Boulier.

In general we can expect 2 classes of uses for |SProp|:

- Small improvements from being able to replace axioms like UIP by
  proofs, or better computation in the presence of axioms (for
  instance function extensionality breaks canonicity even with
  definitional UIP, but definitional UIP means that if a use of
  function extensionality reduces to an identity it will still
  compute). For instance :cite:`Selsam_2016` (congruence closure with
  dependent functions in Lean) make use of UIP, but axiomatic UIP
  would also work.

- Developments which deeply use proof irrelevance, which means they
  also deeply use dependent types. Use of UIP is likely to feature
  heavily. For instance it should be possible to formalize Two Level
  Type Theory :cite:`AnnenkovCK17` using equality in |SProp| for
  strict equality.

In general we probably have much to learn from the Lean world, which
has experience with definitional proof irrelevance. It will be
interesting to see the impact of the different choices we have made
regarding decidability of conversion with the irrelevant universe.
