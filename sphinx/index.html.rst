==========================
Introduction and Contents
==========================

.. keep in sync with latex.rst

.. include:: introduction_french.rst
.. include:: introduction.rst

.. toctree::
   :caption: Résumé Étendu / Extended Abstract

   self

.. toctree::
   :caption: Dependent Type Theory

   pastwork

.. toctree::
   :caption: Dependent Type Theory with Definitional Proof Irrelevance

   stt

.. toctree::
   :caption: Interpreting Inductive Types As Fixpoints

   translation

.. toctree::
   :caption: Definitional Uniqueness of Identity Proofs

   uip

.. toctree::
   :caption: Implementing Definitional Proof Irrelevance in Coq

   implem

.. toctree::
   :caption: Other Contributions

   other

.. toctree::
   :caption: Future Perspectives

   future

.. toctree::
   :caption: Reference

   zebibliography
