The text of this introduction is heavily derived from the introduction
of Gilbert et al. :cite:`Gilbert:POPL2019`.

.. _intro-eng:

The Need for Proof Irrelevance
------------------------------

Proof-irrelevance, the principle that any two proofs of the same proposition
are equal, is at the heart of the Coq extraction mechanism :cite:`letouzey04`.
In the Calculus of Inductive Constructions (``CIC``), the underlying
theory of the Coq proof assistant, there is an (impredicative) sort
|Prop| that is used to characterize types that can be seen as
propositions---as opposed to types whose inhabitants have a
computational meaning, which live in the predicative sort |Type|.
This static piece of information is used to extract a Coq formalization
to a purely functional program, erasing safely all the parts involving
terms that inhabit propositions, because a program can not use those
terms in computationally relevant ways.

In order to concretely guarantee that no computation can leak from propositions
to types, Coq uses a restriction of the dependent elimination from
inductive types in |Prop| into predicates in |Type|.
This restriction, called the *singleton elimination* condition,
checks that an inductive proposition can be eliminated into a type only
when:

* the inductive proposition has at most one constructor,
* all the arguments of the constructor are themselves
  non-computational, i.e., are in |Prop|.


However, in the current version of Coq, singleton elimination ensures
compatibility with proof irrelevance, but does not provide it. This
means that although two proofs of the same proposition can not be
relevantly distinguished in the system, one can not use the fact that
they are equal in the logic (without an axiom).


Consider for instance a working mathematician or computer scientist
who defines bounded integers in the following way:

.. coqdoc::
  Definition boundednat (k : nat) : Type := { n : nat & n <= k }.

Here ``boundednat k`` is the dependent sum of an integer ``n``
together with a proof (in |Prop|) that ``n`` is below ``k``,
using the inductive definition

.. coqdoc::
  Inductive le : nat -> nat -> Prop :=
  | le0 : forall n, 0 <= n
  | leS : forall m n, m <= n -> S m <= S n.

Then, our user defines an implicit coercion from
``boundednat`` to ``nat`` so that she can work with bounded integers almost as if they
were integers, apart from additional proofs of boundedness.

.. coqdoc::
  Coercion boundednat_to_nat : boundednat >-> nat.

For instance, she can define the addition of bounded integers by simply
relying on the addition of integers, modulo a proof that the result is
still bounded:

.. coqdoc::
   Definition add {k} (n m : boundednat k) (e : n + m <= k)
     : boundednat k
     := ( n + m ; e).

Unfortunately, when it comes to reasoning on bounded integers, the
situation becomes more difficult. For instance, the fact that addition of
bounded natural numbers is associative

.. coqdoc::
   Definition bounded_add_associativity k (n m p: boundednat k)
         e_1 e_2 e'_1 e'_2 :
    add (add n m e_1) p e_2 = add n (add m p e'_1) e'_2.

does not directly follow from associativity of addition on integers,
as it additionally requires a proof that ``e_2`` equals
``e'_2`` (modulo the proof of associativity of addition of
integers).
This should be true because they are two proofs of the same
proposition, but it does not follow automatically. Instead, the user
has to prove proof-irrelevance for ``<=`` *manually*.
This can be proven (using Agda style pattern matching of the Equations
plugin [#f1]_ by induction on the proof of ``m <= n``:

.. coqdoc::
 Equations le_hprop {m n}  (e e' : m <= n) : e = e' :=
    le_hprop (le0 _) (le0 _) := eq_refl;
    le_hprop (leS _ _ e) (leS n m e') := ap (leS n m) (le_hprop e e').

Note the use of functoriality of equality
``ap : forall f, x = y -> f x = f y``
which requires some explicit reasoning on equality.
Even if proving associativity of addition was more complicated than
expected, our user is still quite lucky to deal with an inductive type
that is actually a mere proposition, in the sense of Homotopy Type
Theory (HoTT) :cite:`hottbook`.
Indeed, ``<=`` satisfies the propositional (as opposed to
definitional, which holds by computation) version of proof
irrelevance, as expressed by the ``le_hprop`` lemma.
For an arbitrary inductive type in |Prop|, there is no reason
anymore why it would be a mere proposition, and thus
proof irrelevance, even in its propositional form, can not be proven
and must be stated *as an axiom*.

In a setting where proof-irrelevance for |Prop| is built in, it becomes
possible to define an operation on types which turns any type into a
definitionally proof irrelevant one and thus makes explicit in the
system the fact that a value in ``Squash T`` will never be used for
computation.

.. coqdoc::
  Definition Squash (T : Type) : Prop := forall P : Prop, (T -> P) -> P.

This operator satisfies the following elimination principle
(restricted to propositions) given by

.. coqdoc::
  forall (T : Type) (P : Prop), (T -> P) -> Squash T -> P.

which means that it corresponds to the propositional truncation
:cite:`hottbook` of HoTT, originally introduced as the bracket type by
Awodey and Bauer :cite:`Awodey:2004:PT:1094469.1094484`.



The importance of (definitional) proof irrelevance to simplify
reasoning has been noticed for a long time, and various works have
tried to promote its implementation in a proof assistant based on type
theory :cite:`Pfenning:2001:IEP:871816.871845,Werner2008`.
However, this has never been achieved, mainly because of the
fundamental misunderstanding that *singleton elimination was the right
constraint on which propositions can be eliminated into a type*.

Indeed, one can think of singleton elimination as a *syntactic*
approximation of which types are naturally mere propositions, and thus
can be eliminated into an arbitrary type without leaking a piece of computation
or without implying new axioms.
But, singleton elimination does not work for indexed datatypes, as for instance
it applies to the equality type of Coq

.. coqdoc::
  Inductive eq (A : Type) (x : A) : A -> Prop :=  eq_refl : eq A x x

If proof irrelevance holds for the equality type, every equality has at
most one proof, which is known as Uniqueness of Identity Proofs (UIP).
Therefore, assuming proof irrelevance together with the singleton
elimination enforces a new axiom in the theory, which is for instance incompatible
with the univalence axiom from HoTT.
This may not seem too problematic to some, but another consequence of
singleton elimination in presence of definitional proof irrelevance is
that it breaks decidability of conversion. For instance, the
accessibility predicate:

.. coqdoc::
 Inductive Acc (A : Type) (R : A -> A -> Prop) (x : A) : Prop :=
    Acc_intro : (forall y : A, R y x -> Acc R y) -> Acc R x

satisfies the singleton elimination criterion but implementing definitional
proof irrelevance for it leads to an undecidable conversion and thus
an undecidable type checker.



An alternative approach is to do as in Lean, where they do have proof
irrelevance with singleton elimination, but they only implement a
partial version of proof irrelevance for recursive inductive types
satisfying the singleton elimination, which is
restricted to closed terms. [#f2]_

But this partial implementation of the conversion algorithm breaks in
particular subject reduction, which seems a desirable property for a
proof assistant.

Finally, singleton elimination fails to capture inductive types with
multiple constructors such as ``<=`` which are perfectly valid mere
propositions and could be eliminated into types.

Looking back, Coq and its impredicative sort |Prop| may not be the only
way to implement proof irrelevance in a proof assistant. Agda, which
only has a predicative hierarchy of universes and no |Prop|, instead uses
a notion of irrelevant arguments :cite:`Abel2012OnIA`.
The idea there is to mark in the function type which arguments can be
considered as irrelevant. For instance, our user can encode bounded
natural numbers in this setting, by specifying that the second argument
of the dependent pair is irrelevant (as marked by the :agda:`.` in the
definition of :agda:`.(n <= k)`):

.. code-block:: agda

  data boundedNat (k : ℕ) : Set where
    pair : (n : ℕ) → .(n ≤ k) → boundedNat k

The fact that equality of the underlying natural numbers implies
equality of the bounded natural numbers comes for free from
irrelevance of the second component:

.. code-block:: agda

  piBoundedNat : {k : ℕ} (n m : ℕ) (e : n ≤ k) (e′ : m ≤ k)
               → n ≡ m → pair n e ≡ pair m e′
  piBoundedNat n m _ _ refl = refl

However, in this approach proof irrelevance is not a property of the
type being considered but rather a way to use the argument of a given
function. We hope that |SProp| will provide more flexibility to formal
proof developers.


Lessons from Homotopy Type Theory
-----------------------------------


Before diving into the precise definition of a type theory with
definitional proof irrelevance, let us explore what makes it
difficult to introduce while keeping decidable type checking and
avoiding to induce additional axioms such as UIP or functional
extensionality.

|SProp| as a Syntactical Approximation of Mere Propositions
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

The first lesson from HoTT is that each type in |SProp| must be a mere
proposition, i.e. have homotopy level :math:`-1`.
Formally, the universe of mere propositions ``hProp`` is defined as

.. coqdoc::
  Definition hProp := { A : Type & forall x y : A , x = y }.

and thus it corresponds exactly to the universe of types satisfying
*propositional* proof irrelevance.
As we have mentioned in the introduction, the operator ``Squash``
which transforms any type into an inhabitant of |SProp|, corresponds to
the propositional truncation.

The existence of |SProp| becomes interesting only when we can
eliminate some inductive definitions in |SProp| to arbitrary types.
If not, |SProp| constitutes an isolated logical layer corresponding
to propositional logic, without any interaction with the rest of the
type theory.

The question is thus:

*Which inductive types of a universe of definitionally proof irrelevant types can be eliminated over arbitrary types?*

  
Of course, to preserve consistency, this should be restricted to
inductive types that can be proven to be mere propositions, but this
is not enough to preserve decidability of type checking and
independence from UIP.

Flirting with Extensionality
+++++++++++++++++++++++++++++++++++++++

Let us first look at an apparently simple class of mere propositions,
contractible types. They constitute the lowest level in the hierarchy
of types, for which not only there is at most one inhabitant up to
equality, but there is exactly one.
Of course, the unit type in |SProp|  which corresponds to the true proposition

.. coqdoc::
  Inductive sUnit : SProp := tt : sUnit.

is contractible.

We will say that the unit type can be eliminated to any type, and thus
we get a unit type with a definitional :math:`\eta`-law such that ``u`` is
convertible to ``tt`` for any ``u : sUnit``.
But in general, it should not be expected that any contractible type
in |SProp| can be eliminated to an arbitrary type, as we can see below.

.. _bad-singleton:

Singleton types
**************************************

A prototypical example of a non-trivial contractible type is the
so-called singleton type. For any type ``A`` and ``a:A``,
it is defined as the subset of points in ``A`` equal to ``a``:

.. coqdoc::
  Definition Sing (A : Type) (a : A) := { b : A & a = b }.
  
If we include singleton types in |SProp|, and hence permit its elimination
over arbitrary types, we are led to an extensional type theory, in which
propositional equality implies definitional equality. This would thus
add UIP and undecidability of type checking to the theory. [#f6]_
Indeed, assume that singleton types can be eliminated to arbitrary
types, then there exists a projection ``pi1 : Sing A a -> A`` which
recovers the point from the singleton. But then, for any proof of
equality ``e: a = b`` between ``a`` and ``b`` in ``A``,
using congruence of definitional equality, there is the following chain
of implications

.. code-block:: text

      (a ; refl) ≡ (b ; e) : Sing A a
  =>  pi1 (a ; refl) ≡ pi1 (b ; e) : A
  =>  a ≡ b : A

and hence ``e : a = b`` implies ``a ≡ b``. From this
analysis, it is clear that |SProp| cannot include all contractible
types.

Flirting with Undecidability
+++++++++++++++++++++++++++++++++++++++

Let us now look at other inductive types that are mere propositions
without being contractible.
The first canonical example is the empty type

.. coqdoc::
  Inductive sEmpty : SProp := .

that has no inhabitant, together with an elimination principle which
states that anything can be deduced from the empty type

.. coqdoc::
 sEmpty_rect : forall T : Type, sEmpty -> T.

We will see that this type can be eliminated to any type, and this is
actually the main way to make |SProp| communicate with |Type|. In
particular, it allows the construction of computational values by
pattern matching and use a contraction in |SProp| to deal with absurd
branches.

The other kind of inductive definition in |SProp| that can be
eliminated into any type is a dependent sum of a type ``A`` in
|SProp| and a dependent family over ``A`` in |SProp|, the nullary
case being the unit type ``sUnit``.

Let us now have a look at more complex inductive definitions.

.. _bad-acc:

Accessibility predicate
**************************************

As mentioned in the introduction, the accessibility predicate is a
mere proposition but it cannot be eliminated into any type while
keeping conversion---and thus type-checking---decidable.
Intuitively, this is because the accessibility predicate allows to define
fixpoints with a *semantic* guard (the fact that every recursive
call is on terms ``y`` such that ``R y x``) rather than a
*syntactic* guard (the fact that every recursive call is on a
syntactic subterm).
This is problematic in a definitionally proof irrelevant setting
because a function that is defined by induction on an accessibility
predicate could be unfolded infinitely many times.
To understand why, consider the inversion lemma

.. coqdoc::

  Definition Acc_inv A R (x : A) (X : Acc x)
    : forall y:A, R y x -> Acc y
    := match X with Acc_intro f => f end.

The inversion lemma is always defined, since even if elimination for
``Acc`` is restricted to |Prop| the return type is that same |Prop|
(by impredicativity).

However, suppose elimination on ``Acc`` is not restricted such that we
have the general eliminator

.. coqtop:: out

   Check Acc_rect.

Then, from the inversion and using definitional proof
irrelevance, the following definitional equality is derivable, for any predicate
``P : A -> Type`` and function
``F : forall x, (forall y, r y x -> P y) -> P x`` and ``X : Acc x``

.. code-block:: text

    Acc_rect P F x X
  ≡ F x (fun y r => Acc_rect P F y (Acc_inv A R x X y r))

In an open context, it is undecidable to know how many time this
unfolding must be done. Even the strategy that there is at most one
unfolding may not terminate. Indeed, suppose that we are in a context
where there is a proof ``R_refl`` showing that ``R`` is reflexive
(which is inconsistent with the proof ``X``). Then, applying the unfolding above
once to ``F := fun x f => f x (R_refl x)`` computes to

.. code-block:: text

  Acc_rect P F x X ≡ Acc_rect P F x (Acc_intro x (Acc_inv A R x X))

and the unfolding can start again for ever.

As mentioned above, if we analyze the source of this infinite
unfolding, it is due to the recursive call to ``Acc`` in the
argument of ``Acc_intro`` on an arbitrary variable ``y`` that is not
*syntactically* smaller than the initial ``x`` variable, but
*semantically* guarded by the ``R y x`` condition.
This example shows that singleton elimination is not a sufficient
criterion for when an inductive type in |SProp| can be eliminated into any type,
as one needs to introduce something similar to the syntactic guard
condition on fixpoints.

Let us now see why this is *not a necessary condition* either.

The Good and the Bad Less Than or Equal
***************************************************************

The definition of less than or equal given in introduction does not
satisfy the singleton elimination criterion because it has two constructors.
However, ``m <= n`` can easily be shown to be a mere proposition
for any natural numbers ``m`` and ``n``.
Thus, it is a good candidate for an |SProp| being eliminable into any type.
The reason why it is a mere proposition is however more subtle than
what singleton elimination usually requires, as not every argument of
the constructors of ``le`` is in |SProp|.
To see why it is a mere proposition, one needs to distinguish between
forced and non-forced constructor arguments. [#f7]_
A forced argument is an argument that can be deduced from the indices
of the return type of the constructor, and that are not
computationally relevant.
Consider for instance the constructor ``leS : forall m n,  m le  n -> m le S n``, its two first arguments ``m`` and
``n`` can be computed from the return type ``S m <= S n`` and are thus forced.
In contrast, the argument of type ``m <= n`` cannot be deduced
from the type and thus must be in |SProp|.


However, being a mere proposition is not sufficient as we have seen
with singleton types and the accessibility predicate.
Here the situation is even more subtle. Consider the (propositionally)
equivalent definition of ``n <= m`` that is actually the one used
in the Coq standard library:

.. coqdoc::
  Inductive le' : nat -> nat -> SProp :=
    | le'_refl : forall n, n le' n
    | le'S : forall m n,  m le'  n -> m le' S n.

It can also be shown that ``le' m n`` is a a mere proposition, but
``le`` and ``le'`` do not share the same inversion principle.
Indeed, in the (absurd) context that ``e :  le'  (S n) n``, there are two ways
to form a term of type ``le' (S n) (S n)``, either by using ``le'_refl (S n)``
or by using ``le'S (S n) n e``.
This means that allowing ``le' m n`` to be eliminated into any type would
require to decide whether the context is absurd or not, which is obviously not
a decidable property of type theory.
For ``m <= n`` the situation is different, because the
return type of the two constructors ``le0`` and ``leS`` are
orthogonal, in the sense they cannot be unified.


Dependent Pattern Matching to the Rescue
+++++++++++++++++++++++++++++++++++++++++++++++++++++++

We propose a new criterion for when a type in |SProp| can be eliminated
to an arbitrary type, fixing and generalizing the singleton
elimination criterion. This criterion is general enough to distinguish
between the definitions of ``<=`` and ``le'``. In general, an
inductive type in |SProp| may be eliminated if it satisfies three
properties:

1. Every non-forced argument must be in |SProp|.
2. The return types of constructors must be pairwise orthogonal.
3. Every recursive call must satisfy a syntactic guard condition.

To justify this criterion, we provide a general translation from any
inductive type satisfying this criterion to an equivalent type defined
as a fixpoint, using ideas coming from dependent pattern matching
:cite:`cockx2014pattern,cockx_devriese_2018`. Indeed, looking at the
inductive definition from right (its conclusion) to left (its
arguments) allows us to construct a *case tree* similarly to what
is done with a definition by pattern matching. Providing this
translation also means we avoid the need to extend our core language,
as all inductive types can be encoded using the existing primitives.

Rejecting constructor arguments not in |SProp|
****************************************************************************

The first property is the most straightforward to understand: if a
constructor of an inductive type can store some information that is
computationally relevant, then it should not be in |SProp| (or at
least, we should never eliminate it into |Type|.

Rejecting non-orthogonal definitions
****************************************************************************

The idea of the second property is that the indices of the return type
of each constructor should fix in which constructor we are, by using
disjoint indices for the different constructors.
This is a syntactical approximation of the orthogonality criterion.
This is the property that fails to hold for ``le'``.

Rejecting non terminating fixpoints
****************************************************************************

In addition to the first two properties, we also require a syntactic
guard condition on the recursive constructor arguments. This guard
condition enforces that the resulting fixpoint definition is
well-founded. We may thus use the exact same syntactic condition
already used for fixpoints already implemented in the type theory (no
matter which one it is, as long as it guarantees termination).

For instance, in the case of ``Acc``, the case tree induces the
following definition, which is automatically rejected by the
termination checker because of the unguarded recursive call ``Acc' y``

.. coqdoc::
 Fail Equations Acc' (x: A) : SProp :=
    Acc' x := (forall y:A, R y x -> Acc' y).

Deriving fixpoints and eliminators automatically
****************************************************************************

If all three properties are satisfied, we can automatically derive a
fixpoint in |SProp| that is equivalent to the inductive definition.
Each constructor corresponds to a unique branch of a case tree, and
the return type in each branch is the dependent sum of the non-forced
arguments of the corresponding constructor (the zero case being
``sUnit``).
For instance, for ``<=``, this is given by

.. coqdoc::
  Equations le_f (n m : nat) : SProp :=
    0 le_f n := sUnit;
    S m le_f S n := m le_f n;
    S _ le_f 0 := sEmpty.

Note that branches corresponding to no constructor are given the value
``sEmpty``.
One can then also define functions corresponding to the constructors
and the elimination principle (to |Type|) of the inductive
type.

Related Work
------------

Definitional proof irrelevance has been studied by others, although it
is only recently that is has gained implementations in proof
assistants:

- In Agda based on modal proof irrelevance, released in Agda 2.2.8 in
  2010, then extended in Agda 2.6.0 released in 2019 with a universe
  of strict propositions (as described in this thesis).
- In Lean (at least from 2015 according to de Moura et al. :cite:`de_Moura_2015`),
  using a universe of strict propositions allowing the accessibility predicate.
- In Coq since version 8.10 released in 2019.

On the theoretical side, we have for instance

- Work on models of proof irrelevance, for instance Miquel and Werner
  :cite:`Miquel_2003`.

- A proof of confluence of type theory with equality types in an
  impredicative universe of strict propositions by Werner
  :cite:`Werner2008`, together with a conjecture of normalisation
  later proven false by Abel and Coquand
  :cite:`abel_coquand_failur_normal_impred_type_theor`.

- Work on modal proof irrelevance (i.e. irrelevant arguments,
  described shortly earlier in this introduction), for instance Abel
  and Scherer :cite:`Abel2012OnIA`. We do a short comparison of this
  approach with strict propositions in section :ref:`compare-alt-irr`.

- Work by the Lean team, such as the proof of consistency by Carneiro
  :cite:`Carneiro2019`.

Contributions of this thesis
--------------------------------

In this thesis, we propose the first general treatment of a dependent
type theory with a proof irrelevant sort, with intuitions coming from
the notion of homotopy levels of HoTT and propositional truncation.
This extension of type theory does not add any additional axioms
(apart from the existence of a proof irrelevant universe) and
is in particular compatible with univalence.
We prove both consistency and decidability of type checking of the
resulting type theory.
Then we show how to define an almost complete criterion to detect
which proof-irrelevant inductive definitions can be eliminated into
types, correcting and extending singleton elimination.
This criterion, designed in collaboration with Jesper Cockx,
uses the general methodology of proof-relevant
unification and dependent pattern matching :cite:`cockx2014pattern,cockx_devriese_2018`.
Our presentation is not specific to any particular type theory, which
is shown by an implementation both in Coq, using an impredicative
proof-irrelevant sort, and in Agda, using a hierarchy of predicative
proof-irrelevant sorts.

.. rubric:: Footnotes

.. [#f1] http://mattam82.github.io/Coq-Equations/
.. [#f2] See a description of this issue in https://github.com/leanprover/lean/issues/654
.. [#f3] Agda 2.2.8, see https://github.com/agda/agda/blob/master/CHANGELOG.md\#release-notes-for-agda-2-version-228
.. [#f4] Added in Agda 2.2.10.
.. [#f5] Issue #2170 at https://github.com/agda/agda/issues/2170
.. [#f6] This remark is originally due to Peter LeFanu Lumsdaine.
.. [#f7] This terminology has been introduced by :cite:`brady04`.
