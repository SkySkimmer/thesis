We've defined a theory with a small number of primitive
|SProp|-related types. To make use of it, we need to be able to
express more complicated types. Thankfully, |SProp| is a universe and
as such we can construct its inhabitants by large eliminations from
the usual inductive types such as |Nat|. In this chapter we detail a
general way of defining types by large eliminations which behave like
certain inductive types. This allows us to encode such inductive types
without needing them as a primitive in the theory, and so giving us
inductive types in |SProp|.

Most of this material has already been published as
Gilbert et al. :cite:`Gilbert:POPL2019`.

Note that there are other ways to define datatypes with a dependent
elimination principle, as for instance by using an impredicative
encoding in a type theory enriched with dependent intersections types
as done in :math:`\mathrm{Cedille}`
:cite:`DBLP:journals/apal/Stump18`.

.. _translation:

The Translation
---------------

We define elimination by case trees according to :cite:`cockx2014pattern`.

Constructing the case tree
++++++++++++++++++++++++++

From a high-level view, the idea of the translation is to view the
definition of the inductive type as a *function* that does some case
analysis on the indices and returns the argument types of the
appropriate constructor in each case.

For instance, the following inductive and fixpoint are equivalent:

.. coqdoc::

   Inductive le : nat -> nat -> Prop :=
   | le_0 : forall n, le 0 n
   | le_S : forall m n, le m n -> le (S m) (S n).

   Fixpoint le (m n : nat) : Prop := match m, n with
   | 0, _ => True
   | S m', S n' => le m' n'
   | S _, 0 => False
   end.

One challenge in this translation is to determine which constructor
arguments should appear on the right-hand side: for the constructor
``le_S``, the argument of type ``le m n`` makes an appearance but the
first two arguments ``m`` and ``n`` do not. These disappearing
arguments correspond exactly to the *forced arguments* of the
constructor: their values can be uniquely determined from the type.

To tackle this problem in general, we choose not to translate the
inductive definition to a list of clauses, but directly to a case
tree. For example, we construct the following case tree for ``le``:

.. math::

  m \CoqLeq n := \ctvsplit m {
    \CoqZero &\mapsto& \CoqLeqZero \\
    \CoqSuc\; m' &\mapsto& \ctvsplit n {
      \CoqZero &\mapsto& \bot \\
      \CoqSuc\; n' &\mapsto& \CoqLeqSuc\; (p : m' \CoqLeq n')
    }
  }


For constructing a case tree from the declaration of an inductive
type, we work on an elaboration problem :math:`P` of the form

.. math::

  \Gamma \vdash \left\{ \CoqCon{c_1}\; \Delta_1 \; [ \Phi_1 ] ; \hdots ; \CoqCon{c_k}\; \Delta_k \; [ \Phi_k ] \right\}

where:

- |Gamma| is the "outer" telescope of datatype indices,
- :math:`\CoqCon{c_1}, \ldots, \CoqCon{c_k}` are the names of the
  constructors,
- :math:`\Delta_i` is the "inner" telescope of arguments of :math:`\CoqCon{c_i}`, and
- :math:`\Phi_i` is a set of constraints
  :math:`\{ \problemeq {v_{ij}} {w_{ij}} : A_{ij} \;|\; j = 1\hdots l \}`.

To transform the definition of an inductive datatype
:math:`\CoqData{D}` to a case tree, the initial elaboration problem
has for :math:`\Gamma` the index telescope of :math:`\CoqData{D}`,
:math:`\CoqCon{c_1},\ldots,\CoqCon{c_k}` all constructors of
:math:`\CoqData{D}`, :math:`\Delta_i` the argument telescope of
:math:`\CoqCon{c_i}`, and :math:`\Phi_i = \{ \problemeq {v_{ij}}
{x_{j}} : A_j \;|\; j = 1\hdots l \}` where :math:`\Gamma = (x_1 :
A_1)\hdots(x_l : A_l)` and :math:`v_{i1},\hdots,v_{il}` are the
indices targeted by :math:`\CoqCon{c_i}`, i.e. :math:`\CoqCon{c_i} :
\Delta_i \rightarrow \CoqData{D}\; v_{i1}\; \hdots\; v_{il}`. For
example, for :math:`\CoqLeq` we start with the following elaboration
problem:

.. math::

  (m\; n : \CoqNat) \vdash \left\{ \begin{array}{l@{\;}l@{\;}l}
    \CoqLeqZero & (n' : \CoqNat) &
      [ \problemeq {\CoqZero} {m} : \CoqNat , \problemeq {n'} {n} : \CoqNat ] \\
    \CoqLeqSuc  & (m'\; n' : \CoqNat) (p : m' \CoqLeq n') &
      [ \problemeq {\CoqSuc\; m'} {m} : \CoqNat , \problemeq {\CoqSuc\; n'} {n} : \CoqNat ] \\
  \end{array} \right\}

From this initial problem, the elaboration proceeds by successive case
splitting on variables in |Gamma| and simplification of constraints in
:math:`\Phi_i` until there are only zero or one constructors left and
all constraints have been solved. More specifically, the elaboration
algorithm may perform the following steps:

.. _translate-done:

- **Done**: If all constraints are solved, elaboration returns a leaf
  node containing the disjoint sum of the remaining constructors:

  .. math::

     \Gamma \vdash \left\{ \CoqCon{c}_1\; \Delta_1\; [] ;
         \hdots ; \CoqCon{c}_k\; \Delta_k \; [] \right\}
       \elab \CoqCon{c}_1\; \Delta_1 \uplus \hdots \uplus \CoqCon{c}_k\; \Delta_k

  The empty disjoint sum being the empty case tree: :math:`\Gamma
  \vdash \{\} \elab \bot`.

- **Solve Constraint**: If there is a constraint of the form
  :math:`\problemeq x y` where :math:`x` is bound in :math:`\Delta` and
  :math:`y` bound in :math:`\Gamma`, we can instantiate the variable
  :math:`x` to :math:`y`, removing it from :math:`\Delta` in the
  process:

  .. math::

    \ru{
      \Gamma \vdash \left\{ \CoqCon{c}\; \Delta_1(\Delta_2[y/x])\;
        [ \Phi ] \right \} \elab Q \qquad
      (x : A) \in \Gamma
    }{
      \Gamma \vdash \left\{ \CoqCon{c}\; \Delta_1(x:A)\Delta_2\;
        [ \problemeq {x} {y} : A , \Phi ] \right\} \elab Q
    }

- **Simplify Constraint**: If the left- and right-hand side of a
  constraint are applications of the same constructor, we can simplify
  the constraint:

  .. math::

     \ru{
       \CoqCon{d} : \Delta' \rightarrow \CoqData{D}\; \bar{w}' \qquad
       \Gamma \vdash \left\{ \CoqCon{c}\; \Delta\;
         [ \problemeq {\bar{u}} {\bar{v}} : \Delta' , \Phi ] ; P \right\} \elab Q
     }{
       \Gamma \vdash \left\{ \CoqCon{c}\; \Delta\;
         [ \problemeq {\CoqCon{d}\; \bar{u}} {\CoqCon{d}\; \bar{v}}
           : \CoqData{D}\; \bar{w} , \Phi ]
         ; P \right\} \elab Q
     }

- **Remove Constraint**: If a constraint is trivially satisfied, it
  can be removed:

  .. math::

     \ru{
       \Gamma \vdash u = v : A \qquad
       \Gamma \vdash \left\{ \CoqCon{c}\; \Delta\; [ \Phi ] ; P \right\} \elab Q
     }{
       \Gamma \vdash \left\{ \CoqCon{c}\; \Delta\; [ \problemeq u v : A , \Phi ]
         ; P \right\} \elab Q
     }

- **Remove constructor**: If a constraint is unsolvable because the
  left- and right-hand side are applications of distinct constructors,
  the constructor does not contribute to this branch of the case tree
  and can be safely removed:

  .. math::

     \ru{
       \Gamma \vdash \left\{ P \right \} \elab Q
     }{
       \Gamma \vdash \left\{ \CoqCon{c}\; \Delta\;
         [ \problemeq {\CoqCon{d_1}\; \bar{u}} {\CoqCon{d_2}\; \bar{v}}
           : \CoqData{D}\; \bar{w} , \Phi ]
         ; P \right \} \elab Q
     }

- **Split**: Finally, if :math:`(x : \CoqData{D}\; \bar{w}) \in
  \Gamma` and each constraint set :math:`\Phi_i` contains a constraint
  of the form :math:`\problemeq {\CoqCon{d_j}\; \bar{u}_j} {x} :
  \CoqData{D}\; \bar{w}` where :math:`\CoqCon{d_j}` is a constructor
  of the datatype :math:`\CoqData{D}`, elaboration continues by
  performing a case split on :math:`x`. For each constructor
  :math:`\CoqCon{d_j} : \Delta_j' \rightarrow \CoqData{D}\;
  \bar{w}_j'`, we use proof-relevant unification
  :cite:`cockx_devriese_2018` to determine whether this constructor can
  be used at indices :math:`\bar{w}`. For each of the constructors for
  which unification succeeds positively, elaboration continues to
  construct a subtree for that constructor.

  .. math::

    \ru{
      (x : \CoqData{D}\; \bar{w}) \in \Gamma \qquad
      \CoqData{D} : \Psi \rightarrow \Type{} \\
      \begin{array}{ll}
        \CoqCon{d_j} : \Delta_j' \rightarrow \CoqData{D}\; \bar{w}_j' & \text{for } j = 1\hdots l \\
        \textsc{unify}(\Gamma\Delta_j' \vdash \bar{w} = \bar{w}_j' : \Psi) \Rightarrow \textsc{yes}(\Gamma_j, \sigma_j, \tau_j) & \text{for } j = 1\hdots k \quad (k \leq l) \\
        \textsc{unify}(\Gamma\Delta_j' \vdash \bar{w} = \bar{w}_j' : \Psi) \Rightarrow \textsc{no} & \text{for } j = k+1\hdots l \\
        \Gamma_j \vdash \left\{ \CoqCon{c_i}\; \Delta_i\sigma_j\; [\Phi_i\sigma_j] | i = 1\hdots m \right\} \elab Q_j & \text{for } j = 1\hdots k
      \end{array}
    }{
      \Gamma \vdash \left\{ \CoqCon{c_i}\; \Delta_i\; [\Phi_i] | i = 1\hdots m \right\} \elab \ctsplit x { \CoqCon{d_j}\; \hat\Delta_i' \mapsto^{\tau_j} Q_j | j = 1\hdots k }
    }

.. _implicit-identity:

- **Implicit Identity**: If there are unsolved constraints but it is
  not possible to split on any variable, we may turn each remaining
  constraint :math:`\problemeq u v : A` into a new constructor
  argument of type :math:`u =_A v`. In other words, the constraint
  implicitly introduces an identity-typed argument.

  .. math::

     \ru{
       \Gamma \vdash \left\{ \CoqCon{c}\; \left(\Delta, u =_A v\right)\; [ \Phi ] ; P \right\}
     }{
       \Gamma \vdash \left\{ \CoqCon{c}\; \Delta\; [ \problemeq u v : A , \Phi ] ; P \right\}
       \elab Q
     }

The elaboration algorithm repeats the steps above whenever they are
applicable until it produces a complete case tree. Thanks to the
:ref:`Implicit Identity <implicit-identity>` rule it cannot get stuck.

.. Termination is justified by TODO (:cite:`cockx_devriese_2018`? need to check contents)

Generating the Constructors and the Eliminator
++++++++++++++++++++++++++++++++++++++++++++++

To make practical use of the type constructed by the elaboration
algorithm from the previous section, we also need terms representing
the constructors and the eliminator for the translated type. For
example, for :math:`m \CoqLeq n` we can define terms
:math:`\CoqFun{lz} : (n : \CoqNat) \rightarrow \CoqZero \CoqLeq n` and
:math:`\CoqFun{ls} : (m\; n : \CoqNat) \rightarrow m \CoqLeq n
\rightarrow \CoqSuc\; m \CoqLeq \CoqSuc\; n` by :math:`\CoqFun{lz} =
\lambda n.\; \CoqCon{tt}` and :math:`\CoqFun{ls} = \lambda m\; n\;
p.\; p` respectively. Note that these terms are type-correct since
:math:`\CoqZero \CoqLeq n = \CoqTop` and :math:`\CoqSuc\; m \CoqLeq
\CoqSuc\; n = m \CoqLeq n`. We can also define the eliminator
:math:`\CoqLeqElim` by performing the same case splits as in the
translation of the type :math:`\CoqLeq`:

.. math::

  \begin{array}{l}\begin{array}{@{}l@{\;}l@{\;}l}
  \CoqLeqElim &:&
    (P : (m\; n : \CoqNat) \rightarrow m \CoqLeq n \rightarrow \Type{})
    (m_0 : (n : \CoqNat) \rightarrow P\; \CoqZero\; n\; (\CoqFun{lz}\; n)) \\ &&
    (m_S : (m\; n : \CoqNat) (H : m \CoqLeq n) \rightarrow P\; m\; n\; x \rightarrow P\; (\CoqSuc\; m)\; (\CoqSuc\; n)\; (\CoqFun{ls}\; m\; n\; x)) \\ &&
    (m\; n : \CoqNat) (x : m \CoqLeq n) \rightarrow P\; m\; n\; x
  \end{array} \\
  \CoqLeqElim\; P\; m_0\; m_S \; m\; n\; x \\ \quad = \ctvsplit m {
    \CoqZero &\mapsto& m_0\; n \\
    \CoqSuc\; m' &\mapsto& \ctvsplit n {
      \CoqZero &\mapsto& \CoqBotElim\; x \\
      \CoqSuc\; n' &\mapsto& m_S\; m'\; n'\; x\; (\CoqLeqElim\; P\; m_0\; m_S\; m'\; n'\; x)
    }
  }
  \end{array}

Note that the eliminator applied to the constructors computes as
expected for the inductive type.

Since the eliminator is the defining property of a datatype, being
able to construct the eliminator shows the correctness of the
translation. In particular, the eliminator can be used to show that
the generated type is equivalent to its inductive version.

Constructing the constructors
=============================

Let :math:`\CoqCon{c} : \Delta \rightarrow \CoqData{D}\; \bar{w}` be
one of the constructors of :math:`\CoqData{D}`. By construction, the
case tree of :math:`\CoqData{D}` will have a leaf of the form
:math:`\CoqCon{c}\; \Delta'` where the variables bound by
:math:`\Delta'` form a subset of those bound in :math:`\Delta`. We
thus define the term :math:`\CoqFun{c}` as :math:`\lambda x_1\;
\hdots\; x_n.\; (x_{i_1} , \hdots , x_{i_m})` where :math:`\Delta =
(x_1 : A_1) \hdots (x_n : A_n)` and :math:`\Delta' = (x_{i_1} :
A_{i_1}') \hdots (x_{i_m} : A_{i_m}')`.

Beware, as it is not immediately obvious that this term is
type-correct: :math:`A_i'` is not necessarily equal to :math:`A_i`,
since the variables in :math:`\Delta \backslash \Delta'` have been
substituted in the process. However, since the :math:`\textsc{Solve
Constraint}` step only applies when both sides of the constraint are
variables, this substitution is just a renaming. We can apply the same
renaming to :math:`\Delta'`, thus ensuring that the term
:math:`\CoqFun{c}` is indeed of type :math:`\Delta \rightarrow
\CoqData{D}\; \bar{w}`.

Constructing the eliminator
===========================

The eliminator :math:`\CoqFun{D\_rect}` for the elaborated datatype
:math:`\CoqData{D} : \Gamma \rightarrow \Type{}` with constructors
:math:`\CoqCon{c_i} : \Delta_i \rightarrow \CoqData{D}\; \bar{v}_{i}`
for :math:`i = 1 \hdots k` takes the following arguments:

- The *motive* :math:`P : \Gamma \rightarrow \CoqData{D}\; \hat\Gamma
  \rightarrow \Type{}`

- The *methods*

  .. math::

    \begin{array}{lll} m_i &:& \Delta_i \rightarrow (H_1 : \Psi_{i1}
      \rightarrow P\; \bar{w}_{i1}\; (x_{ij_1}\; \hat\Psi_{i1}))
      \rightarrow \hdots \rightarrow \\ && (H_{q_i} : \Psi_{iq_i}
      \rightarrow P\; \bar{w}_{iq_i}\; (x_{ij_{q_i}}\;
      \hat\Psi_{iq_i})) \rightarrow P\; \bar{v}_{i}\; (\CoqCon{c_i}\;
      \hat\Delta_i) \end{array}

  where :math:`(x_{ij_p} : \Psi_{ip} \rightarrow \CoqData{D}\;
  \bar{w}_{ip}) \in \Delta_i` for :math:`p = 1 \hdots q_i` are the
  recursive arguments of the constructor :math:`\CoqCon{c_i}`.

and produces a function of type :math:`\Gamma \rightarrow (x :
\CoqData{D}\; \hat\Gamma) \rightarrow P\; \hat\Gamma\; x`.

To construct this eliminator, we proceed by induction on the case tree
defining :math:`\CoqData{D}`: for each case split in the elaboration
of :math:`\CoqData{D}`, we perform the same case split on the
corresponding index in :math:`\Gamma`. At each leaf, we are either in
an empty node or a constructor node.

- In an empty node, we have :math:`x : \CoqBottom`, hence we can
  conclude by :math:`\CoqBotElim`.

- In a constructor node for constructor :math:`\CoqCon{c_i}`,
  :math:`x` is a nested tuple consisting of the non-forced arguments
  of :math:`\CoqCon{c_i}`. Moreover, the remaining telescope of
  indices :math:`\Gamma'` contains the forced arguments of
  :math:`\CoqCon{c_i}`. We thus apply the motive :math:`m_i` to
  arguments for :math:`\Delta_i` taken from :math:`x` and
  :math:`\Gamma'`. For the induction hypotheses :math:`H_p` we call
  the eliminator recursively with arguments :math:`\bar{w}_{ip}\;
  (x_{ij_p}\; \hat\Psi_{ip})` where :math:`x_{ij_p}` is again taken
  from either :math:`x` or :math:`\Gamma'`. Note that this recursion
  is well-founded if and only if the recursive definition of the
  datatype itself is so.

This finishes the construction of the eliminator. It can easily be
checked that the eliminator we just constructed also has the
appropriate computational behaviour:

.. math::
  \CoqFun{D\_rect}\; P\; m_1\; \hdots\; m_k\; \bar{v}_i\;
  (\CoqCon{c_i}\; \hat\Delta_i)

evaluates to

.. math::

   m_i\; \hat\Delta_i\; (\lambda \hat\Psi_{i1}.\; \CoqFun{D\_rect}\;
   \bar{w}_{i1}\; (x_{ij_1}\; \hat\Psi_{i1}))\; \hdots\;
   (\lambda \hat\Psi_{iq_i}.\; \CoqFun{D\_rect}\; \bar{w}_{iq_i}\; (x_{ij_{q_i}}\;
   \hat\Psi_{iq_i})).

.. note::

   In |Coq| the elimination principles are derived from fixpoint and
   match primitives. Matches are trivial to translate once we know how
   to translate elimination principles since they're less general.
   Fixpoints can also be translated but the justification is harder as
   it involves the complex structural recursion rules.

Non translatable types
++++++++++++++++++++++

The resulting case tree is not always well-founded. For instance, from
the inductive

.. coqdoc::

   Inductive Loop := loop : Loop -> Loop.

we get

.. math::

   \vdash \left\{ \CoqCon{loop}\; \CoqData{Loop}\; [] \right\} \elab \CoqCon{loop}\; \CoqData{Loop}.

This would make us define the translation as

.. coqdoc::

   Fixpoint Loop := Loop.

which of course is rejected. Our algorithm leaves deciding termination
of the produced case tree to the type-checking of the host type
theory. Note that if the translated inductive terminates, then so does
the translated eliminator as it recurses on the same arguments.

Termination checking can be complex, for instance |Coq| recognises
that the applied fixpoint ``n - m`` (subtraction for natural numbers)
is (non strictly) structurally smaller than ``n``. Conversely, there
may be inductives whose translation is terminating but for which
termination checking is not smart enough to finish the translation
automatically. In such cases we may be able to manually adapt the
translation's result, based on our argument for why the translation is
terminating (e.g. by using the accessibility predicate and wellfounded
recursion). Note that such an adaption may destroy the definitional
computation rules of the eliminator, although they remain provable.

Other inductives such as ``Loop``, Peano natural numbers or the
accessibility predicate provide expressive power through their
inductive structure which is not available from the primitive data
types used by the translation (disjoint sum, sigma types, empty type,
unit type and identity type). As such it is expected that they cannot
be translated by our algorithm.

Application to Strict Propositions
----------------------------------

A case tree represents a type in |SProp| when it is is either a leaf
node of the form :math:`\CoqCon{c}\; \Delta` where :math:`\CoqCon{c}`
is a constructor name and :math:`\Delta` is a telescope of types in
|SProp|, an empty node :math:`\bot`, or an internal node of the form

.. math::
  \ctsplit x {\CoqCon{c_1}\; \hat\Delta_1 \mapsto^{\tau_1} Q_1 ;
    \hdots ; \CoqCon{c_n}\; \hat\Delta_n \mapsto^{\tau_n} Q_n}

where :math:`x` is a variable, :math:`\CoqCon{c_i}` are constructors
with fresh variables :math:`\hat\Delta_i` for arguments,
:math:`\tau_i` are substitutions (these will be explained later), and
:math:`Q_i` are again case trees which represent |SProp| types.

The translation fails to produce a type which can live in |SProp| when
either:

- one of the constructors has a non-|SProp| argument. Allowing the
  inductive in |SProp| would mean either inconsistency (if the
  telescope of arguments is not a mere proposition) or risk
  undecidability such as with :ref:`bad-singleton`.

- there are multiple constructors remaining when we produce a leaf
  node in the :ref:`Done <translate-done>` rule. From this we can get
  instantiations of different constructors at the same type (possibly
  in an inconsistent context). Then either the theory is inconsistent
  or conversion is undecidable (we need to verify consistency of the
  context to ensure it has not collapsed).

- the translation produces a non-terminating case tree (or at least
  one that we cannot prove terminating). Then the conversion is
  undecidable as we explained in :ref:`Flirting with Undecidability:
  the Accessibility Predicate <bad-acc>`.

- the translation used the :ref:`Implicit Identity
  <implicit-identity>` rule and the host theory does not have
  definitional UIP.

As such, when we have UIP the translation together with our primitive
|SProp| types are complete: all inductive types which could be
accepted as |SProp| inductives without breaking decidability or
consistency can be translated. (There may still be irrelevant types
which would preserve decidability and consistency, but they cannot be
expressed as inductive types.)

When we do not wish to have UIP, the impact of allowing specific types
involving implicit identities (for instance having an identity type
for booleans in |SProp|) is less clear, see chapter :ref:`uip`.
