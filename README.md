
You can `make pdf` to get `Thesis.pdf`, or `make html` to get
`output-html/index.html` and its friends.

Just `make` is the same as `make html`.

Sources are the files in `sphinx/`. A couple are generated (see
`.gitignore`). The interesting ones are the file named in
`index.html.rst` (they appear without extension), and the
`biblio.bib`.

When adding a new file add it to both non-generated `index.rst` files.

tags:

- V0.1: version sent to the administration on 2019-10-24
- V1: version sent to rapporteurs on 2019-10-27
- V1.1: V1 + extra contrib chapter, sent on 2019-11-15
- V2: updated version, sent on 2019-12-13
