
# this should be a coq version with SProp
# ideally my branch 'uip' for when I use examples with uip
COQBIN?=$(HOME)/dev/coq/coq-8.11/_build/install/default/bin/
export COQBIN

# override to run mklatex without running sphinx
QUICK:=latex

TEMPLATE:=mathstic-thesis-template/main.pdf
TEXD:=output-latex

default: html

pdf: $(TEXD)/Thesis.tex $(TEMPLATE)
	$(MAKE) -C $(TEXD) Thesis.pdf <&-
	cp output-latex/Thesis.pdf 2019IMTA0169_Gilbert-Gaetan.pdf

$(TEXD)/Thesis.tex: $(QUICK)

view: pdf
	nohup okular Thesis.pdf >/dev/null &
	sleep 0.5 # needed for some reason when invoking from emacs

latex html: %:
	sphinx-build -b $* -W -j 4 sphinx/ output-$*

template: $(TEMPLATE)
$(TEMPLATE):
	make -C $(@D)


slides:
	make -C slides

viewslides: slides
	nohup okular slides/slides.pdf >/dev/null &
	sleep 0.5 # needed for some reason when invoking from emacs

.PHONY: slides viewslides pdf latex html view $(TEMPLATE) template $(TEXD)/Thesis.tex

# I don't think sphinx can make latex and html at the same time. -j
# will still be passed through to make -C output-latex, not sure with
# what effect.
.NOTPARALLEL:
